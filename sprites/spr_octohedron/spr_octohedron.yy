{
    "id": "c00dbe50-07de-461d-8e42-a9ca9fa76d36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_octohedron",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 10,
    "bbox_right": 116,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15c7c46c-8170-4633-a17d-91b0f30883ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c00dbe50-07de-461d-8e42-a9ca9fa76d36",
            "compositeImage": {
                "id": "e44653c2-d55b-4f1a-a45c-8aada6bb87de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15c7c46c-8170-4633-a17d-91b0f30883ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d41ce198-8b10-4453-b37c-83a892b2efcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15c7c46c-8170-4633-a17d-91b0f30883ec",
                    "LayerId": "dbc6bc97-cd48-49da-a8c3-eb847514794e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 130,
    "layers": [
        {
            "id": "dbc6bc97-cd48-49da-a8c3-eb847514794e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c00dbe50-07de-461d-8e42-a9ca9fa76d36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 123,
    "xorig": 63,
    "yorig": 58
}