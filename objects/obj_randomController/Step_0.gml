if obj_score.wave == 2 {
	wave1speedup = wave2speedup
	wave1speedlo = wave2speedlo
	wave1range = wave2range
	wave1frequency = wave2frequency
}

if obj_score.wave == 3 {
	wave1speedup = wave3speedup
	wave1speedlo = wave3speedlo
	wave1range = wave3range
	wave1frequency = wave3frequency
}

if obj_score.wave == 4 {
	wave1speedup = wave4speedup
	wave1speedlo = wave4speedlo
	wave1range = wave4range
	wave1frequency = wave4frequency
}

if obj_score.wave == 5 {
	wave1speedup = wave5speedup
	wave1speedlo = wave5speedlo
	wave1range = wave5range
	wave1frequency = wave5frequency
}

if obj_score.wave == 6 {
	wave1speedup = wave6speedup
	wave1speedlo = wave6speedlo
	wave1range = wave6range
	wave1frequency = wave6frequency
}