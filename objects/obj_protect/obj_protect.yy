{
    "id": "5ccd772d-3341-4d15-a8c0-31c701859a49",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_protect",
    "eventList": [
        {
            "id": "f171ed0a-57fb-4a71-b164-afdb1ab3a789",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b2d54146-f531-4038-8401-430adcbc9ae2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5ccd772d-3341-4d15-a8c0-31c701859a49"
        },
        {
            "id": "4a9b4391-15df-40c6-93b2-15efb9dfe257",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "20e91c94-a4c2-4101-ace8-1da776fe2faf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5ccd772d-3341-4d15-a8c0-31c701859a49"
        },
        {
            "id": "aab8db0e-4ffa-47ff-bf18-68583d02cfb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "bc18ae00-46cc-4455-a7d7-7a00a58e7ca0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5ccd772d-3341-4d15-a8c0-31c701859a49"
        },
        {
            "id": "ec8bf9d0-cfec-412e-874b-7fbbd2cd6d11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1d22a4f8-97b9-40c9-9afe-c3822ec7a2fc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5ccd772d-3341-4d15-a8c0-31c701859a49"
        },
        {
            "id": "8d05f6b5-a9b8-405a-97af-eb0f1edf37ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d666e7d4-0a5c-4940-87e5-10de7955f184",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5ccd772d-3341-4d15-a8c0-31c701859a49"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b8b01d74-696a-4839-81e6-b3648571cb3d",
    "visible": true
}