if !instance_exists(obj_protect) {
	if !instance_exists(obj_win) {
		instance_create_layer(x-384,y+160,"Instances", obj_fail)
	}
}

switch (obj_player.score_) {
    case 10:
        obj_score.wave = 2
		obj_waveController.image_alpha = 1;
		if !audio_is_playing(snd_applause){
			audio_play_sound(snd_applause,2,0)
		}
        break;
	case 20:
		obj_score.wave = 3
		obj_waveController.image_alpha = 1;
		if !audio_is_playing(snd_applause){
			audio_play_sound(snd_applause,2,0)
		}
		break;
	case 30:
		obj_score.wave = 4
		obj_waveController.image_alpha = 1;
		if !audio_is_playing(snd_applause){
			audio_play_sound(snd_applause,2,0)
		}
		break;
	case 40:
		obj_score.wave = 5
		obj_waveController.image_alpha = 1;
		if !audio_is_playing(snd_applause){
			audio_play_sound(snd_applause,2,0)
		}
		break;
	case 50:
		obj_score.wave = 6
		obj_waveController.image_alpha = 1;
		if !audio_is_playing(snd_applause){
			audio_play_sound(snd_applause,2,0)
		}
		break;
	case 55:
		instance_create_layer(x-384,y+160,"Instances", obj_win)
    default:
        // code here
        break;
}