{
    "id": "675f4a7f-d98d-48bf-96bc-1da18ce435ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_randomController",
    "eventList": [
        {
            "id": "7a52bf8b-735e-4c04-9ef6-a4d24fa69928",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "675f4a7f-d98d-48bf-96bc-1da18ce435ab"
        },
        {
            "id": "db775aca-f7ef-4ed0-bb75-62ba0e8289ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "675f4a7f-d98d-48bf-96bc-1da18ce435ab"
        },
        {
            "id": "58d7c1a3-9a1d-4fd7-b952-c15ba01f397f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "675f4a7f-d98d-48bf-96bc-1da18ce435ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}