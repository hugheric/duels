{
    "id": "bc18ae00-46cc-4455-a7d7-7a00a58e7ca0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_octohedron",
    "eventList": [
        {
            "id": "9f059303-e2bc-4229-b075-70c42185ef82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc18ae00-46cc-4455-a7d7-7a00a58e7ca0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c00dbe50-07de-461d-8e42-a9ca9fa76d36",
    "visible": true
}