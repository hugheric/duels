{
    "id": "5a27b3e9-2f23-43b4-88b8-f284e4edf271",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font2",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Johnny Fever",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9ea4a2b0-9ee0-4145-ac01-3923e72394ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 112,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 612,
                "y": 344
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0f6d94ba-0deb-4388-8106-3e487b27842b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 112,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 872,
                "y": 344
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5370f99f-0903-4487-96a1-eb5bc03ecf51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 112,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 551,
                "y": 344
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c2b2ea7d-92e9-4e6b-aa35-bba5166047cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 112,
                "offset": 1,
                "shift": 45,
                "w": 43,
                "x": 47,
                "y": 230
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f166b61a-7634-458d-a708-df1a00dfd6e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 112,
                "offset": 4,
                "shift": 49,
                "w": 42,
                "x": 2,
                "y": 344
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "338445f2-2157-48ed-ad9b-94e70cf25692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 112,
                "offset": 3,
                "shift": 71,
                "w": 64,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "35f29817-c773-4595-b40a-731998fd8c7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 112,
                "offset": 3,
                "shift": 52,
                "w": 47,
                "x": 752,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "20db2e2c-eedd-4884-b896-ceabf636e46f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 112,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 936,
                "y": 344
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ee4ccdec-a9ff-45a6-88a5-c9c3d7c02592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 112,
                "offset": 8,
                "shift": 31,
                "w": 23,
                "x": 743,
                "y": 344
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0ed2d712-2fed-4d42-8d19-f4bc0774c2ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 112,
                "offset": 0,
                "shift": 31,
                "w": 24,
                "x": 666,
                "y": 344
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4b7eb38b-2ac6-455d-8ba8-c06eb13bc425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 112,
                "offset": 1,
                "shift": 51,
                "w": 49,
                "x": 701,
                "y": 2
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f1699007-d4b7-41e9-b64f-3eedd9fcbe9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 112,
                "offset": 4,
                "shift": 47,
                "w": 39,
                "x": 130,
                "y": 344
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "acfd4b3f-695b-48d4-a5a8-d8a82632d446",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 112,
                "offset": 3,
                "shift": 23,
                "w": 17,
                "x": 817,
                "y": 344
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "77ec517d-a1b0-4cdb-9288-3c21d14134a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 112,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 517,
                "y": 344
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b68216a6-0896-4af0-9e22-40470f2d052c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 112,
                "offset": 3,
                "shift": 21,
                "w": 15,
                "x": 855,
                "y": 344
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "271e5e79-3fe4-4451-9279-422c2018bc1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 112,
                "offset": 0,
                "shift": 44,
                "w": 45,
                "x": 895,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e35ff6f5-8c70-4ab4-8729-710fdc1ac053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 112,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 787,
                "y": 116
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fbae92a6-e53c-4eaa-8649-507b7b2b37ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 112,
                "offset": 1,
                "shift": 28,
                "w": 23,
                "x": 718,
                "y": 344
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "71a09c78-d932-406c-8f9f-e01ed78be749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 112,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 925,
                "y": 116
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1cf032d9-61df-43b1-828e-5394c7c289cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 112,
                "offset": 2,
                "shift": 44,
                "w": 39,
                "x": 212,
                "y": 344
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1eeacf64-e1ca-4a2f-9a25-4f4e69ea9c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 112,
                "offset": 3,
                "shift": 46,
                "w": 41,
                "x": 46,
                "y": 344
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9e6ca497-cad7-4fee-be32-40cd071d20e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 112,
                "offset": 3,
                "shift": 48,
                "w": 42,
                "x": 857,
                "y": 230
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "385abbaa-32b4-4b72-a1cb-550fe02fb17e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 112,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 833,
                "y": 116
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d8aff9a4-5ba5-4911-a8f0-ddca08044724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 112,
                "offset": 2,
                "shift": 47,
                "w": 45,
                "x": 801,
                "y": 2
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "bb3d0928-8c5c-41c6-b0e4-9b2ba1864676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 112,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 407,
                "y": 230
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "704c79c6-529a-41ad-bb76-29d5e67ac742",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 112,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 497,
                "y": 230
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8c0ae40e-f8ec-4750-85ad-69cb7ce877e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 112,
                "offset": 5,
                "shift": 26,
                "w": 15,
                "x": 889,
                "y": 344
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "09bc31ab-9bfb-488d-8ff8-718b95dcf9ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 112,
                "offset": 3,
                "shift": 26,
                "w": 17,
                "x": 836,
                "y": 344
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "eeecbdd7-2fe8-4c91-9e9e-6b16b605da9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 112,
                "offset": 4,
                "shift": 47,
                "w": 39,
                "x": 294,
                "y": 344
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3d9ff7da-2e2f-4a28-b0a1-91773b2f46cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 112,
                "offset": 4,
                "shift": 47,
                "w": 39,
                "x": 253,
                "y": 344
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "63dd196c-93bf-44cb-a9c6-c579a7caacdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 112,
                "offset": 4,
                "shift": 47,
                "w": 39,
                "x": 171,
                "y": 344
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c21e4b1c-d62b-428e-bc34-5b9f1bdd0a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 112,
                "offset": 1,
                "shift": 48,
                "w": 44,
                "x": 603,
                "y": 116
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "eee1ce57-54be-467c-8d43-607491fd171c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 112,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 812,
                "y": 230
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f1e2c74c-8167-44bb-9dba-c2b7f8a827bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 722,
                "y": 230
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ec02944f-dbc4-436f-85ee-c6412e70d50d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 112,
                "offset": 4,
                "shift": 50,
                "w": 43,
                "x": 677,
                "y": 230
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fa636a4d-7665-400d-a250-198273b66562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 112,
                "offset": 3,
                "shift": 49,
                "w": 44,
                "x": 557,
                "y": 116
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fc73d513-86c1-47bc-bd14-117f848b6538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 465,
                "y": 116
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "299a5e15-c7b4-485b-ab06-e5a675aa161f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 112,
                "offset": 4,
                "shift": 50,
                "w": 43,
                "x": 542,
                "y": 230
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "54b3f793-c4e8-4eb6-9391-089fe656b6a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 112,
                "offset": 4,
                "shift": 49,
                "w": 44,
                "x": 419,
                "y": 116
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "359c3454-23a4-4691-aa61-148bc8533a42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 112,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 373,
                "y": 116
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "11022eec-6ee5-4c7e-9f12-4bda0fd5bb15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 452,
                "y": 230
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5b04b78e-aa8d-4890-b91e-d3475fe37bee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 112,
                "offset": 4,
                "shift": 21,
                "w": 13,
                "x": 921,
                "y": 344
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "73c395fb-fbab-4046-a69e-637240415bc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 112,
                "offset": 1,
                "shift": 39,
                "w": 34,
                "x": 409,
                "y": 344
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c68e995f-01b7-4485-ad59-47f72c08ba94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 112,
                "offset": 4,
                "shift": 55,
                "w": 52,
                "x": 485,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "07dc7cba-4d94-496a-a42e-a6dec08bf821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 112,
                "offset": 4,
                "shift": 39,
                "w": 34,
                "x": 373,
                "y": 344
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7214f4ba-0160-477d-b8d5-fb486bee373d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 112,
                "offset": 4,
                "shift": 68,
                "w": 60,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "976d43bc-1316-4d7c-bcac-5ea76f29fced",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 767,
                "y": 230
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "dc943ed2-ac84-41e4-9c58-3b546c97faf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 112,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 649,
                "y": 116
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f4f6ef2f-b275-40d1-bcd7-80bc3db191fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 112,
                "offset": 4,
                "shift": 49,
                "w": 43,
                "x": 227,
                "y": 230
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8ebc940f-614d-4b79-85d2-a08ddc20de5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 112,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 143,
                "y": 116
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "063ddb83-bb84-44ab-ac1e-bd199794774e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 112,
                "offset": 4,
                "shift": 50,
                "w": 45,
                "x": 848,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "59a221ae-8a39-4c1d-a638-2970a20632d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 112,
                "offset": 3,
                "shift": 47,
                "w": 42,
                "x": 901,
                "y": 230
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1ec1a487-3e52-47bb-b826-82fe0b8074df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 112,
                "offset": 1,
                "shift": 47,
                "w": 44,
                "x": 511,
                "y": 116
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d66f7a72-2621-40e7-a1e9-dd0e3e510983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 182,
                "y": 230
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4ba569fb-28f3-4f71-be0b-b088debbc83a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 112,
                "offset": 0,
                "shift": 52,
                "w": 52,
                "x": 593,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "929bcb2e-9b30-44ce-abed-597f4d249e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 112,
                "offset": 4,
                "shift": 68,
                "w": 60,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "812564d7-4e0e-4c92-80cb-9b1d86250ed6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 112,
                "offset": 0,
                "shift": 54,
                "w": 54,
                "x": 429,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "269bc92e-8546-4a8c-a709-81531a102bda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 92,
                "y": 230
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "cd9b5082-1611-4ec0-8f76-7686cc33b881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 112,
                "offset": 2,
                "shift": 49,
                "w": 45,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4d66f38c-1bef-4bfd-a322-0cd594733e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 112,
                "offset": 7,
                "shift": 31,
                "w": 23,
                "x": 768,
                "y": 344
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2b841615-73f0-4060-9ebb-cdcfbce5d038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 112,
                "offset": 0,
                "shift": 44,
                "w": 45,
                "x": 49,
                "y": 116
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "538472d1-abde-4d10-8d54-07a44b66a1dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 112,
                "offset": 1,
                "shift": 31,
                "w": 24,
                "x": 692,
                "y": 344
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "eafa7419-77d8-494e-91ed-a34d4682b2f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 112,
                "offset": 4,
                "shift": 44,
                "w": 36,
                "x": 335,
                "y": 344
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e9e19b35-4c8b-4674-a115-7785d808e17c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 112,
                "offset": -1,
                "shift": 53,
                "w": 55,
                "x": 316,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1b1af7d4-16d2-4b80-aca7-508b9b63406e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 112,
                "offset": 4,
                "shift": 30,
                "w": 22,
                "x": 793,
                "y": 344
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0532726f-26f6-4fb0-a696-0df24e1d36a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 272,
                "y": 230
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "79d23113-f232-4bdd-9eaa-4efc50a6d3ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 112,
                "offset": 4,
                "shift": 50,
                "w": 43,
                "x": 971,
                "y": 116
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6b2f9c1d-69bd-49fd-b9c6-2c8600389907",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 112,
                "offset": 3,
                "shift": 49,
                "w": 44,
                "x": 235,
                "y": 116
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "33e4e134-f31a-4a57-bc92-8138ec04a7a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 189,
                "y": 116
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d2072ce9-3456-4afc-bd02-7beeb2d5c8ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 112,
                "offset": 4,
                "shift": 50,
                "w": 43,
                "x": 362,
                "y": 230
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "46196375-5c15-4110-acf9-2f2adc35636b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 112,
                "offset": 4,
                "shift": 49,
                "w": 44,
                "x": 281,
                "y": 116
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d2f0c186-5566-494c-bb33-f95ea764ca18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 112,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 327,
                "y": 116
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c096ec15-1245-4548-8008-40e92e7b2516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 632,
                "y": 230
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "78aec9b6-6a43-48c2-8605-da8cbe6dc790",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 112,
                "offset": 4,
                "shift": 21,
                "w": 13,
                "x": 906,
                "y": 344
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "addf12be-d0f6-4329-aa75-301293797823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 112,
                "offset": 1,
                "shift": 39,
                "w": 34,
                "x": 445,
                "y": 344
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5dfe7ecc-f576-4b23-b7de-8f7b1556b53e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 112,
                "offset": 4,
                "shift": 55,
                "w": 52,
                "x": 647,
                "y": 2
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "942f9b93-d95c-44b7-a7c1-3d65e09a7617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 112,
                "offset": 4,
                "shift": 39,
                "w": 34,
                "x": 481,
                "y": 344
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "aa1e1ca6-5b74-43e6-ab1a-2408bccdf77f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 112,
                "offset": 4,
                "shift": 68,
                "w": 60,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "73977cbe-2eb4-4887-ac98-a71749d2a575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 587,
                "y": 230
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0068fdb6-a3f0-4488-b589-716b21800018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 112,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 695,
                "y": 116
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f26e2a8b-d98c-4e59-bea8-d6aae0f8aa04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 112,
                "offset": 4,
                "shift": 49,
                "w": 43,
                "x": 317,
                "y": 230
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "64584d8b-62ae-4339-88d7-1ca94f4d7955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 112,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 879,
                "y": 116
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7abc8154-4c61-42f5-a481-988fcc08f153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 112,
                "offset": 4,
                "shift": 50,
                "w": 45,
                "x": 942,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "251becac-ce7c-4846-a2cf-b46ec533799f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 112,
                "offset": 3,
                "shift": 47,
                "w": 42,
                "x": 945,
                "y": 230
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "990c7b93-4cc9-4832-8b6f-c11ad0d913f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 112,
                "offset": 1,
                "shift": 47,
                "w": 44,
                "x": 741,
                "y": 116
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "51ec6588-6a72-450d-a734-64684f5c8a79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 137,
                "y": 230
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "def8c45f-57de-492d-9c46-425c882148f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 112,
                "offset": 0,
                "shift": 52,
                "w": 52,
                "x": 539,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "68e2e164-f3c5-4c33-aa3a-630016327995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 112,
                "offset": 4,
                "shift": 68,
                "w": 60,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6c1c7dde-6b12-41f0-8560-32d965fb74f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 112,
                "offset": 0,
                "shift": 54,
                "w": 54,
                "x": 373,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1e12b5fb-dc2c-4458-960b-d64e89e4b74b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 112,
                "offset": 4,
                "shift": 51,
                "w": 43,
                "x": 2,
                "y": 230
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "72b80261-d01d-4dda-9ea2-f3c8caaa6f2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 112,
                "offset": 2,
                "shift": 49,
                "w": 45,
                "x": 96,
                "y": 116
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0fc1ed1b-d754-4def-a3b5-3e71ca3e5dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 112,
                "offset": 4,
                "shift": 32,
                "w": 26,
                "x": 584,
                "y": 344
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e151bec6-71a1-4722-bc93-070a4f9411d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 112,
                "offset": 12,
                "shift": 35,
                "w": 11,
                "x": 950,
                "y": 344
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9fe0c61a-5ecc-45ce-8155-43a5d0d11e33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 112,
                "offset": 2,
                "shift": 32,
                "w": 25,
                "x": 639,
                "y": 344
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c62cd9b9-fac6-46c5-a89b-2ca527ad17fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 112,
                "offset": 4,
                "shift": 47,
                "w": 39,
                "x": 89,
                "y": 344
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "e61eff8f-434c-47eb-a1b6-0401b76904e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 74
        },
        {
            "id": "a5c64a20-f37d-4587-933f-4d2cb2243966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 106
        },
        {
            "id": "b2e080c6-fb7b-4ee3-93e8-7369f0ec9605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 34,
            "second": 34
        },
        {
            "id": "2e839ffa-b5f9-4f91-91d8-f38d86983012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 34,
            "second": 39
        },
        {
            "id": "28e39219-93a9-4a55-a0ca-ed2fdfaeca61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 44
        },
        {
            "id": "b3f9fa3d-a7fc-46b8-9fca-4d164afd4205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 46
        },
        {
            "id": "418fd799-11d8-4d36-acc3-5f77e46c586b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 34,
            "second": 74
        },
        {
            "id": "bd82d50f-fe15-4dde-8826-288e2a006249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 34,
            "second": 106
        },
        {
            "id": "cb39c748-f59d-49e9-831d-81487af63f80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 224
        },
        {
            "id": "bb97b589-effc-41b0-b306-71eb8f428636",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 225
        },
        {
            "id": "59e410c6-e6ee-487f-9711-6d136508acf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 226
        },
        {
            "id": "e99db63a-1614-4974-996c-5741c6ed7ccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 227
        },
        {
            "id": "86bd1dfb-9335-4ec0-8011-9e73ac0dccbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 228
        },
        {
            "id": "5a9eee17-10c6-4c63-9b7e-bdc2ad21b46d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 230
        },
        {
            "id": "f907e89d-a065-45a2-b9e5-c134127e4a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 256
        },
        {
            "id": "6baa95a4-ab7e-4f37-884a-cfd644c59feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 257
        },
        {
            "id": "773c98c4-f1f3-4213-a580-29b5b81c80d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 258
        },
        {
            "id": "fa49111b-cb95-4c52-96e6-bd07ee67c00f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 259
        },
        {
            "id": "3f483cf3-e06b-40f4-8208-79f1ca47e3cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 260
        },
        {
            "id": "b5fa0fcb-1797-4714-83e5-56ad4f68afc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 261
        },
        {
            "id": "3124a8d8-7823-4323-814f-cd400c7121eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 354
        },
        {
            "id": "2810ed25-9058-44e4-bc30-5b2e7af2a36e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 355
        },
        {
            "id": "96f1e41a-febd-4b7e-8b32-64e0efb0dfdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 356
        },
        {
            "id": "90639930-04c2-41ef-ab8a-dcdc6d7c66b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 357
        },
        {
            "id": "dd12ef75-33da-47a6-9afb-5c42ff1d4713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 358
        },
        {
            "id": "fa8348b6-ca96-4206-aa89-be7bef4aa746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 359
        },
        {
            "id": "ee926546-b87a-4ebd-a0c1-30fad3e5690e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 538
        },
        {
            "id": "7adbf953-bf81-4be7-ab75-8ee5f69c4e5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 539
        },
        {
            "id": "7fee5ecb-134c-4fa7-9be6-f82f134f2165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 34,
            "second": 8216
        },
        {
            "id": "a5f056a7-1384-4004-97a0-b02794072255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 34,
            "second": 8217
        },
        {
            "id": "c9536623-a2b3-4eb9-8bea-998be409976e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 8218
        },
        {
            "id": "125dc388-618f-4dfe-9421-23c1b98fecd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 34,
            "second": 8220
        },
        {
            "id": "69ae4d55-50f4-4352-9c81-4a65d5ccc4d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 34,
            "second": 8221
        },
        {
            "id": "bc10c21d-8e80-43c5-9a72-788b6dac0acd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 8222
        },
        {
            "id": "972bd7af-d91c-436b-9cd9-c221e892decb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 34,
            "second": 8230
        },
        {
            "id": "ee3a1dd2-6b4f-4e95-90f8-00231bde5953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 84
        },
        {
            "id": "50458fd7-2eea-4319-8d7b-f30f3208f6ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 38,
            "second": 86
        },
        {
            "id": "dad21cfa-d8c4-489f-b98d-8635491d9519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 89
        },
        {
            "id": "d2d270d8-786c-45e3-8070-dabf50f36bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 116
        },
        {
            "id": "ff20fa41-8bd5-4178-ab62-332374b74a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 38,
            "second": 118
        },
        {
            "id": "8bf0ae65-a155-4028-b42e-fb8e2e6d012c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 121
        },
        {
            "id": "f98b20d5-05a8-4751-8517-939926407010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 354
        },
        {
            "id": "c94f7c1f-8094-4f5b-a31d-1c8a7db2cec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 355
        },
        {
            "id": "d4cd91a2-1f70-46bf-a01e-c55f7155578b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 356
        },
        {
            "id": "2970412a-ede3-4d74-9348-974540e56770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 357
        },
        {
            "id": "88f11a6b-a0b1-4f4e-9e0c-3311b87307eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 358
        },
        {
            "id": "58273fd1-f30e-4779-b3da-95ff69ff9d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 359
        },
        {
            "id": "d4228d3b-4db0-4a81-bf52-62047ea5b826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 538
        },
        {
            "id": "e90f0317-55b5-4120-97b8-ef4cb4f8aabc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 38,
            "second": 539
        },
        {
            "id": "4683db7f-1945-4e0d-ba40-12037617d83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 39,
            "second": 34
        },
        {
            "id": "5c0812e6-3ca2-4e27-9ef7-b81975ac048c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 39,
            "second": 39
        },
        {
            "id": "aba19ad7-7d51-4079-8754-dba4e63e912f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 44
        },
        {
            "id": "6998675c-04b7-49cf-9580-945f552d2b9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 46
        },
        {
            "id": "c415a6e2-b9da-4f21-98ea-a4018725043b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 65
        },
        {
            "id": "d6cc05c4-a76e-4023-b910-5daf36444bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 74
        },
        {
            "id": "af50ddda-cbbd-416e-a920-f2e0a0f888bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 84
        },
        {
            "id": "55ff0458-6cd1-41c7-a9c8-5bc85e2ecc2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 97
        },
        {
            "id": "c3d447fe-36ba-4b7e-bab3-47c50cfd154a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 106
        },
        {
            "id": "1aa8a823-9e99-4fa6-899b-08accbec7595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 116
        },
        {
            "id": "a096e4f2-9f22-494e-a857-16523fe9f6b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 192
        },
        {
            "id": "57e9303f-5539-42c6-9e7a-af85a2f5b4e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 193
        },
        {
            "id": "833a89a8-9d2c-4be7-bd26-99de7109d788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 194
        },
        {
            "id": "d1aa4bab-aee9-4a48-ac27-cf9e3da6ba72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 195
        },
        {
            "id": "461c4492-01ac-4b84-9461-de19cff76384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 196
        },
        {
            "id": "1affd912-4a79-42aa-a8fd-b06a5589f07f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 197
        },
        {
            "id": "b9ec906f-cede-4521-b47d-d03b2799b3fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 198
        },
        {
            "id": "5cbcdabb-51cc-472c-a1b3-5cdafdbdb862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 224
        },
        {
            "id": "7e7438c0-8d0a-4398-80e8-e840cbafa02a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 225
        },
        {
            "id": "7e8110e7-e543-47a8-aa65-ddd2067b87dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 226
        },
        {
            "id": "d3a260ae-5081-4c6e-9bca-65561f0e3eff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 227
        },
        {
            "id": "71b9eabf-878b-46b1-90f1-ca0120d5b053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 228
        },
        {
            "id": "3c524842-e462-4952-a701-bff84973bf79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 230
        },
        {
            "id": "8f33b033-b873-44a3-869a-33a7df9d49a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 256
        },
        {
            "id": "e413b94c-278b-4088-ba40-f23f6e79a6bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 257
        },
        {
            "id": "fffac2a2-063e-4bc2-86d6-5f5fc6f5815d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 39,
            "second": 8216
        },
        {
            "id": "29f3fec9-6bb9-44d9-bb39-d5660a6aeb8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 39,
            "second": 8217
        },
        {
            "id": "97652bb0-669a-44ef-88c4-63f050241883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 8218
        },
        {
            "id": "3db8c4b7-8a82-4e4a-89fa-ba48e4fe4e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 39,
            "second": 8220
        },
        {
            "id": "208ee334-a2fd-412b-8e32-40d14d449609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 39,
            "second": 8221
        },
        {
            "id": "1ea4f022-3a0f-4b78-a42a-fd0b8641bc7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 8222
        },
        {
            "id": "b5e10da1-8932-4905-8f03-c28cac44721e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 8230
        },
        {
            "id": "268a5ac9-5922-4273-8444-404cc6316f33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 34
        },
        {
            "id": "0c0acba8-0a82-4507-a7f9-90de73136282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 39
        },
        {
            "id": "aba1ab22-bf89-4e07-a0cf-c715ddde1723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 44,
            "second": 49
        },
        {
            "id": "9ad80da0-9c5c-4a96-b06f-6ea8b77281ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 52
        },
        {
            "id": "c1d34d55-1242-4734-bf1e-05da55e4cd73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 84
        },
        {
            "id": "5722772c-e405-40be-9265-d78a4cfabbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 86
        },
        {
            "id": "6650d4c9-fe18-4f47-b7e9-c34ac0fdbee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 89
        },
        {
            "id": "baa1210e-7790-4ef1-a280-3b0c4a06428f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 116
        },
        {
            "id": "288c4218-fc8c-469a-a90a-5ffd9a9dbb3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 118
        },
        {
            "id": "e907b3da-46d5-4357-9e49-a416f6506e18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 121
        },
        {
            "id": "e2aca994-c56b-4afb-a8c7-f76d47d02970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 354
        },
        {
            "id": "ddef6e17-9757-469e-be2b-baab55091e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 355
        },
        {
            "id": "9181d54b-9a7a-48cd-8b26-789d30d4a809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 356
        },
        {
            "id": "ac722af0-4414-468f-b407-2ce1026c931d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 357
        },
        {
            "id": "526e2119-9eb1-46f9-abcb-9b181088e865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 358
        },
        {
            "id": "d744e308-50ed-4c58-a513-548fe9a29db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 359
        },
        {
            "id": "6d0eb5ca-1ff1-4a31-8f50-1b3e08b01434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 538
        },
        {
            "id": "1802193d-df14-4d17-b3e0-f96a55576825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 539
        },
        {
            "id": "0aa97c15-b0de-4d85-b934-8fc6f75f2990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 8216
        },
        {
            "id": "61cebd83-e259-474f-8bdf-4453f24612c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 8217
        },
        {
            "id": "3e405d50-d844-4ab7-8986-69438fbc4560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 8220
        },
        {
            "id": "02a72a27-5129-4e4f-be13-a34b66b4d61c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 8221
        },
        {
            "id": "88d1878a-583e-42f5-8f76-12cdee170435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 45,
            "second": 74
        },
        {
            "id": "e7534ad5-9512-413d-93d7-7705933aa4a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 84
        },
        {
            "id": "8aeddad2-24da-4bd6-ab01-d1a1e7ecbbc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 86
        },
        {
            "id": "f1b04b81-1da2-48b5-88ed-dfd462e15f5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 88
        },
        {
            "id": "cc68a993-bd8f-48d1-8302-e488d6aab589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 90
        },
        {
            "id": "65e1fe9a-54a7-4649-9eb0-dfc777d4f7c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 45,
            "second": 106
        },
        {
            "id": "fe23fc7d-3298-4d2e-b0f5-b2f3adf3b9c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 116
        },
        {
            "id": "54e9089e-16b9-43f0-beda-a6d49484d761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 118
        },
        {
            "id": "93b75aaf-e5a3-45f6-8cd2-d5a3cb944789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 120
        },
        {
            "id": "33310d2d-dac3-47d0-81b8-0b93f2541a60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 122
        },
        {
            "id": "56ea1f52-62e8-49c1-97e8-a5e96316aff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 354
        },
        {
            "id": "e5554536-ec82-452d-9677-c509b6c541c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 355
        },
        {
            "id": "57de40ef-947e-43e8-aa78-75dd3f490f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 356
        },
        {
            "id": "7cd98446-2f4a-4f3c-8562-756672f2ed91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 357
        },
        {
            "id": "96d6a685-88ae-43fb-81e9-0a1e65c0f04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 358
        },
        {
            "id": "dfd48e01-947a-4cab-afbe-94d3ab7b1cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 359
        },
        {
            "id": "869c834f-3c5c-4486-99ea-2d1854078b5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 377
        },
        {
            "id": "40a7c322-cc51-4382-a136-f852ca5312a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 378
        },
        {
            "id": "c1a3b756-d227-4bd6-af17-d62d56cb8910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 379
        },
        {
            "id": "ae421a82-7288-45bc-af49-ecd16d0e2bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 380
        },
        {
            "id": "f9fb79df-c94f-4c7d-bc7c-45fe9e69446d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 381
        },
        {
            "id": "fd4e2c0c-e42e-4516-9af3-2acb1d66c526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 382
        },
        {
            "id": "0519ca7a-8873-45e2-85fb-bfb3374b1321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 538
        },
        {
            "id": "717367d0-0516-4ae5-a89a-b85525ec4113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 539
        },
        {
            "id": "811a8bed-e6f1-49b6-a4aa-ad262e8e4592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 34
        },
        {
            "id": "752497f1-0c21-4eb7-a71b-46d5cff84246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 39
        },
        {
            "id": "ba673423-8e77-4b8d-9f57-5420759111b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 46,
            "second": 49
        },
        {
            "id": "a5335327-7a45-42a2-8ab4-bc94ddd254aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 52
        },
        {
            "id": "a1a0ee16-cc28-4a43-b856-7612b9b2362e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 84
        },
        {
            "id": "3cab3926-f800-40e2-bcb2-b51b69c51b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 86
        },
        {
            "id": "81fbbb46-c213-45b1-9439-e8a0cc5a34e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 89
        },
        {
            "id": "5c142bc0-80c8-417d-8ec6-bfe96f914dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 116
        },
        {
            "id": "ceb02ef1-a296-494e-aace-e0c23ae1eb06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 118
        },
        {
            "id": "04ec5ed0-54d1-4caf-ad9b-5ea5c7779859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 121
        },
        {
            "id": "28cd40e7-5dc4-4354-8575-0c518d15ca05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 354
        },
        {
            "id": "cbb11374-361d-4e84-9459-4814ed05cd1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 355
        },
        {
            "id": "b621ed7b-394a-43a8-ae96-1d0ffe9fee6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 356
        },
        {
            "id": "9afbea76-09c8-4a0d-acc5-9fd38b39c775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 357
        },
        {
            "id": "c8686231-1e54-48bd-9943-e3eee39f3e91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 358
        },
        {
            "id": "1405106d-325d-40e3-a474-c81389de89ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 359
        },
        {
            "id": "aee35a7d-9367-43b8-841e-8065adfaccd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 538
        },
        {
            "id": "141b2376-17f3-4d6d-b4c3-4b1de9aa796e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 539
        },
        {
            "id": "d17fd61d-0891-455d-b999-526baeb7c2c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 8216
        },
        {
            "id": "993d84e4-c0b7-40d8-8d14-8cd5d67a0281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 8217
        },
        {
            "id": "fa3155eb-dd3d-4fa7-a760-7eda3c457b99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 8220
        },
        {
            "id": "16093065-ba1b-4d83-bb46-42cc28310b32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 8221
        },
        {
            "id": "6546f971-37be-4537-85f0-2d22d4d9c301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 48
        },
        {
            "id": "c801483d-cae4-4ebe-ac94-f13fe06ce804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 54
        },
        {
            "id": "03086360-887a-4138-b8c9-97b582e2f3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 56
        },
        {
            "id": "d2e884f3-2256-4597-b8a9-3be272e3bb46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 57
        },
        {
            "id": "25f218a5-f924-4e1c-a29a-0c5a9fe5aa3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 65
        },
        {
            "id": "f0394033-fd3b-412e-8cb1-c2f67159d749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 67
        },
        {
            "id": "53dcb7e6-778d-4f3d-a7a3-1c21a5c3ae3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 71
        },
        {
            "id": "5a450ace-c947-4d7a-89be-31cf43d2c9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 47,
            "second": 74
        },
        {
            "id": "e5b42f9b-a1de-4763-b4c5-992f943aa07d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 47,
            "second": 77
        },
        {
            "id": "34a75892-a5a6-4265-9737-4ca0bdb05133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 78
        },
        {
            "id": "707ecd28-e533-49e9-8622-7497d17911f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 79
        },
        {
            "id": "00ea4ba5-edb2-46d8-840a-4294382fbc92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 81
        },
        {
            "id": "07dca3a4-bc9e-4c27-a78c-5cfdf4fd3da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 83
        },
        {
            "id": "8e0b7572-fb3d-4ff8-aa49-30f8e478092b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 97
        },
        {
            "id": "404542c0-b9e5-4471-b2de-e0c9f2b1ebae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 99
        },
        {
            "id": "73d92de1-9481-4181-8818-146895f0a0c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 103
        },
        {
            "id": "b9a60523-945e-4149-8504-7ac216a9579e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 47,
            "second": 106
        },
        {
            "id": "c08ffba1-f827-4ecc-93b0-c1e9dbf270f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 47,
            "second": 109
        },
        {
            "id": "4e50e5fd-d9e9-4a86-a861-3cd973d3b62c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 110
        },
        {
            "id": "1ace6a04-822c-4116-a05e-e46c9fd11216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 111
        },
        {
            "id": "c9cde38e-730f-4697-9e53-8eda22704b2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 113
        },
        {
            "id": "638dbf82-1e52-4f84-a3c7-039e624d856f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 115
        },
        {
            "id": "23dd77e1-4b5f-4c21-8df9-df8bebd15773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 192
        },
        {
            "id": "38c5f7ac-39a4-4af4-9de6-ac5dec9c0f6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 193
        },
        {
            "id": "c304c683-9b07-4517-9150-905b27f256f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 194
        },
        {
            "id": "9fede647-64d1-4fa6-9cf5-1863e196785f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 195
        },
        {
            "id": "e5eaa51c-d6e7-4cbb-961a-d330cd5e588c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 196
        },
        {
            "id": "18a17cfa-ff55-45de-b604-776919b79c01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 197
        },
        {
            "id": "76dcc46c-1903-4468-8429-da8ba84ce108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 198
        },
        {
            "id": "ff063617-51b6-42b3-a6ae-a69a66e0e9f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 199
        },
        {
            "id": "708980df-849f-45e8-851e-014588b2cebc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 209
        },
        {
            "id": "a28194c1-501a-4096-98a6-48a64b0e836d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 210
        },
        {
            "id": "549cbfa6-029a-4029-aca3-a185b834f795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 211
        },
        {
            "id": "9db76985-5224-47b9-93ae-8a49dc2f6dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 212
        },
        {
            "id": "ca16198f-ea69-4d77-9ba6-ade8d1234725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 213
        },
        {
            "id": "bcd3cd23-3ca8-4b07-b6bd-db45323efba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 214
        },
        {
            "id": "c3a0c428-afdf-4833-ab5b-5158bc661271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 216
        },
        {
            "id": "272c55cc-9c97-4034-8939-9d8993461669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 224
        },
        {
            "id": "64a8d8c6-9af1-493a-b862-ecae320361dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 225
        },
        {
            "id": "0e452ba0-e170-4841-8157-5832f65ffb92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 226
        },
        {
            "id": "1e14a557-2c48-45b9-845d-4713250bd702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 227
        },
        {
            "id": "bb10f74d-cbbb-40f9-92c0-831c48ded2a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 228
        },
        {
            "id": "6986dba1-fb08-47b2-b127-b47698cbdf35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 230
        },
        {
            "id": "3a7e3afc-6c45-405b-b10c-ffaf28fedd86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 231
        },
        {
            "id": "efa03226-981c-4098-a70f-1f8745288447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 241
        },
        {
            "id": "a9ee991d-3e4e-4b35-9ece-513b3c506404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 242
        },
        {
            "id": "dbed2e79-9ce5-4a97-a1aa-bdfe4da3e511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 243
        },
        {
            "id": "3c667a70-a814-4afe-8d43-38f2785a5c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 244
        },
        {
            "id": "5dad5c96-428e-477a-bf32-1b07238e9277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 245
        },
        {
            "id": "80e944df-51cf-4c5a-99b5-2bfbac60ee7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 246
        },
        {
            "id": "b236cb83-2dd1-41d4-93a5-11de4967c332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 248
        },
        {
            "id": "d8cc1376-180b-44e5-ba3c-36a65a3f0b7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 256
        },
        {
            "id": "ae8ecd89-82ad-48fa-a3f1-6127dc5c13ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 257
        },
        {
            "id": "e6369d3d-fa27-4dd3-b18c-91b7dbcad94a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 258
        },
        {
            "id": "48a05773-f895-4867-a286-fbe7745a57a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 259
        },
        {
            "id": "c808b5a8-dd79-438d-966a-3e1bb7560aad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 260
        },
        {
            "id": "ead15ed8-2cb6-47ca-8482-cd7f447fdeba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 47,
            "second": 261
        },
        {
            "id": "856f2a1a-9398-44b6-b1ab-52b1b6482b52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 262
        },
        {
            "id": "76f4a059-4298-4769-886b-2be3c5af965b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 263
        },
        {
            "id": "2a03f2ef-ff46-400e-83c7-db0ee4ea5352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 266
        },
        {
            "id": "b05d9c00-9bd2-44da-9b02-9c79b29e806f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 267
        },
        {
            "id": "b31c950c-bef8-4e2a-ab69-236f7a1ae307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 268
        },
        {
            "id": "2a326f91-231f-4d70-b16c-ca89c07b44a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 269
        },
        {
            "id": "94707270-7a9e-4bd5-b30f-2363abb4c750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 287
        },
        {
            "id": "df087b04-9892-42f1-95b2-6af45900e2db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 288
        },
        {
            "id": "bb432f67-c4ef-46c2-b96c-1c0d4b650660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 289
        },
        {
            "id": "4406c161-5b7e-4282-9388-94a767421ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 290
        },
        {
            "id": "80c75643-4792-402a-ae08-862b816bfbc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 291
        },
        {
            "id": "bdd500aa-8905-4c77-a249-4930bfce39c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 323
        },
        {
            "id": "c3fe4d16-aae0-4ff8-a1af-c9ca53bd3879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 324
        },
        {
            "id": "f9cad455-c9eb-4d8e-a4ff-d687a835176d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 325
        },
        {
            "id": "145bea44-1419-4877-839b-7cc71fac29c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 326
        },
        {
            "id": "0d6374c8-e001-41b1-849d-aa00c1dcb7c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 327
        },
        {
            "id": "9c189620-c0a5-4b5a-888a-ac4b4aeccb03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 328
        },
        {
            "id": "9375b463-32de-40f9-946a-068be290e5b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 332
        },
        {
            "id": "f4781003-e87b-4e8a-bc09-d487a85b7a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 333
        },
        {
            "id": "0535fc45-986a-40b8-94b3-a9c76ca0dac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 334
        },
        {
            "id": "94c9df0d-c7c7-4cd5-8932-ea70d073ccff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 335
        },
        {
            "id": "7ba6af83-f48e-454f-8bc4-3f22aa0285ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 336
        },
        {
            "id": "49c4e32d-c85c-4cf0-8dbf-9d0b64e6575d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 337
        },
        {
            "id": "248bb156-2c57-4be9-b389-117c187b0b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 338
        },
        {
            "id": "e33c9eb3-d452-4317-beaa-2e472254f569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 339
        },
        {
            "id": "351da268-dacc-4bc6-9f2d-6646a5a2082e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 346
        },
        {
            "id": "881db14c-8f1e-4767-a474-947046121c9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 347
        },
        {
            "id": "9cc65bd0-936a-41b7-b3f2-1784c01534d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 350
        },
        {
            "id": "099c7eba-1ecd-41d0-a58b-ef89b9cd377a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 351
        },
        {
            "id": "378e0664-ffc9-4dd3-8f39-06b620071e46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 352
        },
        {
            "id": "193f31a0-7e00-4067-9a59-0cddf395e932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 353
        },
        {
            "id": "92d837a8-5fcb-406b-9c81-215544e3b1a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 536
        },
        {
            "id": "c967305c-c735-4df8-bc9c-076aa8da88e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 537
        },
        {
            "id": "14b8cab7-19ba-4be2-a2ca-cb228649fa2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 48,
            "second": 47
        },
        {
            "id": "1a141c94-8490-41aa-9449-978d6c2b5933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 55
        },
        {
            "id": "b6109340-7e0f-4d90-a294-82b3bd7c50e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 86
        },
        {
            "id": "2623623c-54ff-435d-be0f-0aeb807d8149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 88
        },
        {
            "id": "30901423-5a0b-4dab-98eb-f1e751d85ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 118
        },
        {
            "id": "7f8f7c2e-d2e4-478e-ad42-c53918c73a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 120
        },
        {
            "id": "23725c36-0b47-4cda-8af3-3ba50ffb83b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 55
        },
        {
            "id": "308eb68b-b7a2-472e-b0f7-7cbddd09147d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 49
        },
        {
            "id": "48001963-9bd3-473f-9948-acd1892dfc4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 55
        },
        {
            "id": "792f38af-ced3-4321-a7ec-80bf12aea244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 54,
            "second": 47
        },
        {
            "id": "47ed59c9-24ea-4b92-8324-a918236e5240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 55
        },
        {
            "id": "5329f944-c470-4854-8261-2cbce77c238c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 86
        },
        {
            "id": "6900f96a-4dc9-4876-8c98-000b8e206c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 88
        },
        {
            "id": "861ce03b-344b-4ab2-a3ba-d0ae83f3cbe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 118
        },
        {
            "id": "226dec16-df20-4e94-9180-d7fdffd16747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 120
        },
        {
            "id": "dd9bf80c-325d-4d48-bdef-329dd085a3dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 55,
            "second": 44
        },
        {
            "id": "3cd86ac4-8d75-47eb-9a03-e0d5d55f9440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 45
        },
        {
            "id": "28593af4-e066-4f73-858e-ee03c5f21f4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 55,
            "second": 46
        },
        {
            "id": "9c6f9cfb-6c39-4c12-b7c9-5d1075b40820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 50
        },
        {
            "id": "20955209-7002-4f38-828d-1805bb2bc00d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 8211
        },
        {
            "id": "47fdfff5-53f8-452b-a2f2-50a64ecac83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 8212
        },
        {
            "id": "5bc071f0-47b7-4526-84f5-14478825eae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 55,
            "second": 8218
        },
        {
            "id": "c7a14939-4e0b-43eb-88e3-c4cd976274e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 55,
            "second": 8222
        },
        {
            "id": "15347f73-c7b4-4770-afbd-4e902c73fbf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 55,
            "second": 8230
        },
        {
            "id": "7a8aae6c-73e2-4ffd-83a8-057de4a4a58b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 56,
            "second": 47
        },
        {
            "id": "2bf56576-9a3f-4670-98d1-62e8651d1720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 55
        },
        {
            "id": "3eb39836-b94c-4788-8184-f93114e2990d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 86
        },
        {
            "id": "7fbbd881-4dd2-4768-880f-0d90ee359886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 88
        },
        {
            "id": "022700e1-55e4-46cf-8b85-ba90419c3225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 118
        },
        {
            "id": "acb40a71-b202-4a06-b958-2ed66ceb1c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 120
        },
        {
            "id": "8e37a5bc-9693-4e97-b7d1-5e448bd05141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 57,
            "second": 47
        },
        {
            "id": "6e47e5f4-76a6-4333-bd59-014384b3e476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 55
        },
        {
            "id": "25c78aee-e435-4fee-b1f2-e90b04919a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 86
        },
        {
            "id": "ee0ec361-6776-40c8-8a9f-fbfe50fa6a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 88
        },
        {
            "id": "a8ee5077-1c9e-48b0-bb84-4243cddd4e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 118
        },
        {
            "id": "581670f0-ebaf-42c1-b7d5-7924f97bd844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 120
        },
        {
            "id": "dd9fc7cf-0f21-4c59-9b0d-d3e835995505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "d004ccfd-0961-414e-b48b-c2dc3e49ac24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 118
        },
        {
            "id": "80fcf1f7-067b-4c65-87d7-ac5122a67877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 66,
            "second": 47
        },
        {
            "id": "e3d73f83-98df-4f65-bf2f-87821bf0c727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 86
        },
        {
            "id": "2c0629ef-ec2f-498b-ade4-e15077376d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 88
        },
        {
            "id": "493f5677-1f8f-4ff7-a5de-b92a78422a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 118
        },
        {
            "id": "1cdb7f01-33a9-47d9-a1d3-6ef69606f758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 120
        },
        {
            "id": "e5417369-68e6-4a45-bf86-25f801306c9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 67,
            "second": 45
        },
        {
            "id": "5d1cad61-488e-413c-b9ef-20cf9720dcca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 67,
            "second": 8211
        },
        {
            "id": "0dc97afe-2ed4-4b2f-b725-726539c0f6bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 67,
            "second": 8212
        },
        {
            "id": "04f6551a-1ebd-428a-82fe-881cfd1d1f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 68,
            "second": 47
        },
        {
            "id": "6765230c-e733-41f8-8a86-f9100eee0160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 86
        },
        {
            "id": "9709261a-2876-4f61-ad53-7d10ef490727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 88
        },
        {
            "id": "ed9cfe27-5e5a-4125-9b86-f60e3d48a6bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 118
        },
        {
            "id": "2f643c6b-5d6f-4fad-92ed-ca951aef8f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 68,
            "second": 120
        },
        {
            "id": "d8294829-73d4-4a9b-bd10-45311f641023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 45
        },
        {
            "id": "46220a90-ebf9-46e7-b05f-45c9c8f784ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 8211
        },
        {
            "id": "af6963ca-c3c4-4f23-91a7-032fa3c9427e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 69,
            "second": 8212
        },
        {
            "id": "ab11af59-6cab-4884-be7c-d4335d0cab8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 44
        },
        {
            "id": "d1ae6808-92f2-4f3c-b2b2-6bf7b186676a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 45
        },
        {
            "id": "3146f208-ac7e-413e-b5f8-77f2ea4f4b4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 46
        },
        {
            "id": "47ca3ee0-2f94-48a1-9d98-0551aedd011f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 70,
            "second": 47
        },
        {
            "id": "6c3c7ee8-0313-4f28-8887-b0b6dd133b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "4de4a2ca-766c-49aa-8203-6777e98b61b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 74
        },
        {
            "id": "1fb47d30-6eea-4c78-bf18-b2e99705f18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "33e26950-15e0-4457-acff-b7e19fff5313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 106
        },
        {
            "id": "bbf7b93f-df20-47ce-82c9-480002174230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "09e6251d-105d-4ffb-a1f7-c0e39f265f7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "1dee6006-557d-4a6c-a261-419153a8c21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "f09749af-f5b0-4617-88f2-6151e19859e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "b82ea82e-c622-4967-ba1d-f7166eeb58ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "8c18bd86-7dd7-44b0-9f18-fee673da068b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "9ff3491b-4c3c-4ec6-ab61-b0cd74b1b110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "8a4d5f3b-b688-46e0-a571-d1c74fcd72cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 224
        },
        {
            "id": "69039753-3845-4694-82f1-15a7327d1f0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 225
        },
        {
            "id": "0801bf37-56da-4683-8e97-c1739b067df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 226
        },
        {
            "id": "41fb290d-2ccf-4bdc-bfa0-c88a6feaf022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 227
        },
        {
            "id": "4f3b25c9-faa7-4cbc-8af0-95b54e21a013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 228
        },
        {
            "id": "c5461180-fc62-447d-af1d-1ce7ceaf27ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 230
        },
        {
            "id": "7b792190-e0cb-4eb4-9475-9344bc6233a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "3418cd46-95ce-4735-a47a-94b7288a5ae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 257
        },
        {
            "id": "b94e6ef9-9068-497d-862b-f385340fdae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "ec5a5833-5b0d-49a3-862c-1bf16ef66871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 259
        },
        {
            "id": "4d431611-a8dd-4ce2-b02e-c7f7bbeaad7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "497eb3e5-4285-4aab-b179-c2760293c0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 261
        },
        {
            "id": "ccca2d7c-665d-434d-8ee5-0681163fd4b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 8211
        },
        {
            "id": "1ae4e4a1-f4c9-4210-9daf-f12ef6a69858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 8212
        },
        {
            "id": "1672c467-2ca2-4abf-86fc-db7b12aa1008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 8218
        },
        {
            "id": "34357d0b-8fa6-4cbf-acc9-a7cab6726d1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 8222
        },
        {
            "id": "e0bf7a2d-259f-41a4-a278-606d48387eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 70,
            "second": 8230
        },
        {
            "id": "42dd0d0a-dfb7-40f9-9ffa-af667decfa4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 44
        },
        {
            "id": "b53dc749-eb24-4d1b-a3fe-99c91d59bc13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 46
        },
        {
            "id": "32903f40-a44b-4ee5-9637-6e6fcac72439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 74,
            "second": 47
        },
        {
            "id": "ad0c5e05-1584-4c8a-9d04-3eb4957e35a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 74
        },
        {
            "id": "780b4fe7-141f-4d3a-871e-bdbfdb2551c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 88
        },
        {
            "id": "112650e4-ab62-4016-b5c4-9f29bb2b6584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 106
        },
        {
            "id": "b24b3dea-6205-4d39-859b-2efbcb57a6aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 120
        },
        {
            "id": "c76e73cc-3d6f-4444-936b-62651e109d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 8218
        },
        {
            "id": "de199353-1545-48c8-8eb6-1955bd0d53ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 8222
        },
        {
            "id": "4bb5ed3d-43b4-413e-885e-6e536e163dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 8230
        },
        {
            "id": "de52740f-91dd-4313-8c84-a2b291aea8eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 38
        },
        {
            "id": "eb4a6b9b-5c14-49b7-bd6d-9c3c3b050358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "4216fcf3-7ba2-4b5e-be99-047d19100cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 75,
            "second": 45
        },
        {
            "id": "b64636b9-a90a-45a9-b063-6e4530a10007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 46
        },
        {
            "id": "301796e0-6027-4a6e-a57b-1331ba0ef87a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 48
        },
        {
            "id": "aaf791f4-384b-4311-bd12-7d0a4b16147c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 54
        },
        {
            "id": "bd2c06ee-3e3c-493b-8657-1b619adcd837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 56
        },
        {
            "id": "e9c9583e-c74d-4bb6-b913-87407a6ed795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 57
        },
        {
            "id": "e0d9cbd1-2a01-4b4f-91c4-f8d116cdc9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 67
        },
        {
            "id": "77b11886-e090-4c35-8c67-b4e6c6735586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 71
        },
        {
            "id": "65302446-010c-4aa0-be57-e49a040d8a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 79
        },
        {
            "id": "df6cb788-279b-4b78-8faa-1808e726b03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 81
        },
        {
            "id": "100aa9f2-2ec1-4e99-a2e5-b70410a32664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 99
        },
        {
            "id": "8cbc4739-c0ca-44f2-bb3b-fd085b4bcd9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 103
        },
        {
            "id": "bb77593c-8247-4c81-83ee-432d200ec134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 111
        },
        {
            "id": "bb24a910-945c-4eb8-a52d-24c9ef08f2cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 113
        },
        {
            "id": "bdc71481-0630-4f4f-8a9b-047787f801af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 199
        },
        {
            "id": "6e0cf044-83ac-4608-9f05-9ff341924fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 210
        },
        {
            "id": "70b457a8-49c8-49d9-b553-1aeb78fed861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 211
        },
        {
            "id": "3eccc328-de52-4600-a9a3-39bfa59c6522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 212
        },
        {
            "id": "924ef9d0-afe0-49c5-9592-b10a004bbc83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 213
        },
        {
            "id": "60aba275-a35c-4f04-a45d-fdf7c2eda934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 214
        },
        {
            "id": "92655e00-4c6f-43a4-ade1-dbcf1106510d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 216
        },
        {
            "id": "434811e4-57b3-4176-81db-24a05f9b426c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 231
        },
        {
            "id": "8fe6a404-62f4-48a5-a536-8fd8fcaf9a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 242
        },
        {
            "id": "8509c053-13c8-4131-adb9-5e6b6f0e7cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 243
        },
        {
            "id": "592d4b33-9cec-41fc-88df-d6fca2fa6e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 244
        },
        {
            "id": "0f48eacf-d840-4f7d-b3c1-1a7e19d0a115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 245
        },
        {
            "id": "ca59f70c-d4d6-4934-b498-d33d62cc3d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 246
        },
        {
            "id": "42d19357-adc9-4e08-b4b5-bd90428d51bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 248
        },
        {
            "id": "c9912786-8e42-4b2c-ad8b-10da237fabb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 262
        },
        {
            "id": "29dbe41e-d347-4377-b2cd-cba1f89c2afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 263
        },
        {
            "id": "7c140936-a5c9-45a4-87a4-3deb126f167f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 266
        },
        {
            "id": "61ce96e5-469a-4f62-b95e-a94ba1ea93c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 267
        },
        {
            "id": "0f9bee43-945a-4d20-a5d2-5a720c822bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 268
        },
        {
            "id": "96091686-f0e5-4393-9bfe-fb9ae2b29b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 269
        },
        {
            "id": "99651228-13f5-4b20-a196-6177b696c047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 287
        },
        {
            "id": "0a7475c6-dd76-4a73-9b8b-2a184fcfb401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 288
        },
        {
            "id": "083a376a-10b0-4c71-ab6f-1bc36a2cc3ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 289
        },
        {
            "id": "adf79a3f-e15b-45d0-b846-a790acf6585d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 290
        },
        {
            "id": "e891d393-2e57-47c4-a537-014abbbf97e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 291
        },
        {
            "id": "15a00144-3105-4c67-a961-1a32c29dbcb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 332
        },
        {
            "id": "c64a5cf8-3885-4cb9-abe7-4ab99cb19f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 333
        },
        {
            "id": "5c20ed19-747f-4b8d-bb4c-7afe51444197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 334
        },
        {
            "id": "cbc9dcfa-1cae-4956-896c-6cf3d12c42bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 335
        },
        {
            "id": "d42ddfc8-9d77-4a38-b43a-ff9647605389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 336
        },
        {
            "id": "72027129-10f4-4502-9fb1-a9d090c8b212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 337
        },
        {
            "id": "0676a38c-93ac-4438-a684-c6104f45ce77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 338
        },
        {
            "id": "a496c31e-dd66-4c98-9372-bef11071d348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 339
        },
        {
            "id": "2e415385-5878-4921-8571-d1f3fc2a29d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 75,
            "second": 8211
        },
        {
            "id": "2860b979-0a64-4989-9bc5-c5b98f83eca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 75,
            "second": 8212
        },
        {
            "id": "84257293-4ea5-40a1-a70e-4f13acc84724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8218
        },
        {
            "id": "89587d17-4cb4-40d3-858d-fd81481b8310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8222
        },
        {
            "id": "b322f98d-8ae4-4a87-ab13-9faf9ee254fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8230
        },
        {
            "id": "fd749390-0784-4cf6-b625-1a855b0ad0f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 34
        },
        {
            "id": "10f2eb07-6cd8-40b7-ac01-a565259970e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 39
        },
        {
            "id": "24b33cbc-3d64-4b82-87d6-b1e51c5eae5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 45
        },
        {
            "id": "425779ca-632e-4d82-ba6d-a64e6992f126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 63
        },
        {
            "id": "9734261b-2eee-4115-a208-a20dde0f2b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 84
        },
        {
            "id": "3ca37a5d-fbcb-47cb-a554-3afc13b1a169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 86
        },
        {
            "id": "ab93b30a-a34f-49c9-8720-ea7b65b06330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 89
        },
        {
            "id": "a8f60366-a511-4615-85a4-ca1a525bafa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 116
        },
        {
            "id": "57b84b6b-0486-4da5-8dd0-1012afe031a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 118
        },
        {
            "id": "1e32663e-9b82-446d-b9aa-3a4241230d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 121
        },
        {
            "id": "6abdf2e0-cdc3-418b-8351-f35a0d0dd34b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 354
        },
        {
            "id": "661eb385-973b-407c-8f47-3a994d8ca23a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 355
        },
        {
            "id": "80d6e4b4-bc05-45c7-b961-ef6b0925d1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 356
        },
        {
            "id": "8126307b-a805-40ac-b7dc-735b9830e27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 357
        },
        {
            "id": "778c33ac-c73c-42f7-b2d1-426ff3bcbc20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 358
        },
        {
            "id": "c42a9f35-c333-4c5f-9b35-a96c97bba71f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 359
        },
        {
            "id": "2bb407be-067c-45ee-943c-bd0733a77d52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 538
        },
        {
            "id": "9f0f3c20-e51e-48fd-ae50-0ff256593cfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 539
        },
        {
            "id": "8e53e02f-cc7b-4f95-977e-c84e3ea3c44e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 8211
        },
        {
            "id": "dd2b83df-440d-4eeb-9b3c-79b02588a72f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 8212
        },
        {
            "id": "6aa45557-2e54-4f49-8340-9d5b1b1927fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 8216
        },
        {
            "id": "16932378-6e94-4e36-b78f-a27bfcafbfb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 8217
        },
        {
            "id": "ee8d5b88-a7ac-4b36-9a07-d1cd9b5984b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 8220
        },
        {
            "id": "542138ef-80be-49ef-9478-678138b8ca9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 8221
        },
        {
            "id": "b56ffa55-de6c-4ccb-9091-c5a928cbd621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 86
        },
        {
            "id": "ccb74f57-2a6c-4977-a551-a3529de6c385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 118
        },
        {
            "id": "18c883b7-90cd-4653-86fc-4611695273c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 47
        },
        {
            "id": "b0391ff5-b33b-4d72-ad4f-4744289350f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 79,
            "second": 47
        },
        {
            "id": "c7d655b9-d762-435d-9b3c-5120702501cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 55
        },
        {
            "id": "91b66ee0-e64f-44ec-b55b-f31dc99c9db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 86
        },
        {
            "id": "269c1089-e390-4fd1-a5cb-8a6cce57fd0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 88
        },
        {
            "id": "169391c7-ca28-4bbb-9694-816e532124f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 118
        },
        {
            "id": "97d8e33e-f3b9-4e46-a922-3a2c4a988a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 120
        },
        {
            "id": "8731c2ae-3b9b-4fb1-8baa-c5e2c4d06bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 44
        },
        {
            "id": "ee5e5499-485f-4071-a86b-b4ca96e489ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 46
        },
        {
            "id": "be2f328c-60fc-4870-8d5d-67408518d571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 47
        },
        {
            "id": "e5009178-c3cc-429b-948b-5a6f3993fe1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 80,
            "second": 74
        },
        {
            "id": "1a63f81c-a592-4084-a6a9-ec1dc0650ea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 80,
            "second": 106
        },
        {
            "id": "7add4e31-a892-40bd-84c9-20f75c4630eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 8218
        },
        {
            "id": "d6235171-9c84-4ec2-be19-db20d420b31b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 8222
        },
        {
            "id": "7f096e66-a5d1-48a4-8d21-0e359f92ece7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 8230
        },
        {
            "id": "b7b57b4a-a74c-4ea5-b7e7-a962d3506b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 81,
            "second": 47
        },
        {
            "id": "370c1964-a7ef-41ac-af65-f075312ba528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 55
        },
        {
            "id": "779474ed-4c85-40e5-9507-be6747c3f164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 86
        },
        {
            "id": "cec9a94a-ab74-4064-930c-a4c80a021751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 88
        },
        {
            "id": "07094bdb-c5d9-4047-916e-afe412d2e477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 118
        },
        {
            "id": "ed49076c-223c-4776-b202-af7e0e979227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 120
        },
        {
            "id": "ed73a0ad-d411-4102-9fc5-6c24ebaf0a81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 48
        },
        {
            "id": "0362bc3c-230c-4382-bacb-178ac9d4f43d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 54
        },
        {
            "id": "610dc099-8168-45ab-becf-eef6a3290171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 56
        },
        {
            "id": "89f7de27-27c7-442e-be69-646cd4ea5a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 57
        },
        {
            "id": "8b4d522e-d27f-41e6-bcff-7f27a9fad999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 67
        },
        {
            "id": "d43cb3c3-769c-4092-8a70-4a0e0b59ee1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 71
        },
        {
            "id": "8d671f43-81ab-4ded-94ea-19fe81a17c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 79
        },
        {
            "id": "0dfbf4fa-0043-484e-8831-70f0532aa5cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 81
        },
        {
            "id": "8e75b5e9-701b-4510-9c00-ff0dd168381d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "dd330a21-69bc-4220-aa3b-a1ee323846e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 86
        },
        {
            "id": "37df6828-365e-4fbb-a5ab-3e46eccba2e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 99
        },
        {
            "id": "de3935ea-0620-4542-9a40-b80f0de5be9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "9d996d1e-fb40-4d64-b98f-6b627de1a564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "556d3885-ba73-4047-8a73-29105b107d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "595084b8-41cc-4add-9864-5f25e7625b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 116
        },
        {
            "id": "3b597d47-3b75-4875-907c-02b4a7f4469e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 118
        },
        {
            "id": "4b316564-fbe5-4967-904c-109f9c36b76e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 199
        },
        {
            "id": "bc4d522a-f536-4363-985d-85c080cfc87e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 210
        },
        {
            "id": "c489f5a2-3ce9-4908-8403-b734097e6c24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 211
        },
        {
            "id": "ad0d26df-414c-4744-956b-dd2f961bdd4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 212
        },
        {
            "id": "ac6ead4d-71c8-4004-a86e-5ddcdb1cf941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 213
        },
        {
            "id": "59c10a29-fbc5-4854-a399-a265001c6ea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 214
        },
        {
            "id": "c1a78ee8-6096-46ee-aeb7-c3e760b9447c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 216
        },
        {
            "id": "706d680e-7ad3-41ee-bf38-3e099f68fd68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 231
        },
        {
            "id": "d65ed6a9-142d-48e7-b4c3-6860480f996b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 242
        },
        {
            "id": "5a567a72-9d70-4cde-9e6b-e05bd462c234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 243
        },
        {
            "id": "b8a16a21-1772-4768-a5ca-3e4bacd8ea4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 244
        },
        {
            "id": "acf6c481-02e1-4cb9-b82e-c57c49289101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 245
        },
        {
            "id": "5c162524-50db-46f4-bee0-8deb8a4a71b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 246
        },
        {
            "id": "920079fc-8dc6-4788-a711-679076310947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 248
        },
        {
            "id": "098566a2-d310-435d-bbbd-4c3e5c2da06d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 262
        },
        {
            "id": "9813ca6a-a885-4891-af4d-e772cac873a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 263
        },
        {
            "id": "b76a3eff-736d-41a8-94fb-b0da672dc4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 266
        },
        {
            "id": "e3485562-547e-4293-83a5-0802d30bfb75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 267
        },
        {
            "id": "915a48ab-c26f-4ba8-9d1e-47650cc33538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 268
        },
        {
            "id": "681b404a-0078-4e5b-a088-f2a47036a70d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 269
        },
        {
            "id": "13e5d922-5cb2-4808-8025-ae5d14e4f016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 287
        },
        {
            "id": "8f4a19ed-e9b4-47c4-9154-a452327e8c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 288
        },
        {
            "id": "b5cf0c93-742c-4f04-acca-c3ca9577659a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 289
        },
        {
            "id": "0c41d6de-76a7-42be-ac83-cb788b79a31d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 290
        },
        {
            "id": "2e228aae-4fa5-4591-af7f-cc15e1c44188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 291
        },
        {
            "id": "04dff775-b371-4b95-bed1-66486bcdefb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 332
        },
        {
            "id": "46bc02de-4745-4d06-9828-9cf7723417b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 333
        },
        {
            "id": "516acf60-1eb0-4d47-a2e3-0ec790ba02de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 334
        },
        {
            "id": "1b05b9cf-2b81-4609-8769-7c9142d113e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 335
        },
        {
            "id": "e9c28f1e-c4e1-41fc-a2e5-17d50a7e54b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 336
        },
        {
            "id": "af04797c-3e2f-429a-91bb-4b77fc266646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 337
        },
        {
            "id": "8d6d831d-42a5-4fbc-8331-9de8b51033ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 338
        },
        {
            "id": "28adb401-79f8-41f1-be16-da39a52b05be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 339
        },
        {
            "id": "4b164387-995d-485f-a022-6d5f8224451e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 354
        },
        {
            "id": "824a8271-ea55-4c60-8711-f16164d83412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 355
        },
        {
            "id": "c11f9108-16c5-4231-b131-214e223548d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 356
        },
        {
            "id": "d8c9f843-9088-4691-b9e3-9904f4e9eff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 357
        },
        {
            "id": "b41ea6af-dd25-4cd2-8fd0-3bc70068144b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 358
        },
        {
            "id": "184e7370-471c-451f-b941-a333ca39cd65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 359
        },
        {
            "id": "4ebb1787-1932-4f21-a3e2-2de41ef7fc2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 538
        },
        {
            "id": "4d1c2417-29d0-4be5-9aef-9de943ea365d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 539
        },
        {
            "id": "7bb8444f-8f41-40d4-b771-0ff730c2e7ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 83,
            "second": 47
        },
        {
            "id": "65e38d4b-af3b-4b1e-9087-cb4845fa61d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 44
        },
        {
            "id": "3b458b07-b932-4bb2-8640-6da59345ebb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 45
        },
        {
            "id": "d7ee9f96-01ed-4d9a-beca-b8a5843733fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 46
        },
        {
            "id": "39244a5c-32ae-41f8-b075-81c334a677f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 84,
            "second": 47
        },
        {
            "id": "0e28e96c-0b57-43b1-89c3-7aaaa3c9cfaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 74
        },
        {
            "id": "8d818377-d6d4-4242-bd45-6361dc82e38b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 84,
            "second": 86
        },
        {
            "id": "8602d63e-3914-47c5-b164-88b2b3d2dc58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 84,
            "second": 106
        },
        {
            "id": "70b4969a-5fc7-4cc2-9ae1-5fc10ea6ad7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 84,
            "second": 118
        },
        {
            "id": "cb704e8f-e607-4cdc-a4f3-e5524af52dcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 8211
        },
        {
            "id": "7b922dce-beb8-43d1-8d03-feaf8c3aa39f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 8212
        },
        {
            "id": "908a018d-e3bb-4611-8c2e-9543f7d647e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 8218
        },
        {
            "id": "6909ef2f-fd6a-47c8-9ad0-5a32d2251982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 8222
        },
        {
            "id": "db40c4c3-1c6c-45b4-8e73-2aaba57b9d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 8230
        },
        {
            "id": "8597870d-5855-4166-a7e1-6b7b7849b0bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 85,
            "second": 47
        },
        {
            "id": "5228e0ab-f466-404f-87bf-d23ddd28ade2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 74
        },
        {
            "id": "cba1c1f0-446f-4996-8f81-252fd1c39c59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "4e27a1e6-0e7c-4640-92fa-dcde46cc953e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 106
        },
        {
            "id": "dffbde60-51fe-40c4-8726-cd8151cb2d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 120
        },
        {
            "id": "4b01ca7e-ee03-4744-bf90-d7c88283ae37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 38
        },
        {
            "id": "1745119b-1035-4981-83d7-1e0515ddf05c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 44
        },
        {
            "id": "0670d60a-1585-4327-80e4-6d38441ab253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 45
        },
        {
            "id": "1f2b9bc4-698a-4392-8b28-8167b4227f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 46
        },
        {
            "id": "e239d5b2-a9a7-4612-bff4-05ec06fd2224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 47
        },
        {
            "id": "6895ae2f-cdcc-486c-9574-400c84f87f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 48
        },
        {
            "id": "00d921d1-d60b-4de4-a215-8cba03d1262f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 54
        },
        {
            "id": "4b4238da-6485-4988-b14e-d8007cc1d4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 56
        },
        {
            "id": "2e545c54-026d-45dc-8eb6-8b69dfe4ff73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 57
        },
        {
            "id": "6365b42d-08c5-481d-a649-0359176d9994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 65
        },
        {
            "id": "28e4dc10-d2fd-4151-bdc4-6e07c19be897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 67
        },
        {
            "id": "429a5447-9de5-42fe-b94a-2a6c00ba168d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 71
        },
        {
            "id": "82e90695-faae-4569-be33-9d57960499ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 86,
            "second": 74
        },
        {
            "id": "62c4b40b-9cc2-44b0-b474-815d17f3d95b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 77
        },
        {
            "id": "b68b810f-3921-4180-8933-a01e92a38aec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 78
        },
        {
            "id": "09c715da-3af1-42ba-878e-62977eac3f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 79
        },
        {
            "id": "c97f7289-488e-436d-9af5-5b194efe7577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 81
        },
        {
            "id": "5a600c2b-2799-4e75-898a-98fd7d5c5139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 84
        },
        {
            "id": "7b076882-c371-402c-ac60-9a691e2e3098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 86
        },
        {
            "id": "50f1d26d-1786-4704-9550-81393b7d8e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 97
        },
        {
            "id": "e102813b-cb29-470f-b4d2-adda68ef8506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 99
        },
        {
            "id": "058676de-e44d-4d61-8c5e-b02f45bed044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 103
        },
        {
            "id": "4dc4cc66-309a-433f-ba3d-1a712c1e129c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 86,
            "second": 106
        },
        {
            "id": "76cd272e-0562-40fb-851d-30ed1070c245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 109
        },
        {
            "id": "ec8ce86b-32da-4130-9057-06d50fcb23ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "31ac1152-3444-40a5-9d3c-280c6e78e4e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 111
        },
        {
            "id": "8f5ff229-acce-4097-afe5-ec9fc03b7504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 113
        },
        {
            "id": "7b19dba1-689b-45e0-afac-7aa3153d6849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 116
        },
        {
            "id": "3f92c541-b0ff-45cf-be9f-1661a6cb38ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 118
        },
        {
            "id": "848608c5-8f8b-49bf-83d6-94a2cc1baf4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 192
        },
        {
            "id": "a0237142-8597-4ada-80f6-976a9c4f1a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 193
        },
        {
            "id": "1293a4e1-7025-4be0-818d-b662f670adfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 194
        },
        {
            "id": "77e8b2ca-53d4-44b5-901b-7584dec70b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 195
        },
        {
            "id": "f700e592-4439-4888-8268-7ce3f519f89d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 196
        },
        {
            "id": "2c167c0e-68d6-462d-bc5b-47058798143b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 197
        },
        {
            "id": "b79cebcf-5f20-43b8-953a-5a954ca9772e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 198
        },
        {
            "id": "198f97b0-bc8b-4e37-b675-3b361f2bf98c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 199
        },
        {
            "id": "ee9f8700-663b-4bba-8990-204c337bf513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 209
        },
        {
            "id": "570ce6c9-9e7d-4531-a7b6-b292b7fbcb24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 210
        },
        {
            "id": "fab5dbab-4046-4c1e-a830-c7d50195ebb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 211
        },
        {
            "id": "658381b2-20f0-4eaf-baac-10552d5fb8d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 212
        },
        {
            "id": "11038804-d04d-405d-aeeb-4c4dea1ea040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 213
        },
        {
            "id": "fe3eb759-2ff8-41e8-8656-3d6d61f8decc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 214
        },
        {
            "id": "f9542b46-b980-48bb-806c-e6515419455e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 216
        },
        {
            "id": "8844c858-795a-4a51-892a-889185f73088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 224
        },
        {
            "id": "17038268-4501-474a-bf1a-17ebc2c4eb2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 225
        },
        {
            "id": "c02d0a9c-6dd1-4652-a8b8-a6d200c35267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 226
        },
        {
            "id": "87b15bae-0a8b-4090-8e6b-5b15ad32a968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 227
        },
        {
            "id": "b86c703d-7ff3-45bd-a0e9-5b3db3d4a6c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 228
        },
        {
            "id": "306fc87e-bbca-40a8-bf2c-c7505630c98c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 230
        },
        {
            "id": "0ebb5f90-54d2-4f36-bb96-2dbf3b71bef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 231
        },
        {
            "id": "4aae35ce-37c9-470b-b5f7-d0f8346a5e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 241
        },
        {
            "id": "b5cae9cc-3c69-4c4b-8c38-e5a4d7344240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 242
        },
        {
            "id": "5c88f42b-f4e1-4976-bba6-ad6da8dea84e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 243
        },
        {
            "id": "27eb9fc2-44e6-4803-b930-c2543a6a8d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 244
        },
        {
            "id": "78d1fe49-8898-4733-ab5b-17929b91da3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 245
        },
        {
            "id": "71aed980-6ad3-4d86-b049-7f8daa60a13a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 246
        },
        {
            "id": "649d051f-1cdc-4be1-9598-f255572b3c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 248
        },
        {
            "id": "2afa3010-390b-4a97-b617-d27b27027410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 256
        },
        {
            "id": "b333a6c0-c744-41f3-adb8-f6f58cf7142d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 257
        },
        {
            "id": "1e98dabd-d99e-45e2-b4de-3cd2feb5e5cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 258
        },
        {
            "id": "30444101-cb56-44d9-aba8-164241cb6c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 259
        },
        {
            "id": "fac3f2e4-065c-469e-8078-59c83602e038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 260
        },
        {
            "id": "33b12e78-faed-4178-a9c1-5e2e9761ed61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 261
        },
        {
            "id": "24751bb6-b6af-4167-8659-08e12b9fcd58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 262
        },
        {
            "id": "19d8cab2-bb94-49ce-96dc-609fe6fd7495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 263
        },
        {
            "id": "f67816a5-9b16-40b1-a3d5-ee781123e9d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 266
        },
        {
            "id": "713f7080-20c1-4b04-a148-01a183e7e829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 267
        },
        {
            "id": "db7bd154-5468-4b00-9846-ee9e4bdb8304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 268
        },
        {
            "id": "db6a95ee-f32c-4639-882c-a3a03528b33c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 269
        },
        {
            "id": "953259cf-6656-4b95-b36a-8db6690134a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 287
        },
        {
            "id": "073f1fed-0b59-440d-982c-f5a81fdadf1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 288
        },
        {
            "id": "813f3aa3-6a00-4863-97a3-815d02d6d9c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 289
        },
        {
            "id": "b2dd0c93-31e2-4904-9440-0629cd11900c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 290
        },
        {
            "id": "af7fd883-223e-455d-bcbe-37e7bad6a12b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 291
        },
        {
            "id": "5501f457-d84a-43d6-8916-4266493ad086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 323
        },
        {
            "id": "9a5d110e-cf89-45fc-911e-84a28d8124e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 324
        },
        {
            "id": "6a82acfb-5e37-4e41-86cc-98846d8d402d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 325
        },
        {
            "id": "162b27fd-b8bf-412c-97c8-43be262dd37f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 326
        },
        {
            "id": "34ff021e-eb53-40d5-8aa6-270f13766905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 327
        },
        {
            "id": "933bdde0-2caf-49f7-8c80-5d7cb01cbbfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 328
        },
        {
            "id": "4387687a-5a52-4649-8763-1ed3bb38f3b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 332
        },
        {
            "id": "f211b195-d7c5-4be3-8925-1fe3481ba1c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 333
        },
        {
            "id": "c1aa4c11-36ca-45f9-994c-c34c968a4be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 334
        },
        {
            "id": "2d282182-3d46-49e4-ac8d-3db563b62ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 335
        },
        {
            "id": "8f53327c-7463-4025-84e0-b6437d5d8eae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 336
        },
        {
            "id": "5798ad0b-4c95-4a72-b1cc-a24cdf8e3058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 337
        },
        {
            "id": "9fcc3d33-7f92-4933-bf6e-89f65c472be9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 338
        },
        {
            "id": "d1a532b4-9187-4cfb-aade-41dc55ff80ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 339
        },
        {
            "id": "bb08d4b7-f0b9-448a-8037-d4188ed4a913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 354
        },
        {
            "id": "c713577d-29b3-4e8d-9985-353149790e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 355
        },
        {
            "id": "0d3e277f-5067-4516-b380-411311afa563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 356
        },
        {
            "id": "f0bfc047-f7b4-4f2a-a7ad-680ae2d1f7de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 357
        },
        {
            "id": "36f024db-f46d-4cdb-bf7c-a77370e30ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 358
        },
        {
            "id": "b5cbc583-fe6c-4c94-8d53-bdb010ebfdc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 359
        },
        {
            "id": "c90dcff2-381f-4527-9053-c854669dfaee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 538
        },
        {
            "id": "fc52857d-d964-4911-9aea-bfc42c3858e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 539
        },
        {
            "id": "3e7d7220-97f7-4a13-acb2-c6259a7ee9d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 8211
        },
        {
            "id": "0718797d-21d7-44e3-bc55-b036bdde12c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 8212
        },
        {
            "id": "fe44efaa-ea64-463a-aa9f-f08ccc577a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 8218
        },
        {
            "id": "e0ddb18f-c9d1-45eb-8fce-de7652f88321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 8222
        },
        {
            "id": "677fb613-c982-448c-a782-4a0ac4d55d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 8230
        },
        {
            "id": "9317bdfd-309e-47f2-9873-4205d476bc1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 47
        },
        {
            "id": "b86542f3-8d27-403e-a77e-9dc4e5b4db89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 74
        },
        {
            "id": "573004ba-a2fa-40ab-97dc-5c4a1ccad31f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 88
        },
        {
            "id": "b82e425e-adfe-4401-888f-c79bff5e64cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 106
        },
        {
            "id": "4c1f237e-0e75-489e-bacd-8a1575be2a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 120
        },
        {
            "id": "0b080d65-47f0-4283-91b5-4f91e6dab74c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 38
        },
        {
            "id": "07c0dc97-1139-45dd-af19-2e700a03f5f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 45
        },
        {
            "id": "3416746d-7bde-4f32-a921-0e709335aa52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 48
        },
        {
            "id": "202e25e2-ba14-400d-8982-c405303a7e4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 54
        },
        {
            "id": "3d3fd11a-ec00-4fcb-a655-66ddfb4f1f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 56
        },
        {
            "id": "280529af-c2b8-4389-a84c-f2b9450df407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 57
        },
        {
            "id": "5d76f025-95f2-4db9-bc70-ac50474e171d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 67
        },
        {
            "id": "8d8d615b-da6d-47ee-8e36-bd95ac95ad80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 71
        },
        {
            "id": "d3a45af5-1f1d-4146-93e9-f097d85a9147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 79
        },
        {
            "id": "e76ad1b7-81dd-4a0d-939d-8a5a9b6c043d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 81
        },
        {
            "id": "34a1177e-5bf1-40cc-8e06-9ff4cfe8ad97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 85
        },
        {
            "id": "69893c35-0046-4cfa-a937-94a751c75f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 87
        },
        {
            "id": "92f43a62-24cd-436a-90dc-ecf519498698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 99
        },
        {
            "id": "1fcdf1fc-7309-456f-9849-5823a51421fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 103
        },
        {
            "id": "f4e80bfc-b75b-4be2-a58e-7977f938601a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 111
        },
        {
            "id": "3173dfc8-27fa-4b3e-8d55-db90164ff524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 113
        },
        {
            "id": "9637b3fa-441d-461e-be95-2c6e625638d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 117
        },
        {
            "id": "9620976c-6c29-48a1-84e9-6c6fb2770b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 199
        },
        {
            "id": "9eda6235-d006-415a-9892-51b1fd2e372a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 210
        },
        {
            "id": "f5eac186-bf4d-4089-9048-c2ee11e540b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 211
        },
        {
            "id": "24dee24e-177a-4ae3-ac33-23e5c6fbd91f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 212
        },
        {
            "id": "23e49829-19dc-48ee-882c-be8b09566845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 213
        },
        {
            "id": "01429149-e7ae-47f4-9c2d-946e1894a75e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 214
        },
        {
            "id": "b94150d8-9e3c-44cd-b40c-2b65dbcd1b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 216
        },
        {
            "id": "419d5dca-5c5d-4e49-b746-88c626c0d90b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 217
        },
        {
            "id": "65a3386f-3e59-46ae-97a8-308df4f0d41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 218
        },
        {
            "id": "cb91ce52-afb8-4f39-bd34-aa4db8b6d0b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 219
        },
        {
            "id": "5f197a05-8bfb-447d-aac0-442e0b7c6c10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 220
        },
        {
            "id": "4b14b58d-0929-4824-a354-cc3106c842df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 231
        },
        {
            "id": "3514833a-d2eb-4612-8e8b-f63e250aa07d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 242
        },
        {
            "id": "eb011ec1-0c4e-4710-86eb-836ea48aca7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 243
        },
        {
            "id": "3ae612dd-7240-4fc8-94a4-4eab681daef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 244
        },
        {
            "id": "3a54cf81-b8e8-44de-b5d3-9eb4e8bc19a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 245
        },
        {
            "id": "94f4134b-d6a8-4c72-95dc-03924d9c8a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 246
        },
        {
            "id": "857c617e-d0f3-4732-86a1-c22af6384f24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 248
        },
        {
            "id": "11265182-bc4f-4367-b5d6-e2a145cd6151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 249
        },
        {
            "id": "4bf595ce-4a65-4cc6-a2c5-c57812b2c145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 250
        },
        {
            "id": "1d159087-0684-4dd9-80a9-ac4b683a8374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 251
        },
        {
            "id": "4b5234af-0221-4b1d-b6cc-f5970983096d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 252
        },
        {
            "id": "58da01dc-a48e-4c21-9638-4c71221bddc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 262
        },
        {
            "id": "45febfd5-1873-436f-9155-c65517075e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 263
        },
        {
            "id": "0c8c8352-762d-429e-a371-15d92730b3ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 266
        },
        {
            "id": "c94195c1-9ac7-498b-8ea9-d0544fb187f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 267
        },
        {
            "id": "f94fb940-4fe5-4abc-89be-c41e54aecb70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 268
        },
        {
            "id": "0178f5af-98bf-495f-a5c2-abf499bcb247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 269
        },
        {
            "id": "39882534-2c9b-4e03-82eb-e9135a1fe164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 287
        },
        {
            "id": "41ad2480-04c7-4e6d-bbe3-633bc6598ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 288
        },
        {
            "id": "33fa729b-0f1c-4fcc-b179-fc7e194cbd30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 289
        },
        {
            "id": "02ae7e54-e654-43e1-816e-342c981f9692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 290
        },
        {
            "id": "060b8a11-f511-467c-9831-6a65513d709f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 291
        },
        {
            "id": "7ad48b22-cf9b-4ed5-98a8-625b91b2a741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 332
        },
        {
            "id": "fd6f074d-50d5-4aab-ae46-fe9f5e6e7296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 333
        },
        {
            "id": "4e4bf20e-f4b8-4735-b46b-f826b9ccf9d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 334
        },
        {
            "id": "492e4809-0d02-4184-9c5e-61984825610b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 335
        },
        {
            "id": "7c7d12c4-ae80-4397-8f2f-472e12d95d93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 336
        },
        {
            "id": "45bd1c10-aaa5-4ec9-a996-a59afef93728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 337
        },
        {
            "id": "a90f1d80-d53a-46a1-a182-680cfdb597be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 338
        },
        {
            "id": "27a4e0fc-b988-415e-996d-c51b6b3ac0d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 339
        },
        {
            "id": "4db91d78-f1b6-4c42-a5bd-61f5fd621b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 360
        },
        {
            "id": "6091b32d-6fda-46ca-a74b-b71951f704fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 361
        },
        {
            "id": "a55168bb-ed31-4811-b4f2-79eeb7903270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 362
        },
        {
            "id": "bab03fb0-096f-40ba-b5a9-5cc10f74df17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 363
        },
        {
            "id": "7ce87156-0bb8-4729-a52f-19e717feb4bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 364
        },
        {
            "id": "bda93d4e-0883-445a-845d-58196eede682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 365
        },
        {
            "id": "5a8ea334-2f02-45b3-986e-fbaa934dd918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 366
        },
        {
            "id": "f86b6630-f587-4ac0-92de-981fe294eca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 367
        },
        {
            "id": "dd6ab601-e64d-474c-9bc5-429502bf1a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 368
        },
        {
            "id": "6a6cf86e-f52f-43f4-94c4-3ff78046f904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 369
        },
        {
            "id": "49a42a23-49be-41a2-9048-d815b0977594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 370
        },
        {
            "id": "0a256351-6a9e-427f-94e5-b5bf9cbb58a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 371
        },
        {
            "id": "6d9b8c77-6c9f-49b7-9bcf-a1991241ee1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 372
        },
        {
            "id": "a1686e64-dfd4-4405-aa25-f3e4d6f54e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 373
        },
        {
            "id": "ffd05f23-cd58-4c96-a4db-5220b4dc9fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7808
        },
        {
            "id": "b920d74a-f56c-45b0-85dc-00be959d3b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7809
        },
        {
            "id": "43fdd270-7e87-43f9-98fc-13cab7d5db3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7810
        },
        {
            "id": "6b1fcc05-7bf6-4a6c-936c-8803ec57a5c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7811
        },
        {
            "id": "49bcde53-8b67-49b6-af13-95c606382100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7812
        },
        {
            "id": "92745127-e387-4ba1-9fb3-108c09a77cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7813
        },
        {
            "id": "022ea967-e983-4bb8-8edb-bdb1d2a32db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 8211
        },
        {
            "id": "938e14be-cf12-41de-adaf-77ba3a4cf1ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 88,
            "second": 8212
        },
        {
            "id": "52356b74-3813-4f6a-b1f4-25781e13deba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 47
        },
        {
            "id": "7d76d817-3998-49a4-92e6-47cf751e9071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 74
        },
        {
            "id": "07889629-fecc-45a2-a6b1-0413b28935dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 88
        },
        {
            "id": "ea26ee42-81f8-42a8-aab6-51ca5863ca7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 106
        },
        {
            "id": "9662e671-22f9-4ba6-859c-7fa785426b76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 120
        },
        {
            "id": "524842d3-a953-42b3-9e3d-e4be4fb8f5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 45
        },
        {
            "id": "0e5ff2ab-34b5-4c97-8e8d-1ffeed57d807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 89
        },
        {
            "id": "afd53789-2dbf-4f65-80fe-2c3e314d534c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 121
        },
        {
            "id": "a120c7ac-3bc1-4435-8262-77fc8fbee6ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 8211
        },
        {
            "id": "b6a13901-1446-411f-9137-ae07514e75bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 90,
            "second": 8212
        },
        {
            "id": "3376b125-458e-47c4-ae65-8574ca491ed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 34
        },
        {
            "id": "c0d73280-efcd-497f-9b1c-fd7d3390681c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 39
        },
        {
            "id": "8c57d52d-2d48-4ec2-a038-d73f497f4d23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 84
        },
        {
            "id": "942876a7-dc86-44b3-88f8-49971f3d25c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 86
        },
        {
            "id": "5a435f50-c796-48b1-b7bd-23d0a212304f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 116
        },
        {
            "id": "9993525c-ab02-43fb-974e-ed7e94836f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 118
        },
        {
            "id": "97371a19-eeb8-46bb-a7ae-0abfc50e392b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 354
        },
        {
            "id": "18e59619-2640-4290-b14b-d8b3a4dfa472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 355
        },
        {
            "id": "5a649bb8-4259-4c8c-8019-01a41fb4478a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 356
        },
        {
            "id": "8eaf017c-1429-4777-93af-5778de9a524f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 357
        },
        {
            "id": "4d12d9d6-b10d-407b-adaa-b653bcaebd60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 358
        },
        {
            "id": "d4f902f7-9c51-4f91-978b-d081b91803e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 359
        },
        {
            "id": "3de2baad-1ec2-4728-9555-ddfaded5fc10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 538
        },
        {
            "id": "e2196a00-3ff1-4294-9feb-7ef3440a86a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 539
        },
        {
            "id": "8491eb95-94b6-4bdb-91bb-77e33cfa4377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8216
        },
        {
            "id": "a374426a-4a0b-45cc-8f70-c0b517bedfc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8217
        },
        {
            "id": "d4d3d8af-b11e-4f3d-9c04-9abaf2985b95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8220
        },
        {
            "id": "7f488d88-3986-41df-84f8-ee2c610b5b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8221
        },
        {
            "id": "6efdd50d-41f2-41c5-a777-285f2153e254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 44
        },
        {
            "id": "0c66491c-aa70-46d3-a201-b6442e40345e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 46
        },
        {
            "id": "b2b59939-4b03-47b3-87f8-d6914f8fbe0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 98,
            "second": 47
        },
        {
            "id": "2228791b-940c-4672-9c55-587a6ab9fb10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 86
        },
        {
            "id": "5b38228b-b1c8-4498-b07c-d43e43599f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 88
        },
        {
            "id": "a6899a42-3285-4169-afd6-16b1012a2db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 118
        },
        {
            "id": "69d089d1-9244-40db-bc9e-733969a37c8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 120
        },
        {
            "id": "75bd8c1f-c912-496d-a006-69dc33c1174f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8218
        },
        {
            "id": "a878c110-aafd-4768-9b1a-73f48d718c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8222
        },
        {
            "id": "e8b30c8c-31d3-4321-85af-b3024e1f0e7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8230
        },
        {
            "id": "afa8ad88-fd54-4785-9324-ddaf6010eed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 99,
            "second": 45
        },
        {
            "id": "c0365b84-b5c0-4a01-8e50-a422ef58dbed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 99,
            "second": 8211
        },
        {
            "id": "ce9c881d-2a40-4e2e-8e34-9d24e9788a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 99,
            "second": 8212
        },
        {
            "id": "e9d3dce7-79de-473f-859a-dc1effea481c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 44
        },
        {
            "id": "7a1ae089-4f27-40d7-9b8d-8ad81bc3aa42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 46
        },
        {
            "id": "ab0ce03d-1585-4fa6-8c79-d2263e689004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 100,
            "second": 47
        },
        {
            "id": "a3dd729e-7154-4e59-96ca-02ef4f068b36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 100,
            "second": 86
        },
        {
            "id": "f502e9ba-d23f-46cc-90a7-11a2006f2a78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 100,
            "second": 88
        },
        {
            "id": "ec1a2b09-125e-4246-a734-44afa5c07ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 100,
            "second": 118
        },
        {
            "id": "cfc8d5d1-2a20-4f64-9a0f-6b8b9234137c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 100,
            "second": 120
        },
        {
            "id": "6a88117b-0853-4ada-b13a-a995b45d9e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 8218
        },
        {
            "id": "2edb0325-51aa-4e11-9755-bc1264d7b100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 8222
        },
        {
            "id": "06e2ad1a-5720-4ade-a74c-1c82f287e14a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 8230
        },
        {
            "id": "0c4c93a2-0da6-4e25-818e-d5c1833b1c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 101,
            "second": 45
        },
        {
            "id": "8ce7f888-9f01-46ff-b432-dcd61d26214b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 101,
            "second": 8211
        },
        {
            "id": "e09fff3b-83bb-4b12-a887-1395693d4dd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 101,
            "second": 8212
        },
        {
            "id": "74f0e2e3-a746-4e5b-b5c8-eafc5faf611b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 102,
            "second": 44
        },
        {
            "id": "66cb24ed-7dae-428d-b4b6-0e94b4c9b3f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 102,
            "second": 45
        },
        {
            "id": "a8dc69c2-c736-4c9d-b3d2-d4356450d1d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 102,
            "second": 46
        },
        {
            "id": "22117402-5acf-4c84-b9a9-51bcb46be4fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 102,
            "second": 47
        },
        {
            "id": "2d79f7a8-1075-4112-a19d-13e3b97fc1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 65
        },
        {
            "id": "249f7c02-4ba8-478c-bbca-9823a64346ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 102,
            "second": 74
        },
        {
            "id": "86e92440-a582-40a6-ae2d-d2a629ac4703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 86
        },
        {
            "id": "8f9abe0f-c892-4bea-94b0-3f6757f3baac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 97
        },
        {
            "id": "3b33514e-08ae-4727-ae46-4bd344af49c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 102,
            "second": 106
        },
        {
            "id": "a3bfeafc-0d88-47f8-850c-bcba78148fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 118
        },
        {
            "id": "18ef527d-aba2-4a44-b801-d52903e2a00e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 192
        },
        {
            "id": "aeeb0ddc-8136-4da9-b220-947d9a5516ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 193
        },
        {
            "id": "f108b9fd-357b-46ce-8c07-c55dfbeba2fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 194
        },
        {
            "id": "b6b9ca01-ca43-465c-9829-4b344c412b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 195
        },
        {
            "id": "f9483d6b-ea58-42be-85be-e2cb17195648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 196
        },
        {
            "id": "b702b8f4-fb83-4e44-8f7a-b39ea45223c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 197
        },
        {
            "id": "2506a3c3-cf20-4e23-a329-1a3cf3248817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 198
        },
        {
            "id": "33f0fc71-eccb-416f-bebe-6b8304aa7282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 224
        },
        {
            "id": "069391bc-18fa-4267-a46a-fea2f1a1dd11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 225
        },
        {
            "id": "63857ba4-f544-4c09-9b4c-07e80e1f9f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 226
        },
        {
            "id": "655574cd-5485-434a-9951-115d69e23ea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 227
        },
        {
            "id": "fd8cbf19-5718-4266-bc06-f0026bf0aee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 228
        },
        {
            "id": "79ec3bcd-7872-421b-85b2-fbe7937af546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 230
        },
        {
            "id": "6e24248a-b8b2-4205-b826-3e127e7cbdf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 256
        },
        {
            "id": "f65a204f-7d06-41db-9d56-25d2008224f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 257
        },
        {
            "id": "c6605b28-5398-47b9-a1d3-16b91a0d6fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 258
        },
        {
            "id": "552fa24d-299e-4210-839e-8be52d6aa27d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 259
        },
        {
            "id": "8834002c-9ead-4b70-8bad-56e9b05a156c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 260
        },
        {
            "id": "058fce64-1507-4cb9-8a3d-c1a4034f88fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 261
        },
        {
            "id": "9c3544b2-433b-41a7-b3bd-ea4cc489a648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 102,
            "second": 8211
        },
        {
            "id": "82cb6a6a-3223-4d61-9ade-6f28cbe62ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 102,
            "second": 8212
        },
        {
            "id": "72c7cd35-5ed0-400a-80a7-6c6c20e4ae26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 102,
            "second": 8218
        },
        {
            "id": "31df3700-b131-4caf-89cb-69277943c527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 102,
            "second": 8222
        },
        {
            "id": "f9113328-8873-4d1b-8df7-e7b432e18679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 102,
            "second": 8230
        },
        {
            "id": "38bee738-c154-4d4f-ad07-6388a2fb5fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 86
        },
        {
            "id": "364dba53-c5c1-419e-ad0b-869c0641abe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 118
        },
        {
            "id": "ecc6b1d4-dedb-4bf3-a607-f1ec83ca05c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 44
        },
        {
            "id": "78f125da-9a87-47ab-b958-6b0e3d8a1ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 46
        },
        {
            "id": "73938edc-6409-45d4-8389-095582c702a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 106,
            "second": 47
        },
        {
            "id": "d424493e-0e64-4848-a117-737af5ba8d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 74
        },
        {
            "id": "f84c0ecc-c1fe-41f0-ad45-05888e2041d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 88
        },
        {
            "id": "7cad2b8a-0eaa-4d84-98b3-a74c8d4705b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 106
        },
        {
            "id": "64521614-c02c-44ab-9179-cd7d7ead031a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 120
        },
        {
            "id": "5b751e6a-4922-4f53-bc52-47fc33f5e30d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 8218
        },
        {
            "id": "847123dd-5715-4040-b18d-6b66d03b3004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 8222
        },
        {
            "id": "761a4258-34dc-4694-821e-a4325a46cd3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 8230
        },
        {
            "id": "d8363892-8789-4451-86d4-d472c2c2d259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 38
        },
        {
            "id": "fd319cf8-dc5a-46ed-898e-c0af27113567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 44
        },
        {
            "id": "e802597e-a3ad-455f-836f-2c4026fcadd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 107,
            "second": 45
        },
        {
            "id": "8bd136b2-4f40-4dde-b756-c73bc2378ad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 46
        },
        {
            "id": "8807a248-908a-445a-8b39-c6c01094272d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 48
        },
        {
            "id": "4b1ee2cd-5c8f-4f0b-a1ba-31383693bbe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 54
        },
        {
            "id": "9def4476-84ea-4b8d-9e44-4702d31b0790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 56
        },
        {
            "id": "57ea56a2-9d26-47ad-88aa-e3a50ef298db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 57
        },
        {
            "id": "050ed253-1fdf-46f1-a6d5-385760d614da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 67
        },
        {
            "id": "5e6c2ec9-788a-4f3b-82b2-a2b0022126ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 71
        },
        {
            "id": "3a8a88f7-6103-4ef1-9dec-0dca50ae778e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 79
        },
        {
            "id": "60ccde8c-7d05-40b2-8545-82e2cc852288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 81
        },
        {
            "id": "3697fea1-107f-4648-b5ad-c9c093e9897e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 99
        },
        {
            "id": "e257014f-ded1-46a5-82a6-81402aa696d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 103
        },
        {
            "id": "cd9f17b9-a651-4fbc-ad77-21812dcd6ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 111
        },
        {
            "id": "6a8559e2-9e28-42f6-875a-4f609f6d3b61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 113
        },
        {
            "id": "79530127-0db7-4a48-a0f0-378d60a410fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 199
        },
        {
            "id": "9c0fbd88-d36d-4e86-89fe-6fe664d4d035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 210
        },
        {
            "id": "07fbeec1-db27-4c0e-8d62-1c189f8790d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 211
        },
        {
            "id": "468948f0-8145-4c46-bcb5-d908e3285423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 212
        },
        {
            "id": "9dc30994-c645-42fb-8877-6e323e063035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 213
        },
        {
            "id": "faa7266d-24f8-42b1-ac83-793617cd8fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 214
        },
        {
            "id": "7ce63d5e-1801-4e24-88f7-3bb94a81c879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 216
        },
        {
            "id": "e87b5d03-aa48-4858-ac54-e268e4b1b330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 231
        },
        {
            "id": "3c1f6a92-db9d-468a-8a1d-a964f5b04fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 242
        },
        {
            "id": "fc671d1f-69d3-4b72-974a-f1579e79c560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 243
        },
        {
            "id": "c6238f61-57f7-4cdc-a6c4-ff0152e26789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 244
        },
        {
            "id": "ae4e6056-c3c1-4843-a586-f4e103dd1b62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 245
        },
        {
            "id": "3e362d5d-64d0-4087-949b-9196b9dc8e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 246
        },
        {
            "id": "e6e497a6-8c8e-4bf4-8c0a-278483043b32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 248
        },
        {
            "id": "379c006c-23aa-41b8-956e-111bcacb15f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 262
        },
        {
            "id": "673b31e8-0267-4bd8-b389-5dfe4a3e5f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 263
        },
        {
            "id": "2f7343b6-1c89-40c8-9e74-3fdd09794e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 266
        },
        {
            "id": "991281ce-d605-4d20-a397-afae7075454d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 267
        },
        {
            "id": "b40bf622-049d-45ce-b691-7e725058fdb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 268
        },
        {
            "id": "86dd2c04-5bf4-4b21-826e-e43ad5cb6752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 269
        },
        {
            "id": "d06cb8e9-e469-47a3-a5be-99cc61b1db6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 287
        },
        {
            "id": "4442a23f-81a6-4827-b3b6-12b12cbbb063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 288
        },
        {
            "id": "76b12a12-930e-4ed9-872c-f603af2fb16f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 289
        },
        {
            "id": "9750abf7-f641-463e-9a04-5ea603c6cda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 290
        },
        {
            "id": "f40b6271-31f8-4060-ab89-a3a199d0beb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 291
        },
        {
            "id": "d91bd625-1dd9-4ace-b81b-e7744edc9808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 332
        },
        {
            "id": "a9501f10-9b41-4fd1-893b-a2a0e741a047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 333
        },
        {
            "id": "56edebe8-814a-4f6b-97a1-e89cceb002ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 334
        },
        {
            "id": "9a8fefee-b86b-4e2d-9133-4895872ae6be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 335
        },
        {
            "id": "285b9f21-2dc2-4454-a2a9-4ffe612740d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 336
        },
        {
            "id": "107eda07-b3c5-4934-a96a-62fa1cfbe866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 337
        },
        {
            "id": "ecfc75b6-1bc7-4b28-b556-96f8e9f3a8d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 338
        },
        {
            "id": "cdbd9dff-5ce4-401b-a6d6-2b22101b6782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 339
        },
        {
            "id": "b628347e-8fa4-4fca-b18d-910638075082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 107,
            "second": 8211
        },
        {
            "id": "3e86d98d-d0f2-41ea-abfa-2bbe1afb5dcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 107,
            "second": 8212
        },
        {
            "id": "bd4902b8-7784-447d-939d-90752b6085ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8218
        },
        {
            "id": "3d6ad0d5-7d8c-47ce-8cdc-6b269c7dd821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8222
        },
        {
            "id": "ec6cd286-153d-4b2f-b690-3125b184dfff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8230
        },
        {
            "id": "849c9334-9059-484d-9af7-da9fb47b97a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 34
        },
        {
            "id": "a1d4f55a-605c-458f-a06d-5760ba6ed7b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 38
        },
        {
            "id": "91ea6bd4-8988-46e1-beef-8646f5483e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 39
        },
        {
            "id": "2736068a-0d93-4672-ba16-7541ec427dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 108,
            "second": 45
        },
        {
            "id": "8de4b39f-95f7-4da3-bc67-bdef32673e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 63
        },
        {
            "id": "7bfcde03-ef1c-4252-babb-05e7406099f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 84
        },
        {
            "id": "5614ad67-c758-43b0-aa45-f42ec7d39fda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 86
        },
        {
            "id": "1b6871ff-8443-4445-98d1-5bba7e795bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 89
        },
        {
            "id": "12e91c1c-b034-4e30-86dd-55179f1c5295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 116
        },
        {
            "id": "3ba008ec-b79e-4d4a-8933-2cb08c9aa00b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 118
        },
        {
            "id": "d9b4a933-44f6-4ce3-a35f-a8fa4ed46979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 121
        },
        {
            "id": "6c8b159c-6e47-4403-b435-58e36e51d5f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 354
        },
        {
            "id": "17514662-4cf5-4a35-8513-65ef3220d3dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 355
        },
        {
            "id": "e93cfae9-cf4b-4b9b-9fb2-037b97a0618a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 356
        },
        {
            "id": "08dce746-f49d-467d-908a-e6d78275b1a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 357
        },
        {
            "id": "94079a27-6c7d-4f14-b271-9eab8dc0096f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 358
        },
        {
            "id": "900226e9-febe-4ee3-979e-069edb954a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 359
        },
        {
            "id": "b70bab0b-e8ae-46db-8b35-2f0eb7ea7a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 538
        },
        {
            "id": "c55d82fd-a19f-476f-8e54-287297c06494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 539
        },
        {
            "id": "631ab4dc-056c-4f8f-a6d8-59505c04e07f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 108,
            "second": 8211
        },
        {
            "id": "8ce3ddc4-1f25-4798-b8de-2468c5edebeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 108,
            "second": 8212
        },
        {
            "id": "e28b3458-aea6-467c-9e2d-6096cb550cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 8216
        },
        {
            "id": "504806f3-66c0-4a6b-87ac-51e062990d2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 8217
        },
        {
            "id": "0abfbf3a-685b-4679-9e41-8962d43d5a89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 8220
        },
        {
            "id": "9fcd46de-d194-45a0-9966-e4c98bfd0c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 8221
        },
        {
            "id": "e36eaf77-3bdc-46b5-a8c7-e77739549645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 86
        },
        {
            "id": "f914b21c-14ac-4a6d-b227-8f63c5445b26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 118
        },
        {
            "id": "d501313c-0113-4887-accc-4a855cdfd50b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 110,
            "second": 47
        },
        {
            "id": "89e824d3-6180-494a-b9a3-2dc684284bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 44
        },
        {
            "id": "91ba537e-6414-4439-b36b-fb612369fd73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 46
        },
        {
            "id": "1e0123b6-676b-418e-9f77-4809f06c35b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 111,
            "second": 47
        },
        {
            "id": "8bae5b2f-1f12-4935-930e-30e2202dfc7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 55
        },
        {
            "id": "a315b790-8d5d-4966-84be-eb9ab5ca1b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 86
        },
        {
            "id": "0425426e-6ed0-472f-8ab2-d6740890c35d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 88
        },
        {
            "id": "d0a69a9a-cd92-4326-a18f-3881cda14a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 118
        },
        {
            "id": "b439f6cd-43f4-41d4-b8b2-25cb797e018b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 120
        },
        {
            "id": "45588f7d-e59a-4d55-a995-ad868fd763ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8218
        },
        {
            "id": "6c904e18-660f-4d92-bb3d-fec157ec18c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8222
        },
        {
            "id": "67d7714a-c6a7-4a61-9b57-25f4918b5140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8230
        },
        {
            "id": "86f7cda1-a952-4435-adde-7ba6601e068d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 112,
            "second": 44
        },
        {
            "id": "46133f91-5cc9-4bd4-9d3e-7d9bcd8f7b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 112,
            "second": 46
        },
        {
            "id": "e6115851-e3c7-4bc6-a0dc-b6ab82e1f890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 112,
            "second": 47
        },
        {
            "id": "baad2514-fcf0-4c40-b2ed-98d9c1d8bf1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 74
        },
        {
            "id": "4bd2a945-3492-463b-b512-44d1d3e04fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -12,
            "first": 112,
            "second": 106
        },
        {
            "id": "7a9ccf29-8145-4f5c-9614-5cfac8b830de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 112,
            "second": 8218
        },
        {
            "id": "2a914666-4e46-414b-b6f6-9bba40036bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 112,
            "second": 8222
        },
        {
            "id": "99489cdc-578b-415f-ab44-80a133264913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 112,
            "second": 8230
        },
        {
            "id": "6f4e23db-a62b-4dc7-9f38-71bf524f4241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 44
        },
        {
            "id": "12623e0a-9c95-4923-be58-32ebadd30cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 46
        },
        {
            "id": "a6acb848-eb2c-4a82-a682-3f60b8a425e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 113,
            "second": 47
        },
        {
            "id": "4745eb3c-f084-49bf-b79a-4e7f853b9d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 113,
            "second": 55
        },
        {
            "id": "e3608236-cb91-4562-b5aa-eebac21acae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 86
        },
        {
            "id": "c587bbbd-9ad6-4c9d-b9f8-0aa9ae81535d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 113,
            "second": 88
        },
        {
            "id": "b76569ce-bf7c-43b2-8a24-ac516c91bc0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 118
        },
        {
            "id": "3e64c714-2466-444f-948d-ce5c82522daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 113,
            "second": 120
        },
        {
            "id": "02784c78-a98f-408f-aba6-9c86d12ad569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 8218
        },
        {
            "id": "17f091f7-889f-4850-b34e-e71a7d762a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 8222
        },
        {
            "id": "370b44c5-91c2-4abb-9170-933c3324ce26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 8230
        },
        {
            "id": "e4d7ea21-9fe8-458e-836c-f41dc9ffd406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 48
        },
        {
            "id": "b39a5321-c247-4e8f-9687-828a6b15a7ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 54
        },
        {
            "id": "4074ce8d-41bd-4ce5-9c25-35edb005f410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 56
        },
        {
            "id": "5b95a323-bb7d-4364-aa96-2ba5510a337f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 57
        },
        {
            "id": "e0cc9904-3b80-46ef-8337-e31e871e79d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 67
        },
        {
            "id": "f62daa8c-0423-4dbf-9d4c-6c194a4b4ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 71
        },
        {
            "id": "22ba361a-84ba-400a-86b2-ab37e3314d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 79
        },
        {
            "id": "19213b8a-9258-42bf-9697-483248c50c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 81
        },
        {
            "id": "67371dd4-e577-4437-8924-e2648d7d81ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 84
        },
        {
            "id": "da4f7ebf-4e17-4c4f-b622-f7c1a2d6c2cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 86
        },
        {
            "id": "26744c86-2453-45e9-9d5d-5d1f11f96368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "836803a5-5d7d-449b-b3d4-afd5cf07104e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "b7ca65e7-e155-419d-af7b-2d8d3e700bf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 111
        },
        {
            "id": "f13b44c7-54da-4a83-8ac0-0c81db071d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 113
        },
        {
            "id": "f5244910-e8fa-4361-bd27-52c98f6331f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 116
        },
        {
            "id": "1f6cae74-a391-45cf-8692-1f32b234ec7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 118
        },
        {
            "id": "9c98f102-3b46-4f36-b338-3837f5d44b0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 199
        },
        {
            "id": "ada2789f-1f7b-48d1-ac6a-8e9c712ab2bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 210
        },
        {
            "id": "5c1371c5-dd1c-4921-90a7-06a8f3d2f63a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 211
        },
        {
            "id": "0e6a04e0-7421-44d8-ad82-6a1a75699100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 212
        },
        {
            "id": "e5165463-9e9e-41a5-a5b2-ee5ffe9ea3cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 213
        },
        {
            "id": "811dd4f5-4696-4782-9591-d8743d1dd7fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 214
        },
        {
            "id": "0e61f6d8-9cbf-4de1-890d-d002fac6c71a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 216
        },
        {
            "id": "88c24464-b371-4e04-b83c-e520dec9c803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 231
        },
        {
            "id": "1f3b9a76-56b0-434c-93a4-246db8c01b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 242
        },
        {
            "id": "8e6692dd-2ff3-407d-9490-6248da68ec63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 243
        },
        {
            "id": "555c51e9-4f31-42ff-9700-fed91e8c94c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 244
        },
        {
            "id": "8e83f7c8-5854-4ede-b253-43091469e803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 245
        },
        {
            "id": "977b635a-6d69-49e2-967f-c75ad3325413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 246
        },
        {
            "id": "4d61c606-8891-4ab3-ac90-a2beb7e24065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 248
        },
        {
            "id": "648e5a5d-d99c-4987-9ba1-b4b38a7d9f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 262
        },
        {
            "id": "a81ed199-3da4-462c-b750-be22da2fe050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 263
        },
        {
            "id": "2c74983d-cda9-4d26-bae3-c37b1498139a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 266
        },
        {
            "id": "2c87520f-a4ea-4682-a4f8-858e2f959999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 267
        },
        {
            "id": "fa5e12af-1904-4842-816e-2ec3cf34ecc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 268
        },
        {
            "id": "5624e3c4-d55f-495c-ac95-2e4c2e9d29f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 269
        },
        {
            "id": "a7d46154-4b6a-446e-a55e-802fff287fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 287
        },
        {
            "id": "8226512e-991e-4187-a5e3-e714abcf3de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 288
        },
        {
            "id": "2c1b7af1-d899-43a8-b3ab-57060a62867f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 289
        },
        {
            "id": "ff9d8230-d5a0-4c28-b18f-994e2388eefd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 290
        },
        {
            "id": "0d169d5c-8d56-4370-b688-c96b83f779b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 291
        },
        {
            "id": "5779273f-c505-4222-a206-50358ed3995e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 332
        },
        {
            "id": "924ccf1e-c55c-408f-9f40-aa472fc8fc7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 333
        },
        {
            "id": "64c7f411-b0bd-4ddc-8979-a17cbc42adc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 334
        },
        {
            "id": "3b05d919-11db-4e31-be36-67e1b51d44c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 335
        },
        {
            "id": "c91b1c37-2787-43aa-b124-c3c03aead076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 336
        },
        {
            "id": "fdeb1e13-bcb7-49aa-b4d6-638f3cd58285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 337
        },
        {
            "id": "2a592036-19f4-4c62-ab6a-1f6f79550ceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 338
        },
        {
            "id": "cd38805d-2ec3-40b2-b026-f6a0dd097ad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 339
        },
        {
            "id": "d14f214c-bc87-433d-97e6-74c3dc6b305e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 354
        },
        {
            "id": "07366a72-b3ed-4fc0-a64f-6384be2babe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 355
        },
        {
            "id": "bc7a42f9-f4b9-4c40-b7c2-295246d411aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 356
        },
        {
            "id": "2311c41c-f17f-48bd-82f6-46d4559fb088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 357
        },
        {
            "id": "2001519a-a195-4a2a-ab17-e2977bfb2139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 358
        },
        {
            "id": "c5add098-1832-4306-bdb7-f5b15560e2c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 359
        },
        {
            "id": "b79e7390-760e-4c51-901e-1fd79eaff522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 538
        },
        {
            "id": "598fe3f7-cdd3-4467-ba14-11392023ca16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 539
        },
        {
            "id": "b2794d78-a860-44ca-8baf-eff43c0a15c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 115,
            "second": 47
        },
        {
            "id": "fe00e091-174e-49a5-8d70-bd34212159f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 34
        },
        {
            "id": "2765dcca-80c9-449b-a724-60dfe5939b15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 39
        },
        {
            "id": "f079a45d-b68f-4af9-8013-94a5812da1c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 44
        },
        {
            "id": "472634da-31e1-4a21-997f-95a17c7f05f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 116,
            "second": 45
        },
        {
            "id": "ba00a1b1-c659-4a6f-9b56-16da34c62da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 46
        },
        {
            "id": "f356446a-1508-4c6f-852d-a857808558ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 116,
            "second": 47
        },
        {
            "id": "5a23d233-08e8-43dc-a058-b55ae9d4a8e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 65
        },
        {
            "id": "2fbb611f-75ef-4cc4-90ee-0f74c942707b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 116,
            "second": 74
        },
        {
            "id": "97938f49-3bf1-42f2-9674-11a60c3bef15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 84
        },
        {
            "id": "39b934ac-c822-4269-a1e9-3a9f4f082a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 116,
            "second": 86
        },
        {
            "id": "6f87a02c-138d-499c-961b-ac1e8bbe635f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 97
        },
        {
            "id": "4d746e61-42e3-4ed3-8b41-d452eb8a36bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 116,
            "second": 106
        },
        {
            "id": "57239157-5324-4223-8034-414f3b7e27eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 116
        },
        {
            "id": "19fa99d1-00ba-4c24-ab5e-5836e6ee362c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 116,
            "second": 118
        },
        {
            "id": "45130df3-73b6-4e96-bfa7-74b4b2bedd13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 192
        },
        {
            "id": "211a6b0c-4649-49e6-9c91-6312c77ed124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 193
        },
        {
            "id": "6cb6ad02-02dd-4e1f-a889-0717a3dd1966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 116,
            "second": 8211
        },
        {
            "id": "3b4536da-117e-49dc-9df4-5ba0d86d1869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 116,
            "second": 8212
        },
        {
            "id": "1798a503-9ddc-4c63-a6a4-90f10514be68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 8218
        },
        {
            "id": "6b783242-d3e1-48f4-aafc-d2aa880eebe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 8222
        },
        {
            "id": "fe7e0967-f703-4d27-a46e-3e54c9121dcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 8230
        },
        {
            "id": "1c63b491-9c2f-4b86-87c0-a6dba89eddc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 117,
            "second": 47
        },
        {
            "id": "a4f2cc81-d316-4d31-9172-26faba1acd4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 74
        },
        {
            "id": "9c0f4535-be09-4cd8-9b02-c63f724d7744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 88
        },
        {
            "id": "56d34a3c-2025-43b3-ad1f-5e5499d905d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 106
        },
        {
            "id": "05b92861-ad3f-46d6-bf7f-8a1e19b69c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 120
        },
        {
            "id": "4cf4d081-e3df-4f9c-ba63-4d9e6af8d1ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 38
        },
        {
            "id": "b899223c-9b58-45f4-bbd7-4f3561fd0624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 118,
            "second": 44
        },
        {
            "id": "fe31e348-f7ee-4bcf-93c8-c3c3c3fe98ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 45
        },
        {
            "id": "a323e976-2b31-4275-bc8c-de5f6031f13c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 118,
            "second": 46
        },
        {
            "id": "315aa516-7657-410d-be45-756787799d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 118,
            "second": 47
        },
        {
            "id": "4d1afff8-3b4d-4495-90e4-a7b744c00400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 48
        },
        {
            "id": "6d0bd0f8-458b-4696-9c6c-6c2b97dda501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 54
        },
        {
            "id": "23768022-4082-4f26-ba16-b7bc389e94bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 56
        },
        {
            "id": "6b25fa8a-70cb-4d31-895a-79f62ddc1f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 57
        },
        {
            "id": "1e74bc73-bcc9-4279-b7d9-fb77b0da837c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 65
        },
        {
            "id": "c9b1f200-e4ed-4520-8a1f-db645c6febbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 67
        },
        {
            "id": "be8d6ab8-a34b-4d8e-be42-c8329192b525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 71
        },
        {
            "id": "7b917d98-11e8-4fd3-81d6-cbf3ee405987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 74
        },
        {
            "id": "a18fddde-3bba-4323-b4fb-ea79cae92e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 77
        },
        {
            "id": "0cc7ccb4-cfaa-4af8-bf3b-0c1186a2cbd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 78
        },
        {
            "id": "230b7dbf-1638-4660-955c-45ba445c95dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 79
        },
        {
            "id": "6764ec92-9f9f-455b-b3aa-57defa315c48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 81
        },
        {
            "id": "797c5ea7-659a-4ce2-8fdf-e649baa917b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 84
        },
        {
            "id": "42d664b4-c24c-4ec0-b96a-81c692fa84fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 86
        },
        {
            "id": "24c74140-59ce-492a-953e-7022b74b1de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 97
        },
        {
            "id": "dfe97bf9-0566-40b6-bcd9-23912b842bce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 99
        },
        {
            "id": "1a75e78f-476c-4e31-9bae-bbb9b7ff3705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 103
        },
        {
            "id": "a72b2cfc-8979-4ef4-abf8-5443d92d967f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 118,
            "second": 106
        },
        {
            "id": "5ea2994c-1585-42c5-af9d-d17f27360edd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 109
        },
        {
            "id": "a5e550e3-8e27-4a9e-af06-a8e0ef7f022e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 110
        },
        {
            "id": "475df32c-7f88-4650-8aab-516a933a04c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 111
        },
        {
            "id": "5b31f929-806a-409e-a46b-3e0d9a511735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 113
        },
        {
            "id": "513bf6ef-184a-4272-ab3d-400cd2639f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 116
        },
        {
            "id": "b206d7e4-ea21-47ae-9d5c-14fa20865b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 118
        },
        {
            "id": "a0c71bd6-71b4-4ada-9cec-3b139ecd8c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 192
        },
        {
            "id": "d87eebc1-b899-415a-9fbe-eddc334d549e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 193
        },
        {
            "id": "701f5292-217b-4929-b64d-12fb0bfd8da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 194
        },
        {
            "id": "ac8ac06d-2c69-4501-a9d5-6f715bf19c62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 195
        },
        {
            "id": "752ffe42-4931-4829-b5d5-e5d645701730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 196
        },
        {
            "id": "3f109a42-9ca9-410e-9703-d7487fc18541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 197
        },
        {
            "id": "85611fda-d326-40b6-8780-39dc39197a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 198
        },
        {
            "id": "816ddc65-6fae-4d9e-adac-d692b4e77f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 199
        },
        {
            "id": "6cb80eaa-0f57-41a5-be60-c276e1468ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 209
        },
        {
            "id": "d9304100-abd8-4752-b01a-ffcf57504155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 210
        },
        {
            "id": "12ae8767-6999-463d-8451-be6ae1d46207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 211
        },
        {
            "id": "155aeb3e-89d7-4063-b485-9202767f0c80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 212
        },
        {
            "id": "2f9a6f7e-fe27-4184-8798-69bfb3ee598c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 213
        },
        {
            "id": "7a5ae7e7-c0dd-400e-bb50-2a642049fb51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 214
        },
        {
            "id": "5b0331de-4a8c-467e-89fe-09f15b34816f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 216
        },
        {
            "id": "77138682-4394-4b27-8353-ee91af945d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 224
        },
        {
            "id": "24a733ae-6d0f-4bfe-b21b-6977d25f62e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 225
        },
        {
            "id": "59ee77fe-69e9-4ee5-94af-1803708f2e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 226
        },
        {
            "id": "60a502b3-35a4-4bf6-9010-afdb036c1e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 227
        },
        {
            "id": "125836e1-b5a1-4927-8a1a-98f84e8afecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 228
        },
        {
            "id": "e2c73cfc-6631-484c-8678-013edb70b1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 230
        },
        {
            "id": "a94a9929-fefd-4e53-aaf2-e0c5cd154742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 231
        },
        {
            "id": "019a2710-9b3d-4c00-a2c9-8047f3e2181d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 241
        },
        {
            "id": "e1ac0bb4-e6c6-4cc1-bb33-b0d43b63d696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 242
        },
        {
            "id": "c60559c7-763a-418f-92a8-844081b5128a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 243
        },
        {
            "id": "9ef1028a-2d5e-45eb-a4be-396499c0fbf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 244
        },
        {
            "id": "90978a0d-4d32-429b-9f7d-681e802e1a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 245
        },
        {
            "id": "b826366c-2df6-47c7-97b3-2c18d22f37f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 246
        },
        {
            "id": "6ee425f7-e2ef-453a-871b-cce2f0e3aab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 248
        },
        {
            "id": "0c39318b-2890-45ee-ac8c-6ab55df0dbd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 256
        },
        {
            "id": "1323c7dd-cebb-432d-8fef-b71b342fd877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 257
        },
        {
            "id": "88c3519c-7f64-4c7f-829f-fc7cf6e21b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 258
        },
        {
            "id": "5835cca1-cd44-42b0-a178-c8114b8e10c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 259
        },
        {
            "id": "c746a419-716b-4069-8b82-e4c02f6dcfe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 260
        },
        {
            "id": "e4efe0ee-e95b-4793-9fd5-d1929415472b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 261
        },
        {
            "id": "523e1acb-9bad-4c3e-a099-4f297ef079d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 262
        },
        {
            "id": "e96b351f-faa1-4214-8ccb-b59b5062b54d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 263
        },
        {
            "id": "287b136e-1318-48fc-b031-f0b9429ae3ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 266
        },
        {
            "id": "f8641f19-68e7-45b7-8e27-308f1fb54398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 267
        },
        {
            "id": "b5507a38-1df7-4c55-984b-441890101aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 268
        },
        {
            "id": "317e8604-904c-4377-9807-e8bc0c33deb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 269
        },
        {
            "id": "02171719-eada-47a5-89b5-6763b7d8ec72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 287
        },
        {
            "id": "94d1ddff-b517-4268-b2c4-80aedd4d744d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 288
        },
        {
            "id": "1bdc5503-5ed6-4808-a950-a882b0c3aa9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 289
        },
        {
            "id": "ce309b30-4b50-4110-adee-dea479f93e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 290
        },
        {
            "id": "5db50690-56f2-4ddb-8fcb-ad345fc47a55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 291
        },
        {
            "id": "7251b8a6-ee3a-4d33-86db-eb5c140e47cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 323
        },
        {
            "id": "38fb575d-2af3-42fc-a6c4-10508c42e7a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 324
        },
        {
            "id": "821bab2a-55df-4475-a438-bde287b2e996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 325
        },
        {
            "id": "60a30c9a-6357-4e56-9537-b80101781924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 326
        },
        {
            "id": "c3423d44-7323-447f-a1d0-f2498b21f963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 327
        },
        {
            "id": "505bad0c-d777-4a10-a3a4-c7c6f67a78b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 328
        },
        {
            "id": "a6d2d53e-5181-4dd0-be1a-52274f4d7317",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 332
        },
        {
            "id": "f60c0426-a337-427a-9864-3fd866d2cf56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 333
        },
        {
            "id": "088a1974-e492-488d-9044-d42d52a16f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 334
        },
        {
            "id": "179a5a5a-4aa0-48a9-8ca2-2edbfb056e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 335
        },
        {
            "id": "666991d6-a93b-4e3b-ad92-ff38c48acb5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 336
        },
        {
            "id": "6f656835-dd1f-4a8c-a93c-a51676a24cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 337
        },
        {
            "id": "f57819fb-0329-4f72-b739-489f40498bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 338
        },
        {
            "id": "9989ff09-1d55-4114-8781-b3997afa4697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 339
        },
        {
            "id": "0d9115aa-5e74-4011-b835-e99cf0819beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 354
        },
        {
            "id": "6e864822-2b55-4e4c-9402-3693592c9e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 355
        },
        {
            "id": "8c11cad9-727e-4e82-9934-616f6f4083e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 356
        },
        {
            "id": "91427928-06af-4c05-8ca7-1c304d85902f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 357
        },
        {
            "id": "9842038d-7c79-4927-99b5-6f326654787c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 358
        },
        {
            "id": "afa8f3f6-8602-48fd-8607-360f35991fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 359
        },
        {
            "id": "1eefe6d0-15c4-4a8c-b99b-a5c0a428f2db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 538
        },
        {
            "id": "b4d9c193-de34-4b32-b0c5-651b5b414dd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 539
        },
        {
            "id": "b8e8ee52-8c7c-4d45-b746-7fa3e02c7c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 8211
        },
        {
            "id": "585f2494-6291-43e0-b5ad-1cd6c03ef5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 8212
        },
        {
            "id": "fd20e9f5-70f5-460a-8ca1-2d53fc72aead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 118,
            "second": 8218
        },
        {
            "id": "f60e2667-4dd1-4ac9-8818-97452a941716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 118,
            "second": 8222
        },
        {
            "id": "a0998b0b-5e5a-44a0-997a-7c98d3c31297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 118,
            "second": 8230
        },
        {
            "id": "7dc36aee-4d72-4e3f-8b71-75d1a5293750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 38
        },
        {
            "id": "172214eb-652a-4c20-8dea-eef87d7beabc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 45
        },
        {
            "id": "b77e1480-0d48-4d4c-bd82-9b7ca93e92d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 48
        },
        {
            "id": "aad2f11a-7dc5-4339-9586-e926cf8f9741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 54
        },
        {
            "id": "d3b9690f-bdc5-473c-9e3a-080ccc6eb6b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 56
        },
        {
            "id": "4ac3cfce-dd1d-4ece-8fe7-5919d5d5710e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 57
        },
        {
            "id": "c51ab8f9-9503-45b6-ad34-6b4195a6052c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 67
        },
        {
            "id": "edd7c114-6622-4716-9c6a-0e9c2ec8a060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 71
        },
        {
            "id": "a9b8680e-b15f-4d51-8c21-d307d3910a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 79
        },
        {
            "id": "2ab10588-d5ea-4a4a-9415-9a92cd990f14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 81
        },
        {
            "id": "7b1edb71-9319-4a36-b8f4-63bc12845f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 85
        },
        {
            "id": "0905ff0c-50ce-4328-afd4-03aac2afe2c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 87
        },
        {
            "id": "03414c94-46f0-45e8-a576-3f222405ee90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 99
        },
        {
            "id": "7cee1053-2992-414a-8d62-8acc865daca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 103
        },
        {
            "id": "b2441dfa-3ddf-4817-a40b-c098f2bfdddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 111
        },
        {
            "id": "ef9a98be-7a90-4839-bb25-37144e6a1007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 113
        },
        {
            "id": "bd3301ab-2257-400b-88b3-726bd8fa169c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 117
        },
        {
            "id": "7692c10e-6c4b-497e-a2bb-41825c3d34b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 199
        },
        {
            "id": "283ae7f4-b1e8-4dcb-b955-f3fe334d6658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 210
        },
        {
            "id": "d2ff1224-c871-4bfe-b483-be7f14a5dec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 211
        },
        {
            "id": "c4d2bcc8-d5b3-4a37-8698-8b4fb14f2a59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 212
        },
        {
            "id": "eed8d73c-73a9-4067-a769-d4dd54ec3d02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 213
        },
        {
            "id": "5378f180-db93-4144-a4bb-1ec464530e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 214
        },
        {
            "id": "3044b624-a4b8-4255-9225-d292a4a1796b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 216
        },
        {
            "id": "87041d49-7c01-4de5-a23d-ee770de45b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 217
        },
        {
            "id": "f3557787-c551-4bce-b99c-06ed836ff825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 218
        },
        {
            "id": "fd8d855a-9319-43f3-bfb8-742701ed45ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 219
        },
        {
            "id": "d976692b-f988-44d6-a07b-a4ca9e3bfa4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 220
        },
        {
            "id": "8f8926df-1f2a-4d5b-9ce0-597c60ddb6f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 231
        },
        {
            "id": "c3bb33aa-0dc8-42ff-af8b-900660771267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 242
        },
        {
            "id": "d600ee45-7702-4d81-85a5-d1273a3883fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 243
        },
        {
            "id": "f37f7720-b8a9-42b7-b95e-80b892859b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 244
        },
        {
            "id": "89f3d66d-ccde-4c35-bdb3-cf3c684575c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 245
        },
        {
            "id": "5d4fe40f-0c69-4141-8979-af17799f6759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 246
        },
        {
            "id": "a03a66a0-06b4-4d46-9172-be41dba3036c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 248
        },
        {
            "id": "b6770321-8660-48d1-8199-6ec515924844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 249
        },
        {
            "id": "61bd137a-808c-4fe8-821c-cb8e963cc53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 250
        },
        {
            "id": "138878b8-a910-4ce4-b622-0d03d079c25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 251
        },
        {
            "id": "1e9d80ef-540b-4a24-b358-b2ad8964e133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 252
        },
        {
            "id": "c74cb5e0-8d61-4860-b04f-242c8708657f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 262
        },
        {
            "id": "6ef0fe1e-5dfd-43df-b584-e4570e6ca949",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 263
        },
        {
            "id": "317f5a35-9331-4f35-8372-a55cd81a5d78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 266
        },
        {
            "id": "ae15f9dd-f8ce-48fc-b708-facf69ff1e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 267
        },
        {
            "id": "35ef2d8b-e479-4c7c-95e6-78fbe386d074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 268
        },
        {
            "id": "c5439426-399b-4029-a29c-918f31f45c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 269
        },
        {
            "id": "397f6276-c9c9-4952-83ac-41398f422609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 287
        },
        {
            "id": "355a57f2-1275-492a-80f9-75ef7c76ca2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 288
        },
        {
            "id": "ea72e8de-c5aa-4131-b2f2-d1d097679e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 289
        },
        {
            "id": "4dd92f0e-34e5-4f7f-8748-5b511b84ad9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 290
        },
        {
            "id": "0c316981-2129-4985-8084-1ac436514bb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 291
        },
        {
            "id": "b7d9cec7-8577-450f-ac39-663bd1a7265b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 332
        },
        {
            "id": "fed20798-706f-4fe9-8b28-ea5fdd854147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 333
        },
        {
            "id": "4b8db753-cfd8-4494-bca7-34bdeb60d221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 334
        },
        {
            "id": "457b9631-b2d3-43dd-ba5f-0a83d2999208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 335
        },
        {
            "id": "e4ab975b-1a1b-4d30-8c1d-15640f2d162b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 336
        },
        {
            "id": "bdabc4f1-4216-4452-b555-7ed94799d77b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 337
        },
        {
            "id": "b9cf9069-25fd-4425-baff-ec066a134618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 338
        },
        {
            "id": "5ca18dd8-0243-4500-99d2-c5a0e2a91631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 339
        },
        {
            "id": "60d8ac44-e218-469c-9dd7-3f2e8e5517c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 360
        },
        {
            "id": "81306e80-a7b9-4e44-a1be-23c509c5852f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 361
        },
        {
            "id": "d7350b3b-d8a0-4c5d-9d0e-252b55199fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 362
        },
        {
            "id": "95b91616-98c3-4729-9550-40f29d7bd3ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 363
        },
        {
            "id": "adb63bb4-1416-4bbf-a679-f056396d7f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 364
        },
        {
            "id": "8799a69f-b784-44a0-9239-72c78cf33d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 365
        },
        {
            "id": "b5f114e1-7042-41b2-9cb1-0cfe7d593c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 366
        },
        {
            "id": "11292809-064e-43e4-9c9d-fe781a3d8029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 367
        },
        {
            "id": "62fbfb4c-b2a0-4ec3-88f1-ca13022366f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 368
        },
        {
            "id": "611d3db6-19da-4b44-84e2-55e6b42e528a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 369
        },
        {
            "id": "361ae095-d1b6-45be-ae76-92607fae26c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 370
        },
        {
            "id": "5374f627-8c85-47e5-bac8-23bf532fc41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 371
        },
        {
            "id": "f2f96b9e-2b7f-4aa7-9f3f-bd64c833e1cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 372
        },
        {
            "id": "34cf5db9-2fb8-42c8-91ca-c4c55c5feddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 373
        },
        {
            "id": "131f26e9-3260-47e4-afbe-1be70bd5152b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7808
        },
        {
            "id": "26b649a3-f662-45e4-b34e-b7d61deb6f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7809
        },
        {
            "id": "4b2a9a18-4891-4202-9710-37af9b5b261d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7810
        },
        {
            "id": "c30134dd-b4b6-495c-8746-cd451dcac398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7811
        },
        {
            "id": "61e49a76-d2c0-4861-ba16-bd464363a4bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7812
        },
        {
            "id": "9e495822-1b36-4d6f-8882-e8d0e5af54f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7813
        },
        {
            "id": "c2d243c9-b311-4557-8282-fc6510c205b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 8211
        },
        {
            "id": "d1ec501c-b21c-44f3-a3b8-2cb647ed377d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 120,
            "second": 8212
        },
        {
            "id": "e5ed8119-a9cb-429b-bea1-19506d32602a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 47
        },
        {
            "id": "1536c4aa-9c5d-4ec9-95a8-aded3064cea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 74
        },
        {
            "id": "5a2b224a-cf0b-4872-afb2-bcc04eed315d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 88
        },
        {
            "id": "0792b9e5-45eb-433e-bf49-117a5e9988da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 106
        },
        {
            "id": "220f8695-7f37-4ccd-b02f-4e26fb233859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 120
        },
        {
            "id": "2fe5f2b0-9486-45e1-8f3e-daa348e40e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 122,
            "second": 45
        },
        {
            "id": "42bce2e4-5494-46c6-8c4d-850060cce28a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 122,
            "second": 89
        },
        {
            "id": "add0a2fa-a94a-4e67-b88d-c7ff169a5d7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 122,
            "second": 121
        },
        {
            "id": "e3f5e5ba-4d03-4db1-8afc-cfa43d35b8ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 122,
            "second": 8211
        },
        {
            "id": "e011e240-933f-4a09-9fc1-54dae1c08664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 122,
            "second": 8212
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 70,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}