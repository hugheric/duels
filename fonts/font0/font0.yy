{
    "id": "33ade2f9-e926-4101-89a2-7f029c90ad60",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Johnny Fever",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4ee79077-1aa3-4054-bf70-ac749e56ac2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 48,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 257,
                "y": 152
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6de20b3e-f153-45f7-b2cd-536bd8d77a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 48,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 341,
                "y": 152
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "35154875-8c9b-4258-9cd0-5146c066494d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 48,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 147,
                "y": 152
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "806011fe-2409-4fe4-a758-09285897e682",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 48,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1fed489d-710f-4703-9422-54cb80b67100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 461,
                "y": 52
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "47f53205-9764-48b5-ae51-b586b85fefc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 48,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "57b295dc-a3ae-4057-adb8-48a65c3b2edc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 349,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "fe8fa65b-c9b1-420a-8a44-cf787a112afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 48,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 359,
                "y": 152
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "128a2e74-2eb4-4712-9f10-9c31b0e3007a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 48,
                "offset": 3,
                "shift": 13,
                "w": 11,
                "x": 192,
                "y": 152
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "958711ff-a9bf-4b44-9d18-99c195695b4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 48,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 270,
                "y": 152
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7f4f3267-9639-4465-b785-980fde3dc48b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 48,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 325,
                "y": 2
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "37948c60-40d2-4e29-81fa-04e814ce5df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 462,
                "y": 102
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2503c7bb-3d06-4564-8eb7-9fb9d51d8823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 48,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 304,
                "y": 152
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c2c94a72-bc47-49ef-8ba8-a72d810c772f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 48,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 163,
                "y": 152
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "03f8be10-cc83-46f4-bf41-a41db47396a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 48,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 314,
                "y": 152
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "601015c1-e57f-4471-98c8-bdbfb3486ac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 48,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 149,
                "y": 102
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9b83a96f-a6ac-465d-b5c9-6ec40dd13046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 170,
                "y": 102
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2b496248-f5ea-4e1b-80be-61631f43dbcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 48,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 205,
                "y": 152
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3f80118a-0ba5-4075-ad73-b44474c9f67e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 212,
                "y": 102
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a266e7b7-d7f5-404a-a930-8aae8b33f2f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 48,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 42,
                "y": 152
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "301179d7-c14c-4408-843a-27e1bf07926c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 442,
                "y": 102
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e8bf7e1a-75a6-4529-8d85-84621c5d246b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 254,
                "y": 102
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "089204e5-3393-4a57-a6ab-7ff5e1606732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 296,
                "y": 102
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1966a61a-7729-4163-b02e-849c5aa1a673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 48,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 372,
                "y": 2
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7802e59f-5cfb-43e0-9a51-876423c160c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 317,
                "y": 102
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bb28dea9-a90e-48cb-a859-a5d44d2866a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 338,
                "y": 102
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "862654df-4dba-4bcb-85a0-d9eda2576a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 48,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 350,
                "y": 152
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c49a801f-11d2-4200-ba98-2517be4e8cc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 48,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 294,
                "y": 152
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e7cf5bfb-25dd-4723-95d6-f7bb36b0dbd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 22,
                "y": 152
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5c074f60-c800-445e-8672-b09f9354e070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "38ade28d-a493-4c66-ad81-366a31e5db5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 482,
                "y": 102
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d5171499-3651-45d8-913e-c18c6a32d0aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 48,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 244,
                "y": 52
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ac24ed2a-a64c-4c6c-bb9b-24170bd66c7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 107,
                "y": 102
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a72f4697-7b55-439f-83b3-44eb00e4f1ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 68,
                "y": 52
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "92cb623f-5c1d-4715-888a-1253e666ac55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2b7078df-fef8-4b24-bf47-3fac41a2b69b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 359,
                "y": 102
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a63424f4-7c9a-4abe-95f5-031e6d2a81b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 417,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6fe3b371-96e5-446b-94ae-eedb10499eb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "26b56d23-23aa-444a-8d0a-d1bb1b46cbc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 461,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8263c016-bf51-4dd7-b3da-29b685d26402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 275,
                "y": 102
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5f4e1233-cff8-4083-8083-d626a0b69376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 483,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b2e67915-d594-4d7f-bc83-442dc1f26394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 48,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 332,
                "y": 152
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "182287ed-aafa-483c-a782-ead37ad7f734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 48,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 96,
                "y": 152
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6109fd9c-86e6-4860-a967-2407c877ad67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d99d6d69-7985-4af1-a067-f3da7317f981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 48,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 79,
                "y": 152
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "73d8673c-0f0a-43d3-998c-b23af69ee19d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 48,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e537c871-650b-4e75-9f5d-c1dceb97e6f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "63906674-702b-4019-8b41-9691fe839f2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 401,
                "y": 102
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3692b736-4da7-4a8f-bb69-7c53e530a497",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 90,
                "y": 52
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a084a7a7-320b-4b3b-9995-3a86b3395bd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 482,
                "y": 52
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1170ea6a-01b2-48d2-ae28-6bef0d776a7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 112,
                "y": 52
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "61d07663-b3b5-4c64-9b22-8ee5993ec6b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 419,
                "y": 52
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6e68272e-45ea-4063-ab3a-bc56a526c5ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 48,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 134,
                "y": 52
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0715220d-796a-4eb2-8d04-20904b9a4690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 440,
                "y": 52
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ef46f356-d463-4947-91f2-4adec83632b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 48,
                "offset": 0,
                "shift": 22,
                "w": 23,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7bcacdd9-8fc5-4696-9427-2e933303c689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 48,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "18e0c6a8-a604-4e26-a618-44b36cb24cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 48,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 250,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f20c1a27-66b1-4841-96ae-2ffb36530d7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 23,
                "y": 102
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5b82ab5d-d261-4c93-84ec-d6823fb96425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 156,
                "y": 52
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1ed68b8e-a859-4a6c-a89d-fc91b01d135a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 48,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 282,
                "y": 152
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "86a840f0-9662-4657-a8e3-c62010ea6ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 48,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 65,
                "y": 102
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2add7d33-e6bb-4fbd-a1c0-7f57b6278e4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 48,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 244,
                "y": 152
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b89e88e9-e589-4b50-a417-b0cd6e410afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 48,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 61,
                "y": 152
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "297087d6-3d3c-483a-b5c7-222382b5805c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 48,
                "offset": -1,
                "shift": 23,
                "w": 25,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "74d94a5f-a827-4883-b6fe-864f4a9bf697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 48,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 218,
                "y": 152
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f7fdfaab-9f69-4429-9e53-af56f3059973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 222,
                "y": 52
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0e27cbb1-b852-4ba4-9e7f-bb2c571c7c41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 178,
                "y": 52
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9f2cd24b-89d2-47dc-9188-cf0d93ca3658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 128,
                "y": 102
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7fd8e3ad-acd8-4453-8ce4-74d4a4b749db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 46,
                "y": 52
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "23622c12-8d85-4179-915f-c04c6eb4f245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 24,
                "y": 52
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "28ce4a5d-8772-4923-a269-d697646ce8da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 200,
                "y": 52
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1958570f-3bbb-4cd9-b892-e77f032c80a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 380,
                "y": 102
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5d2d6c45-89cf-47b4-94bb-688853978dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 288,
                "y": 52
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "16b0d5e0-5617-4cfd-ae21-85a6e646f881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 48,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 323,
                "y": 152
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "82a5be27-46d9-45c9-a6e5-5f71fd529f1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 48,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 113,
                "y": 152
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e3075217-c03b-45f3-b968-958b960f20ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 275,
                "y": 2
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5e78749f-b697-4793-ac87-2ab62a230ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 48,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 130,
                "y": 152
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "12b71940-8e74-432b-b4fb-cce23543398b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 48,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f09a0066-cc81-4bed-b64c-b35603d3aa10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 310,
                "y": 52
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "bd37f357-17e5-4623-b6a7-0f83ee6627ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 233,
                "y": 102
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ddb932f7-5132-4970-9c56-040512de9833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 376,
                "y": 52
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "91ddb5c7-06b2-4ad6-9c70-88baa396d774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 191,
                "y": 102
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3d7915a6-401e-47cb-8e6e-e64ab479df86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 266,
                "y": 52
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d69c782f-70e6-4b3d-bd96-33afbf1d26a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 398,
                "y": 52
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "55fe07c3-f8d6-45d5-8cba-347b6d02316e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 48,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 354,
                "y": 52
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c28ab1d4-dc7a-436b-859f-beace80f81d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 86,
                "y": 102
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "500bb0b8-47af-4115-b867-4fa642bf66f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 48,
                "offset": 0,
                "shift": 22,
                "w": 23,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "af5fd414-7c72-48c3-ac53-557b67b90c8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 48,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b9dd1a6e-6e96-4c95-a23b-0f8269b261e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 48,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 300,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9bff4f27-8995-44c5-ada7-bbdc082e34f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 44,
                "y": 102
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "041a131f-e810-4765-b979-10dd21bb3439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 332,
                "y": 52
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "53e4e0ed-fa26-4864-97ec-9e5b792087c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 48,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 179,
                "y": 152
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "00b87c55-929c-49b2-ac4d-312a7329b654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 48,
                "offset": 5,
                "shift": 15,
                "w": 5,
                "x": 367,
                "y": 152
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "db6ab462-cf03-4a6d-999e-fc5024c06ac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 48,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 231,
                "y": 152
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f5905949-405f-4b47-8512-d430515a987f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 48,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 422,
                "y": 102
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "d15f2e3a-c4a4-484f-a0f2-e2845954991d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 74
        },
        {
            "id": "77174ae5-49d6-4f6c-b899-0f0838d5a841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 106
        },
        {
            "id": "d26797cf-7087-44d7-bdc2-721d5fd13a1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 34,
            "second": 34
        },
        {
            "id": "0e421d19-31b6-4a22-9ab1-ca17ab889f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 34,
            "second": 39
        },
        {
            "id": "05854241-fb90-4577-a19b-8a8e4f53b36a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 44
        },
        {
            "id": "67208733-a50b-4a67-9cb0-a66286021b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 46
        },
        {
            "id": "c8689143-2f07-4132-956a-a8356e888efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 74
        },
        {
            "id": "79c2307b-4600-4090-93fe-8a30c24a578b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 106
        },
        {
            "id": "1b00981b-a6b0-4b17-b877-a7bccf553b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 34,
            "second": 8216
        },
        {
            "id": "24cdcac3-9f6a-48d8-84bc-c0556bae1545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 34,
            "second": 8217
        },
        {
            "id": "bfd19b58-ee33-4315-92e1-b867ccd0167d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 8218
        },
        {
            "id": "9e59f575-8dec-46b9-8180-dbd33813c845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 34,
            "second": 8220
        },
        {
            "id": "c5f689b4-ef83-4405-8bb9-67e9acdef468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 34,
            "second": 8221
        },
        {
            "id": "0c643025-5ea8-4778-a39b-8cb52e159720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 8222
        },
        {
            "id": "2b2120e9-e5ba-4d7c-ae9f-8498095e8da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 8230
        },
        {
            "id": "d4f62fc9-c431-43ef-869c-f1684c4b39b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 84
        },
        {
            "id": "d0a51eef-55ae-4414-b88a-01e15e32a50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 86
        },
        {
            "id": "8c609d19-059b-4e84-a913-b8e041000835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 89
        },
        {
            "id": "11995116-b4fe-49e2-87af-50fd88e0c41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 116
        },
        {
            "id": "f4899d74-540c-4ac9-9751-be174afd81f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 118
        },
        {
            "id": "9c44bc72-f4a3-4fd1-b60a-a9e9bf97ffcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 121
        },
        {
            "id": "5374a32f-9525-4544-9037-e4eb16dbd2f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 354
        },
        {
            "id": "804d91bc-7876-408e-bac0-46b166896869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 355
        },
        {
            "id": "3817d098-b96d-403d-96d1-3a81ea28c474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 356
        },
        {
            "id": "4769a29e-3813-4abc-bd16-c0c3f0e1e238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 357
        },
        {
            "id": "9bd4fbc0-2d11-4541-8f3b-643062e01a95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 358
        },
        {
            "id": "e2780941-832f-4bde-9f07-8202d9015ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 359
        },
        {
            "id": "0fa03e44-dd10-4274-9238-e7833c03e665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 538
        },
        {
            "id": "34d6b9a8-91b5-400f-b612-ccb9065be925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 539
        },
        {
            "id": "8a31e181-e4c0-40c4-b00f-66ecbec14c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 39,
            "second": 34
        },
        {
            "id": "0302862c-5e9e-4cb5-bfa3-3cc3ee14939f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 39,
            "second": 39
        },
        {
            "id": "ff28e5d2-8f95-4773-9bb9-538033d85f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 44
        },
        {
            "id": "1e0bed22-35db-4573-b90b-cf97a7f34bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 46
        },
        {
            "id": "925b467b-0e32-4be8-be70-3875d5cf64b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 74
        },
        {
            "id": "4c77c6cb-2ca9-413a-82d5-b567b31575f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 106
        },
        {
            "id": "f5e92c0c-b42d-404d-a09d-1dc0bcc94ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 39,
            "second": 8216
        },
        {
            "id": "a04470c1-bc3b-493a-bd4c-9e3a5f37e3e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 39,
            "second": 8217
        },
        {
            "id": "73568f24-ccc0-479d-a150-98ce232c605f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 8218
        },
        {
            "id": "c64b99eb-6e0a-4bea-a4ba-b8b662223e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 39,
            "second": 8220
        },
        {
            "id": "20a21f80-8ecd-4da8-8c6a-f92449ac7b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 39,
            "second": 8221
        },
        {
            "id": "a7bd9494-2924-44af-a011-b4268ea6535a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 8222
        },
        {
            "id": "72d5e2db-d229-4897-a68e-c762738cc73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 8230
        },
        {
            "id": "761df715-80d5-4c2d-8b77-df4a32a1d4f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 34
        },
        {
            "id": "4bc5323c-2597-4729-aebb-1c0e18fe210d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 39
        },
        {
            "id": "67c4a3da-1d2d-4019-9705-d9bdcc81cdf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 49
        },
        {
            "id": "95a77131-b670-4ff9-bf8c-55bd7d8a4a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 52
        },
        {
            "id": "d8e49222-9d56-4d12-a3f5-ffb4ed9a0db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 84
        },
        {
            "id": "0c72347e-2907-4085-b354-1450cff04f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 86
        },
        {
            "id": "9a391c04-c5d5-4534-855b-a4929de5e125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 89
        },
        {
            "id": "3b7b0301-ca09-492e-9c83-ce16d907ab3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 116
        },
        {
            "id": "a37711a6-fed3-470d-87d7-3073bfd18c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 118
        },
        {
            "id": "555c7252-5ea9-4a5f-926e-80c3a1d1f6ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 121
        },
        {
            "id": "5639c65a-7740-49d4-bcbb-2afa7acc4474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 354
        },
        {
            "id": "374368a9-a6b1-4dcc-b17d-7ddc12256062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 355
        },
        {
            "id": "8bc470ab-3b24-40f0-8165-d89cdf635545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 356
        },
        {
            "id": "91f1b971-d5bf-400d-995e-9334beda165f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 357
        },
        {
            "id": "83641f30-f2b1-432e-a873-0d24776cbf77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 358
        },
        {
            "id": "55fa6ea5-a59a-47bf-8a98-858c2eaa64af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 359
        },
        {
            "id": "63459d3a-73d5-4955-bf91-cfd3ac0dc405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 538
        },
        {
            "id": "4a0eba5a-5e6a-481b-95db-44729a4c5291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 539
        },
        {
            "id": "0147b2ed-7909-41d5-b60b-9857d19afb1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8216
        },
        {
            "id": "64738b34-2992-4f89-a3b1-f7a88447e074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8217
        },
        {
            "id": "f1e2db50-b8bc-4866-aed8-cb5eeb49c7b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8220
        },
        {
            "id": "21208711-ba8a-402e-b21c-33c6660e03cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 8221
        },
        {
            "id": "60795ba5-7f2c-4523-8e76-1798d492e982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 74
        },
        {
            "id": "0a1e2982-8592-4d9b-a186-6bf65b2a40a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 84
        },
        {
            "id": "d145e4f1-28f1-413a-98d8-e18c2c4ea41d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 86
        },
        {
            "id": "1153c463-194d-4df4-9af8-529b135dd6e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 88
        },
        {
            "id": "693eb563-ffd4-458c-8830-2d2182b0f9da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 90
        },
        {
            "id": "de5d68b6-4224-4367-a1d1-fef2110656c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 106
        },
        {
            "id": "a003d96a-e2aa-492c-938c-6c9f7b1ba1d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 116
        },
        {
            "id": "80764a4e-535f-4698-893b-fc53aeaa5a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 118
        },
        {
            "id": "567c7c00-7f76-4204-b0bd-129fc5bc6a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 120
        },
        {
            "id": "c66b9da4-ec02-41f7-bee0-6744f7b23ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 122
        },
        {
            "id": "8561af41-724a-4c07-981f-63ac56eb8eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 354
        },
        {
            "id": "d72d6020-d6c7-42ff-be2e-7961473c4b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 355
        },
        {
            "id": "c965be51-bdb8-4433-a210-271dbc30457a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 356
        },
        {
            "id": "735c27ee-60b7-436c-b968-7ea65fc8e86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 357
        },
        {
            "id": "ab681530-a1d9-45b9-82cd-82684b54ee04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 358
        },
        {
            "id": "33e03fef-a5da-41e9-9beb-ea15104687d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 359
        },
        {
            "id": "5c65af7c-7f59-40eb-a088-2f2c788ba966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 377
        },
        {
            "id": "338892e4-021d-42c2-834d-79e813f99797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 378
        },
        {
            "id": "32ae6c3c-92b7-46b0-950e-7c966ae89c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 379
        },
        {
            "id": "87784f01-4246-43ae-8cfb-047ddf484607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 380
        },
        {
            "id": "672e6347-fd44-4e5c-927c-cd53d08568b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 381
        },
        {
            "id": "63747cca-1248-46e1-9eb8-dd03f7502d08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 382
        },
        {
            "id": "15e37129-9c74-47bc-897d-ad36e2f90c2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 538
        },
        {
            "id": "ceedbea3-ef72-438f-8ab1-6c7ec36e6d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 539
        },
        {
            "id": "fc9a9b49-e685-4902-a516-1b214df77c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 34
        },
        {
            "id": "53b8a429-c88b-49ea-a6e3-baa6f73929e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 39
        },
        {
            "id": "d716fe0a-9335-4bc2-919c-42333395474d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 49
        },
        {
            "id": "986d5ac3-265a-4fa9-9cd1-52dcc2611fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 52
        },
        {
            "id": "2135841b-fbb3-4205-8cf3-7421bf20630b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 84
        },
        {
            "id": "f0d6111f-45f2-4822-af54-4cf6e1b95a8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 86
        },
        {
            "id": "8695bfd2-d3cc-4f06-b0de-621d0cb18008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 89
        },
        {
            "id": "7f04926a-e872-4d8d-8a25-a7f4945a94d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 116
        },
        {
            "id": "b07de4b1-1ab4-4c9b-a82a-eaf2d69bcf7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 118
        },
        {
            "id": "114bac50-cb48-4639-85bf-8c7bd395d80b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 121
        },
        {
            "id": "992c7a42-388e-416a-b581-1c8ca47366a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 354
        },
        {
            "id": "18c1287c-e544-4194-97fe-c80e86f099cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 355
        },
        {
            "id": "7fdc3825-de2d-4ffa-9858-7085a7bef06c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 356
        },
        {
            "id": "d537c911-74d6-4d9a-85d7-7cdd345a240d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 357
        },
        {
            "id": "62e7ad2e-b730-465b-aae9-934d60b2df74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 358
        },
        {
            "id": "56b5089d-e31d-41c2-bf4e-ccc6ce8e3c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 359
        },
        {
            "id": "e9d4a8a3-177d-41c2-8f56-67b07ce2d9bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 538
        },
        {
            "id": "578cc8ce-22fa-40c2-8cb6-c5c56091af8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 539
        },
        {
            "id": "442bd2ba-c8ba-4f7d-becf-bff24b8a8764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8216
        },
        {
            "id": "f759b871-d5a0-4a1f-8099-eb4b3d6c09f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8217
        },
        {
            "id": "ef7814ec-ec32-41b6-9bad-4a8673466ab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8220
        },
        {
            "id": "a65ebb9b-360c-40f0-9e17-d4d453d65c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 8221
        },
        {
            "id": "f9680f7d-80c0-4258-943f-e8065d2a1c28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 48
        },
        {
            "id": "f54116d4-bb1c-4b70-86a1-8b99f39f07ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 54
        },
        {
            "id": "9c04d608-074a-4ecb-890c-7b903609caf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 56
        },
        {
            "id": "2eeff4f3-07b6-4f3d-901e-ed6926c64e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 57
        },
        {
            "id": "daa2157c-2d53-46e0-acdb-415983c6f02a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 65
        },
        {
            "id": "17a18dbf-caaf-487f-8137-da30146d7dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 67
        },
        {
            "id": "ca2d5f8d-adc5-4c73-9a9d-d909194a0a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 71
        },
        {
            "id": "d81b8d23-f586-40af-b72c-77cb658b3752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 74
        },
        {
            "id": "a2166b9b-a9d0-441a-9ea7-02d1a8584fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 77
        },
        {
            "id": "c281b719-f74a-48de-b5c9-5c4d56a5eaa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 78
        },
        {
            "id": "f0af4807-8de5-463f-9f84-7371bc8b6b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 79
        },
        {
            "id": "b6d61570-0ccf-4279-9aef-673f8d216c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 81
        },
        {
            "id": "4ec5983f-fef3-4cc4-a222-7c98bcb41b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 83
        },
        {
            "id": "be64c7ac-015d-4732-b8b3-f81acf2c5b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 97
        },
        {
            "id": "bd5bfe78-a538-4463-8409-7faa4648cb61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 99
        },
        {
            "id": "5486c179-5e6e-43ce-ade3-df69277d6cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 103
        },
        {
            "id": "1f89b59f-2cec-4889-99fd-4b031eabd42f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 106
        },
        {
            "id": "162a629c-3e3b-4114-84d0-0eddda2a3f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 109
        },
        {
            "id": "3d4ad58c-014d-4cdf-8d50-dd1e05606ba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 110
        },
        {
            "id": "bfb24349-01b9-41c3-ae46-9a4eb30e4f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 111
        },
        {
            "id": "0305f40b-21cb-4bd6-893a-41f1ca83920f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 113
        },
        {
            "id": "81fd4acd-26d2-405f-87c6-7c2ab6140a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 115
        },
        {
            "id": "31495b0f-00ed-4c2d-b4f7-f109f8dd0788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 192
        },
        {
            "id": "f3f55a19-d9ee-433b-82a8-4fda1dd3db6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 193
        },
        {
            "id": "2affaee0-4af9-4ac3-b1c9-37e2034050b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 194
        },
        {
            "id": "a26cfaac-694f-4c25-9170-3eec4b8b707c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 195
        },
        {
            "id": "a69cfa39-ade3-401d-a4d5-a1eb5a85e238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 196
        },
        {
            "id": "e176877a-1998-4c8e-ac5f-1245e54e04c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 197
        },
        {
            "id": "973de76c-0b7b-4954-8a16-53e290a59e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 198
        },
        {
            "id": "aee94f1b-f918-4eae-8a72-0e0a9232d262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 199
        },
        {
            "id": "f7d412cb-8dff-46d0-8920-8b31724519eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 209
        },
        {
            "id": "e2b91ee5-6b49-4a02-b0db-d99f8242aacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 210
        },
        {
            "id": "a4264824-46ea-4c55-995e-72836ba6443e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 211
        },
        {
            "id": "d4089953-0fc9-4587-93a7-68e688638807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 212
        },
        {
            "id": "93b90a37-8183-4dd3-9a4f-5e862bec9ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 213
        },
        {
            "id": "738ee244-089a-49c9-938a-7f66a3a8741a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 214
        },
        {
            "id": "b5720ab6-3267-4310-97f9-70f2d6cbe461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 216
        },
        {
            "id": "bbb3b1eb-56cb-4725-be7e-1ed1906bc86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 224
        },
        {
            "id": "d8f2e6de-d0c5-4d97-b1d7-a47460f2469b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 225
        },
        {
            "id": "34131027-88ca-48ae-9e6d-be92a6ab11c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 226
        },
        {
            "id": "bd36a885-ab9f-487d-b1f0-d7670b89f446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 227
        },
        {
            "id": "d0b7248c-52a5-4c31-aec9-3588a948d7c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 228
        },
        {
            "id": "2ac45fe4-293a-4e0f-a719-462b5748818f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 230
        },
        {
            "id": "523ab49e-5c77-4bd7-a57a-e96a24043961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 231
        },
        {
            "id": "14616d40-2494-4717-a825-fb00eb022ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 241
        },
        {
            "id": "c730bf74-e7c7-4308-83e8-eb3b5af41cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 242
        },
        {
            "id": "b1b1c5d3-383d-4454-a7b0-a8e3696ed910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 243
        },
        {
            "id": "2ee9496c-4b23-4b91-990d-1063e0ac3da7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 244
        },
        {
            "id": "5e75b558-071c-4e3d-8e27-2f91789e4e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 245
        },
        {
            "id": "914160c2-c8b6-47e8-b38d-274f8ab0b7cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 246
        },
        {
            "id": "1301ccb3-3068-4550-b769-fbdd854de623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 248
        },
        {
            "id": "c1b094c1-80c9-45c4-8c7d-fac38dfcd3bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 256
        },
        {
            "id": "62dee1a9-3dd2-4f20-873d-9f8f336b18cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 257
        },
        {
            "id": "0d66cd26-3005-4813-8914-6587a238ba1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 258
        },
        {
            "id": "f8b713a9-de8c-46c3-89cc-14c548839717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 259
        },
        {
            "id": "6f3a3190-e291-4f86-b27b-952929e91f72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 260
        },
        {
            "id": "739daeac-06e2-48f6-b2a1-3e2534fb406a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 261
        },
        {
            "id": "767a6cdc-1f72-4999-96a0-eae9ce53a09e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 262
        },
        {
            "id": "ba3c16d4-df96-4c6f-a23c-5037562c1098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 263
        },
        {
            "id": "5f41472d-d163-4b0b-aa61-ef496259e50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 266
        },
        {
            "id": "c5545d7b-ea41-4932-a88c-e5bcc0b572aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 267
        },
        {
            "id": "05b08ce4-7010-4208-902d-a1564deb39b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 268
        },
        {
            "id": "35b01d09-a497-4510-b5f4-547488613fb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 269
        },
        {
            "id": "2ee7ce1a-627b-42f5-9ffb-98b5d57952b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 287
        },
        {
            "id": "7892430e-e44a-4717-b7e0-c585ade1698b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 288
        },
        {
            "id": "d7da2797-ffb8-4afe-a13a-59e73752b9e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 289
        },
        {
            "id": "3e76ab6e-d055-4625-9b38-5c281ce2764c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 290
        },
        {
            "id": "cccb2965-dff1-4be3-a79b-b1063b05f3cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 291
        },
        {
            "id": "12242835-829d-4184-89b8-6681b3320e90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 323
        },
        {
            "id": "7bd23298-bfc5-4692-b685-ec68c84b01c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 324
        },
        {
            "id": "4e67a1fd-79df-4410-90e6-e442f05d36c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 325
        },
        {
            "id": "9e139dba-0497-47a4-92ad-69d4018931cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 326
        },
        {
            "id": "fbaf7164-0bff-4ff2-9c60-e2e4033a4d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 327
        },
        {
            "id": "c2d0c2fa-6bb2-4fcf-8196-88227a89a76c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 328
        },
        {
            "id": "ea5a9198-c815-4426-ae8b-29a8e7c50bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 332
        },
        {
            "id": "de3942e2-0a67-4cf1-b576-42da2105f9ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 333
        },
        {
            "id": "6788bd25-323d-4f0a-90bc-7f557bb6edd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 334
        },
        {
            "id": "2c466786-4415-4c3f-a744-29f4d3ca2900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 335
        },
        {
            "id": "ca71381d-f58d-442a-802e-b14375424777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 336
        },
        {
            "id": "975158e0-43a7-4486-b2d2-f88b67b4f8f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 337
        },
        {
            "id": "7df73daa-13bd-46c5-a619-daa31406f893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 338
        },
        {
            "id": "bf581831-4be3-4e97-a997-394160177f25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 339
        },
        {
            "id": "34320355-6ddd-48b1-817a-87c97071a77b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 346
        },
        {
            "id": "d74ffa8f-b086-45e6-af12-21fcfeac9d7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 347
        },
        {
            "id": "4598825b-8c69-4831-b55e-755b9a410942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 350
        },
        {
            "id": "8783738b-9e62-4edc-8575-421481736fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 351
        },
        {
            "id": "754cf5d5-b45e-4dd3-bcbb-668ed6e9f564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 352
        },
        {
            "id": "b14ca00e-4459-4e76-a8eb-5eedbd0401b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 353
        },
        {
            "id": "23c1d75e-fcb7-418a-95d1-d2311ef5d0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 536
        },
        {
            "id": "1f147f26-7cd0-48f9-aa15-7e154d9afeaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 537
        },
        {
            "id": "e3a15a45-942a-4f3c-aebb-a875fc84d2b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 47
        },
        {
            "id": "5321a03a-bec8-40b8-b1b2-47b9b2ce8856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 55
        },
        {
            "id": "44b14cdd-861b-427f-993f-6b1f4ca812a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 86
        },
        {
            "id": "d2862eb7-68d9-41d4-bb65-fb8b9284b585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 88
        },
        {
            "id": "cbb05569-3899-42f7-b66d-2cd731748e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 118
        },
        {
            "id": "7203fdd3-ad15-4a39-a193-7509b4bf0366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 120
        },
        {
            "id": "f6f750a3-727a-4171-9abc-0e9986b3d6d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 55
        },
        {
            "id": "e2ee7ec2-096a-48f0-8ebf-70e0ca1908ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 49
        },
        {
            "id": "a825f9c1-1e03-4343-a5c6-4908f5e50857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 55
        },
        {
            "id": "1c4cc544-b8f5-43d5-9e5e-fa456ce40ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 47
        },
        {
            "id": "b5aae4a5-d84d-4601-b843-99acc15124ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 55
        },
        {
            "id": "4e721ccd-49cd-405e-b1cc-fa12c23d2d92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 86
        },
        {
            "id": "39efefc3-44fb-45a3-8816-8c7dc87f95b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 88
        },
        {
            "id": "5ec533e1-e307-4891-b4c1-7bfeb72de1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 118
        },
        {
            "id": "8b7c4bbb-7985-40ee-9c18-15287871fc3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 120
        },
        {
            "id": "072b5645-8d00-485a-b6d1-1863cffd290b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 44
        },
        {
            "id": "13ccc779-8767-4c44-890c-72fc82b29ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 45
        },
        {
            "id": "0b1ea912-5b38-4df3-a2f7-4dbf0ea67548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 46
        },
        {
            "id": "6ec35454-5fd9-4658-b500-bd3c63ff6e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 50
        },
        {
            "id": "4456f3de-bcbf-4264-bf58-5a2fdbc669d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 8211
        },
        {
            "id": "975ba02f-a583-4d1f-a799-b016b9b1b118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 8212
        },
        {
            "id": "8b3cd29a-3b74-462d-8326-20dcc98c5376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 8218
        },
        {
            "id": "df9fd33a-ca5d-4062-b974-bf65a263392c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 8222
        },
        {
            "id": "ad19725a-768a-492c-aaa1-8d6b068e73d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 8230
        },
        {
            "id": "985d07e4-15a7-4f93-92d5-24c0a2c97daf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 47
        },
        {
            "id": "aa96dd85-4953-4d7d-adae-eacbe4908f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 55
        },
        {
            "id": "4acddf42-2736-4263-a30b-24b0552ac624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 86
        },
        {
            "id": "bd11014f-6253-4449-96e5-d992e92102dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 88
        },
        {
            "id": "195ff7b7-04a1-4d54-bd4a-86e0d4ade85f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 118
        },
        {
            "id": "f74be219-2fb3-4af0-8c8c-fa68fd33bcd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 120
        },
        {
            "id": "4adc0d55-9c3e-4a0f-9799-d006c6bd9136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 47
        },
        {
            "id": "0cffb281-e578-48cd-9307-bbbf187a0ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 55
        },
        {
            "id": "a5685ad8-3c97-4dfa-b401-0f1795afe125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 86
        },
        {
            "id": "b9b2a3db-fbb6-4baa-b060-c50815add9cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 88
        },
        {
            "id": "82d4c408-5e15-4aec-9161-694242c0261a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 118
        },
        {
            "id": "1760e3bd-3d47-44b3-8516-6d054256cdd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 120
        },
        {
            "id": "0c982bce-3a36-4e11-b78e-e7b7b35d16e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "52f6d9b5-6bd1-4840-bf53-e6d138f265fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "91929191-db24-4548-b36a-2c746a0aac02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 47
        },
        {
            "id": "62cc1142-84e9-4246-aaf2-b80f230bf887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 86
        },
        {
            "id": "391710d2-b19d-4f26-9248-b039455580c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "94e7a432-70a9-475a-ae36-2a6d230ac8b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 118
        },
        {
            "id": "cab132ea-a058-46a9-99da-49cff99c479c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 120
        },
        {
            "id": "37ba6bdb-2030-4db7-a25a-88d0f06eb209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 45
        },
        {
            "id": "d028c035-27a2-4693-bfd4-f6fc3b4f10c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 8211
        },
        {
            "id": "6df615bb-4903-4b78-a588-6412910c3d28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 8212
        },
        {
            "id": "70fffe39-9f83-4224-b074-cb85ab546534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 47
        },
        {
            "id": "0c912918-676b-47cd-921c-bbb55e0ab0a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "696f5504-5eab-40d3-bace-71dff0a5b5ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "354066bc-092c-4954-9170-c631ec07dfd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 118
        },
        {
            "id": "a9eb4440-0466-46f5-aa2c-f033519c168c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 120
        },
        {
            "id": "5c0a56f8-6ac7-4633-b11d-5bcce0527aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 45
        },
        {
            "id": "9c57c3b6-59b4-4295-8226-d28f59fb4233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 8211
        },
        {
            "id": "b90c2672-bcd7-43d4-af73-a190b9fe8ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 8212
        },
        {
            "id": "bae0e5c2-69c6-4a4f-8640-908c80c70813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "5c09ce8e-c4b1-4781-ae3c-cefac876b842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 45
        },
        {
            "id": "c0dc86de-abe9-40f2-b35b-1f78f8795455",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "b017cbeb-0f40-4dab-b714-e79c9170df8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 47
        },
        {
            "id": "7b8fc362-efe7-4490-97ca-60f1d1693a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 74
        },
        {
            "id": "2e5d2bfe-8494-43e6-83ef-3f8a2a0bbb83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 106
        },
        {
            "id": "f891721d-94b0-459d-b387-af3b674151a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8211
        },
        {
            "id": "640d78e6-7da7-4dad-afb4-7b53c74a8eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8212
        },
        {
            "id": "4130b6d5-93ec-45d9-9ab3-d25b5925381a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 8218
        },
        {
            "id": "4ab1b8e9-0b76-4827-8910-2c8391fef75c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 8222
        },
        {
            "id": "33a09e86-cce0-4537-8449-98e84710000d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 8230
        },
        {
            "id": "7ac02c52-6239-4b8d-a98f-9b129b18fd2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "1422f51a-ce03-4b51-b0f9-b3f18097ae96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "875f7378-b2a2-46b3-96fc-7137cdb8c918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 47
        },
        {
            "id": "862e2744-1b37-487d-a64c-08b9ba3c6d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "f821bafb-9aa4-4e80-90f2-01aa5519b00a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 88
        },
        {
            "id": "b6e8f7da-625d-4379-a695-2754c3289454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 106
        },
        {
            "id": "b619efb2-79d0-4fcb-94df-4ec4e649d19e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 120
        },
        {
            "id": "c6e7befd-3e13-4c41-8b28-9788d33050ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8218
        },
        {
            "id": "60f9f379-8892-4bd7-8a7d-76e9972e0167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8222
        },
        {
            "id": "a61f1afb-b5f4-47c6-86ff-dfe4112f17b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8230
        },
        {
            "id": "2d698906-9459-4e31-8309-c2ada92ac8ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 38
        },
        {
            "id": "4f07ac28-af53-474f-b570-1762836cc37e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "950b4158-b287-4c3c-9988-9f364a40df01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 45
        },
        {
            "id": "9c407ed2-405a-47f2-8241-abeb394ac5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 46
        },
        {
            "id": "a7534338-1887-4386-971e-b40c83ca8b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 48
        },
        {
            "id": "6d93f294-5584-4aab-be03-9e67d833f90a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 54
        },
        {
            "id": "a7cbae0d-f811-4c39-8d03-5156b7ecb803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 56
        },
        {
            "id": "c8a6c586-d577-4b55-9593-15e829f0d548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 57
        },
        {
            "id": "fa2bbf57-da23-4f52-b49f-bf05756376a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "494e2b1c-9811-4fb3-bb9c-fe2ae73a50ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 71
        },
        {
            "id": "764ddda5-aaba-4f2e-98f8-37c6ec2f0593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "c81d2b51-78cf-46ca-9e56-041954993792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "d18eaea4-60b9-4373-8d4e-8d9f671b6c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 99
        },
        {
            "id": "3d5137ca-2bd0-43c4-83d0-266900d675f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 103
        },
        {
            "id": "d67301ff-44c2-4f6c-85d9-98eb671f0522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 111
        },
        {
            "id": "9b046b37-fb99-4694-9ffc-19264b18da4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 113
        },
        {
            "id": "e86b37a5-e6db-4ee3-9890-71c57e8c87fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "d4b0c024-58bd-4cf1-be9b-4d86f199d831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "7923bc93-ef20-42f0-9cd6-5b9a302a0d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "5efe3c12-6d1c-464d-a911-0ebaf691d9d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "8a4c8f4c-1f2b-401a-ac2f-7362a606ac70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "684d31e7-658b-4adc-bc0b-5ccb46ab2ede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "ec5279b2-a0af-4f46-8c66-e0b0d9da0572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "3efe02f4-bb5c-409f-aead-420294b58519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 231
        },
        {
            "id": "6c4fad9e-a7de-40e0-bd53-ad9d5fc2cf59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 242
        },
        {
            "id": "4967764f-eeae-4db3-b7fa-87aa1d1680fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 243
        },
        {
            "id": "17b247f2-f672-4b2b-b901-0e853bf58931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 244
        },
        {
            "id": "75256fe3-3bbb-4b30-9b68-c3474423f834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 245
        },
        {
            "id": "2a95fb51-7c7e-48d2-9759-a052948edfd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 246
        },
        {
            "id": "b5b2bbf2-75aa-4286-8bb1-c98bd8eae868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 248
        },
        {
            "id": "fa72fbef-e360-468f-a186-8721e27f79e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "8e7b8170-c4a4-4d84-9dcd-e9e59df1562b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 263
        },
        {
            "id": "3af6c86c-bcc7-4730-8cc6-e374197fd0e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "1c26c772-a036-4f2a-a328-506b8b40e5e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 267
        },
        {
            "id": "691a4963-f045-478b-a5f3-321c2f967282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "67f1a2a0-45fe-49d4-8786-8ff378526122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 269
        },
        {
            "id": "4ac57731-a111-4106-8ba1-07dc9ab219c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 287
        },
        {
            "id": "44e8acb8-2981-4774-9dcb-ff91fbe4177e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 288
        },
        {
            "id": "d874367d-f175-4102-a9b4-78d5164ec031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 289
        },
        {
            "id": "e9ff0a9d-c3b4-4c22-8b4e-70bd26bb238f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 290
        },
        {
            "id": "6204509c-4d11-4643-aeb7-2fb546fa9c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 291
        },
        {
            "id": "ed2b32ad-5dd2-4673-8acc-aa6c0d6ebf50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "81891217-5b82-4c3a-9b4f-bc13aed0af4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 333
        },
        {
            "id": "f52b8bcd-216b-4ae0-9819-d46fab0be3b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "e09336d7-f89f-4982-a40c-63849700a55e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 335
        },
        {
            "id": "852dc62b-50b6-4033-b0f5-70d4286bf409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "123f6f41-7a5d-49bc-9571-9d574c8a7502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 337
        },
        {
            "id": "6d517d37-1de2-4108-8908-aab3fcfe4b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "f72ccc32-fb9d-4ac5-b4e3-37a87d08e980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 339
        },
        {
            "id": "643f535e-d5f3-484f-ad68-2ecf2a34c47f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 8211
        },
        {
            "id": "a45ed793-521f-4f14-9d7b-6d8c8c541258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 75,
            "second": 8212
        },
        {
            "id": "abff0754-681b-479d-8bd8-372f44253f65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8218
        },
        {
            "id": "fefa6676-ba98-45a3-8a67-dfd18e5d6916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8222
        },
        {
            "id": "a30bbf99-0a6d-4647-a8e0-5b3f26bbd869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8230
        },
        {
            "id": "6656d299-08fc-466f-9864-129d09226afe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 34
        },
        {
            "id": "4379b4e2-d88c-4aac-b442-4acacbe77a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 39
        },
        {
            "id": "ef02ebaa-8267-42af-bf3e-777067a0250e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 45
        },
        {
            "id": "d68898c9-981f-48e3-b047-65f1e1b31fa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 63
        },
        {
            "id": "22f614e0-b25f-48cc-b80e-fcf9c4dbd12f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "93332c40-b6e5-4754-ad11-b0a764ef3837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "8f279013-98cb-4879-bc6d-a9a8dd7f4bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "6726e847-54fd-43f5-b3ab-f207fb49ddcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 116
        },
        {
            "id": "f5da417b-7f95-42c5-bdea-2ee13cc55e9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 118
        },
        {
            "id": "6af71de5-a9fc-40ca-90cc-90a761ae29a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 121
        },
        {
            "id": "e4580f41-66a8-4341-b7c7-5fcebb5c1153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 354
        },
        {
            "id": "412be9c9-eaf1-42f0-b64e-67f2a015e841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 355
        },
        {
            "id": "07c2185d-a85c-48e6-b803-29dd1334cc10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 356
        },
        {
            "id": "20883762-981c-4230-94d9-5e7a3e643b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 357
        },
        {
            "id": "999bcf46-1160-4d77-b510-ccc92171596e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 358
        },
        {
            "id": "d55073d4-aba4-4577-b11c-4d9894bcf9bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 359
        },
        {
            "id": "262efc60-8dbc-466e-9ac4-3acddd95e296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 538
        },
        {
            "id": "13703324-63c7-48b4-915d-be90cce8fb02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 539
        },
        {
            "id": "0dfac2da-74eb-4a60-a269-40f1d8a79be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8211
        },
        {
            "id": "9d3bc8f5-000e-4e81-b3d6-51d6cadf82b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8212
        },
        {
            "id": "dbb1cef4-8d55-4e04-b6fa-3db74f21d7e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8216
        },
        {
            "id": "ac072f20-3ffe-479e-960e-a1254314f5ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "5e92c215-b716-4981-90f8-f4d756fb408c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8220
        },
        {
            "id": "922bc92c-9c0d-479f-b548-22814451db20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8221
        },
        {
            "id": "e7c056ee-1736-43be-8066-67da70164e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 86
        },
        {
            "id": "6c4b8e81-6fcd-4dc7-933d-63511e248275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 118
        },
        {
            "id": "51af05f6-f4f3-43d0-ba5a-aaba964c45d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 47
        },
        {
            "id": "46746f24-091c-4a3c-b957-a2c72a414f80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 47
        },
        {
            "id": "88da8a0b-72b9-48d0-bad4-26ab6719d2c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 55
        },
        {
            "id": "f9cd391b-6856-4bbc-88b2-75be34e2b6ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 86
        },
        {
            "id": "0e82c761-7a38-4066-8b2d-19f67ae7feb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "851dc9e4-2838-49df-be26-6a57b7cd8572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 118
        },
        {
            "id": "4486d163-ba56-4595-a233-08eab50cf0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 120
        },
        {
            "id": "891c1e5c-5d24-4e07-89cb-673e49ba73d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "85cfc06c-1b51-4b40-8e6c-4f4c1116bc22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "78fa32b7-1193-4df4-87ee-f731a3e50fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 47
        },
        {
            "id": "53234329-dea6-4535-98be-3add0c793c10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 74
        },
        {
            "id": "2b81cf66-4b4e-411e-af7e-2d2987bf7e8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 106
        },
        {
            "id": "a2009ee9-9be0-4664-9118-7d6ac14710cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8218
        },
        {
            "id": "49ac81be-804a-459f-b123-956bdb234541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8222
        },
        {
            "id": "584ccb5a-8f88-45c9-831d-7e3d24da0f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8230
        },
        {
            "id": "6794498d-df95-4a76-b303-8fa8f721e79c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 47
        },
        {
            "id": "eae44f99-d097-4542-9974-23bfac4fc45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 55
        },
        {
            "id": "818ff138-1e6c-4b03-ae4e-b52fd815441f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 86
        },
        {
            "id": "9bcef567-1a44-48ac-969c-c9d9da6f02b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "26e2a234-0e1b-4af0-b27d-756395f9a6d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 118
        },
        {
            "id": "d6e50fab-a291-414e-ba6b-0310c850d8da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 120
        },
        {
            "id": "2869c95a-ee4f-46ac-89a0-161997c5421f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "39d8e9f5-96d6-4066-a317-cea2170073cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 118
        },
        {
            "id": "d3b8686c-3e32-4098-a4ba-d5f81ee0d079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 47
        },
        {
            "id": "65dfe1b6-fe40-4584-9a62-7f28a6b355f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "2697780f-96c2-4bf0-b4d2-72e9b1814f38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "a770760f-e901-42fe-8a26-25ea3a31cd66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "9ed6641f-26d3-43f5-a439-ef5e69ef3afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 47
        },
        {
            "id": "9b148572-6fad-4597-bb34-5dfb39eee7b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 74
        },
        {
            "id": "e5bca470-74c1-4ede-ac23-af084ad2b8e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 86
        },
        {
            "id": "47e508d8-8726-4109-bb2d-063487f32d43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 106
        },
        {
            "id": "a87c55a1-3422-416f-bacc-3b358a611692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 118
        },
        {
            "id": "9164c690-d188-4187-83a3-a391f0d44d7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8211
        },
        {
            "id": "31dab662-c37d-4ffa-9638-2459dcf36ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8212
        },
        {
            "id": "7102b6ce-a258-460f-b104-bf38594129d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8218
        },
        {
            "id": "1383bbc4-071f-4cba-87d4-6c0e30861534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8222
        },
        {
            "id": "3ac26f54-1d1c-4737-82ce-adc26a3cf6c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "89449823-fb72-4e80-96ad-010c50114aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 47
        },
        {
            "id": "4f93066d-451b-40b1-b5a2-3a7609ab03c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "9a698b5f-5eca-4172-bd9e-a184e6a715e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "2c7bfc87-9c99-4541-9442-ed1414c4c2c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 106
        },
        {
            "id": "b8f1e708-7b0f-48cb-b785-6efd69e01c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 120
        },
        {
            "id": "07b980da-2fac-4dd5-aa84-a40a0289cd68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 38
        },
        {
            "id": "a6e28b91-c4f3-4801-8e4a-a65d52382672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "573f4cb8-8d48-44a6-9e8a-70e0170c3c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "ac3a57c8-fbd5-4c38-ac3f-f1c78185983e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "e4b65f6e-4dcf-45b3-8311-a614b7d725be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 47
        },
        {
            "id": "b92010be-c42f-456b-8e2a-3d583ec2fa0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 48
        },
        {
            "id": "2c0603a8-396a-441c-a27b-2fff30b047aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 54
        },
        {
            "id": "1220a30b-2a54-49bc-840a-50f56b1296a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 56
        },
        {
            "id": "1696b381-d046-4cfc-8ca0-8c3ece3e67cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 57
        },
        {
            "id": "fdf633db-18d1-497e-a28f-e7c3b2f05a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "c5a7d53e-d403-4c85-a4c8-87f41c9bbf61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 67
        },
        {
            "id": "837e82b5-78db-4921-8096-c2d6d85d9e89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "5f0c6c0f-f381-4953-892a-e304518556e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 74
        },
        {
            "id": "c4422574-1321-43e8-ace4-48e28940ff29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 77
        },
        {
            "id": "ebc26fc0-9e8b-43fc-b627-0d987e3cb88c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 79
        },
        {
            "id": "f3500b17-aad6-4e4a-a64f-4732cf7b509b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 81
        },
        {
            "id": "89e2f1cb-c902-4bee-992d-50d087949f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 84
        },
        {
            "id": "8f2d57ec-965e-4569-9a6a-c63a54b49a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 86
        },
        {
            "id": "03c05f49-a27f-43bb-b156-64a23d60eb43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "3bfa5a28-6270-4860-9e5b-46dacd2e5aad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 99
        },
        {
            "id": "17824742-867e-40b0-9226-2812fc00652c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "1574ba34-aaea-4786-ba96-b313dc72b41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 106
        },
        {
            "id": "2e4e4192-0e0f-4db7-a8a7-695c0ddd042f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "32866882-c4ef-4ab1-9d40-9ee52d42bdb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "07f2e605-780f-4620-9322-2c4ff574837d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 113
        },
        {
            "id": "20f1a5f2-36f4-414c-a8ce-ba4d75288c87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 116
        },
        {
            "id": "1653e26f-bf2b-499b-843a-17fbd8662a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 118
        },
        {
            "id": "79815f51-e3c8-47e5-8777-e2a4a0e2051b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "6933edee-5ad3-44d3-9dd5-8f277742bd52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "5cca487e-b7ba-4f79-a4a1-561e4fb4ee4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 194
        },
        {
            "id": "48998e33-3da2-4417-8979-aabefc0ab0fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "77af77de-e650-45e1-a164-d928cb24125b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "ae571ed4-9cab-404c-9905-04f10e763ce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 197
        },
        {
            "id": "f8388025-ee7d-4b6b-88a1-147d59d5b7a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 198
        },
        {
            "id": "ad1822c9-8859-4389-8ec3-23090976a953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 199
        },
        {
            "id": "9d5b4ae1-588b-4afb-b0bb-cdf5ce3fa0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 210
        },
        {
            "id": "f53f63e9-5699-409e-8674-748a54eaa536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 211
        },
        {
            "id": "47bc562e-676d-4bee-ae44-5d959bf6e442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 212
        },
        {
            "id": "7eafc55d-aaa4-4968-b689-004dcd6e8fc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 213
        },
        {
            "id": "79952384-5ff0-4cd8-9e10-1c3e35db3c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 214
        },
        {
            "id": "35b09c15-7506-4736-af25-b316ffe502f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 216
        },
        {
            "id": "74f54c29-69c4-47a7-9ace-c3f636f1d376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 224
        },
        {
            "id": "9e0e3ad6-f142-4449-9248-bc587a5ede58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 225
        },
        {
            "id": "46d3d9fc-4b40-4d4c-98ef-80d889f2b290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 226
        },
        {
            "id": "bde2daf8-83c7-45dd-a2d8-593a3c6b20af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 227
        },
        {
            "id": "874e2237-eb2f-4993-9885-f39d8f67b9fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 228
        },
        {
            "id": "40bd73c8-c1bb-47dc-8ae9-40ea4819adf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 230
        },
        {
            "id": "c4a36422-e657-40ab-b880-53d3f43740e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 231
        },
        {
            "id": "a34266d0-59d4-4309-8ffb-c4b96ffdc291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 242
        },
        {
            "id": "b03b3eda-f781-4ba3-b7c5-d4d53ce34581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 243
        },
        {
            "id": "32d54d60-b215-47a0-9d39-9ce446602271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 244
        },
        {
            "id": "860a9fa0-6180-4e48-908b-88d6e4772efa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 245
        },
        {
            "id": "dfdaaa54-e1cb-43ec-b86a-1238d0324ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 246
        },
        {
            "id": "bf2b730a-1071-4d9e-8369-588164345776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 248
        },
        {
            "id": "87d2213c-52ca-4f96-bec6-31bd60343df8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 256
        },
        {
            "id": "a32a9b5b-e3d4-47c6-8d19-ebc41a152fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 257
        },
        {
            "id": "47ac1f43-aded-4f0f-bd58-71eb3326fb6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 258
        },
        {
            "id": "df96839b-7ea5-4731-8bea-61dc5819f68d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 259
        },
        {
            "id": "9c4cdbd7-3990-4557-b726-f2012c3ee720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 260
        },
        {
            "id": "65df6299-9787-4c6d-93f2-8f03466ac369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 261
        },
        {
            "id": "b7b9228a-5cad-43ba-8331-34d6a5c34986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 262
        },
        {
            "id": "7538daa2-6126-4241-ba51-8ad0a7ed133e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 263
        },
        {
            "id": "8c93158e-c2b2-490d-9edb-78161ec6e401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 266
        },
        {
            "id": "8e7a4b10-f63f-4e88-9e2a-a6accdc87691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 267
        },
        {
            "id": "210402b6-6c51-49f3-ae77-ebfe5afad0a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 268
        },
        {
            "id": "ad076192-6bff-4155-b529-898dfe52721b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 269
        },
        {
            "id": "349a8128-bfa8-4d40-9979-f2619e253526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 287
        },
        {
            "id": "b332f888-62ca-4795-ad98-50d2643c8709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 288
        },
        {
            "id": "2d249c1e-b8fc-436d-a23b-68af1c05b469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 289
        },
        {
            "id": "a86254c7-cd7c-4932-99d0-7ce0b9721b15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 290
        },
        {
            "id": "e7654b85-de59-465f-b53f-a230307c5150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 291
        },
        {
            "id": "36f2127b-d5d1-46aa-8e4f-e0beaa8cc894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 332
        },
        {
            "id": "0eae9d9a-3c64-40d4-bb19-d4a459f5012e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 333
        },
        {
            "id": "ab4babbd-7067-4433-85df-ff1c7e3450a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 334
        },
        {
            "id": "6c97e7ca-93fd-4cbd-bbc9-a500d4938d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 335
        },
        {
            "id": "ca1fe0c2-cfff-4c0a-82d7-658672bfc1a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 336
        },
        {
            "id": "4f371037-65fd-44af-9b60-fc520ad0b5e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 337
        },
        {
            "id": "b1d63761-0be0-4f18-9867-283af5262cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 338
        },
        {
            "id": "a92eb938-4746-4214-8058-b7343aef2843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 339
        },
        {
            "id": "8a48b219-278b-41c9-aa6d-f14251bc8040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 354
        },
        {
            "id": "ca4a6068-3982-4df5-8dd1-9c33426fedc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 355
        },
        {
            "id": "6b393fbc-a3ae-402c-9681-e0138345647f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 356
        },
        {
            "id": "1b2ba446-3fa1-4309-933c-c52c74e6f6af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 357
        },
        {
            "id": "49d9fb6f-319f-40d4-bdfc-3c53f60bdf2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 358
        },
        {
            "id": "564fa4db-d646-4458-9fee-877db108b5cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 359
        },
        {
            "id": "a711327d-4037-427e-bfc9-f5208fda89aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 538
        },
        {
            "id": "b522b2b0-0248-44ca-a045-e4e4e5dc856f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 539
        },
        {
            "id": "07d3d912-d1da-4279-b4ea-78a7b979d0dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8211
        },
        {
            "id": "cc5bf745-9ed8-448b-b8d5-efe49acebe97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8212
        },
        {
            "id": "1e0b6bf5-41ba-4869-b8f9-e8430dcb393c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8218
        },
        {
            "id": "19bb7ab5-86e8-4f5e-b285-9c760b6e7093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8222
        },
        {
            "id": "12e21fcd-493a-4763-8e81-0cc6a51c750e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8230
        },
        {
            "id": "5baf2c5e-fe1e-4863-a477-95b87e1afd29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 47
        },
        {
            "id": "987ccc73-8fe5-491d-a646-83b7cbccc010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "b158ad54-e159-46fc-9674-9c4890a9d140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 88
        },
        {
            "id": "29d5a961-4b93-4013-8f98-9bea358c82da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 106
        },
        {
            "id": "538edebd-400f-44de-9f34-0830a9281399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 120
        },
        {
            "id": "357ff754-539a-4b62-b61f-53475b6256d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 38
        },
        {
            "id": "4a501977-2548-4d22-96e0-f420e56f1797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 45
        },
        {
            "id": "1506cfec-713d-40ad-9be3-c5242f47cc44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 48
        },
        {
            "id": "abbecf51-5170-4935-a533-94f83117cc8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 54
        },
        {
            "id": "c7568a10-d157-4a22-b4fb-a0311424675a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 56
        },
        {
            "id": "03b61e39-ad41-4e94-baf7-7c95eb81dc3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 57
        },
        {
            "id": "b6373c5a-63a4-45cd-acc0-5ba25a1eb061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "5f152d7f-62a5-40e8-9358-f40a43bee807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "6f6e12d6-4227-4877-8a59-b9bb37d30683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "b2649f53-2192-4779-8674-e5c674ffadbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "a63e965c-86f3-480a-9606-7bdd0e1ff0e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 85
        },
        {
            "id": "b0040fea-319d-46d3-a197-6794b20eeb5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 87
        },
        {
            "id": "312da97c-8651-46dc-af58-3afa8220e7c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 99
        },
        {
            "id": "bdbf2165-a154-4275-9e37-66ffa6e8844c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 103
        },
        {
            "id": "67ffaeb7-8811-4160-8f6e-443364bccd58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 111
        },
        {
            "id": "4a29f9d7-4bdd-43a7-9538-ad2b056e7ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 113
        },
        {
            "id": "2be47505-fd78-464b-8c19-792e0586152e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 117
        },
        {
            "id": "c87b1e45-1abd-423c-9a16-882f32c3e861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 199
        },
        {
            "id": "58518862-60ff-4890-876e-1ba76fc4a5ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 210
        },
        {
            "id": "f0907b46-ca83-447c-9d5b-82d517d2489a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 211
        },
        {
            "id": "e8282371-07de-4ba2-bb9b-e640a14183ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 212
        },
        {
            "id": "6080b9e6-0e66-474a-86e2-bff7ce50dc53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 213
        },
        {
            "id": "d216de8c-0536-4a63-a223-70637556e86f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 214
        },
        {
            "id": "a5d19a64-7a56-40e8-b7d1-beca7b2c85ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 216
        },
        {
            "id": "538616cd-8832-477e-881b-ee454e0e55f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 217
        },
        {
            "id": "34816d39-6ddf-46a3-9f8d-d8fc87209b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 218
        },
        {
            "id": "953945a9-67ee-4634-a5db-a7c87c1fc422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 219
        },
        {
            "id": "dc7f1672-9c2a-4440-9014-5899a24d14d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 220
        },
        {
            "id": "792755d6-a93d-439e-a331-b3a8eec210aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 231
        },
        {
            "id": "68c84041-7879-4e1e-839a-331a6b5f17f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 242
        },
        {
            "id": "e9f369f4-f2ac-4352-b942-add7c39b1841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 243
        },
        {
            "id": "57edfe79-236b-4d4f-a8ce-b48345bc9056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 244
        },
        {
            "id": "5b00c1ef-b6af-4d1b-ad41-b9661ec917c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 245
        },
        {
            "id": "eceee15a-f6fa-4ecb-9880-54725d3e1b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 246
        },
        {
            "id": "dfe1f7bd-c3d4-4dae-969d-ba27dbe342fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 248
        },
        {
            "id": "de67424a-bab6-40d9-b547-2af0aad3a046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 249
        },
        {
            "id": "cfb5dd96-a21e-470a-be33-bbfb3eab7cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 250
        },
        {
            "id": "56244a48-f6a4-402f-bd6f-b94c4b0362cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 251
        },
        {
            "id": "b0f066c7-7da2-41a6-b5f3-2fe4529b12e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 252
        },
        {
            "id": "b86dfa9e-3bf2-49e7-a8de-aca447b2da12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 262
        },
        {
            "id": "3bc87d90-8b96-4c31-9eae-dd7670126226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 263
        },
        {
            "id": "383be02f-3e67-4fa1-961d-a17cd8f85551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 266
        },
        {
            "id": "66c4003d-50b0-4b0e-af82-56d33e1fdb03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 267
        },
        {
            "id": "ba865e18-b543-4829-a912-ecd60b8d7748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 268
        },
        {
            "id": "5c2e951e-8c11-4ecb-8d43-0709dab351d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 269
        },
        {
            "id": "b95c7f4c-3717-485e-915b-6eab7ba8962f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 287
        },
        {
            "id": "f4467e75-65d2-41d4-b43c-2eb957924e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 288
        },
        {
            "id": "b9604c45-7e27-4cb5-8150-de3f9176f2e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 289
        },
        {
            "id": "9531a729-f3eb-403d-9294-4e79f7a312cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 290
        },
        {
            "id": "2dfd913d-e575-4e1e-a6c6-3149c1c2e223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 291
        },
        {
            "id": "3d544d00-71e9-4707-ac35-1fd36426a250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 332
        },
        {
            "id": "45f29724-622f-41be-ab7f-6335a6379c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 333
        },
        {
            "id": "2d94287e-8b42-4c43-92e3-53e80ade555e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 334
        },
        {
            "id": "3e974055-0cb9-437b-9ad8-5bfd48703f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 335
        },
        {
            "id": "f9bf804a-a856-45dd-a267-75963918b525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 336
        },
        {
            "id": "e7d1645d-0c2c-4b1f-b862-53946b03e354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 337
        },
        {
            "id": "b74ad195-7196-4176-92b4-42e751f154c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 338
        },
        {
            "id": "fc17a4d5-17bf-4081-8f05-c2426d050ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 339
        },
        {
            "id": "ce432496-67f0-4b65-a0a8-a33b211b4796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 360
        },
        {
            "id": "44986eaf-f093-496d-a09c-83f2d2b71421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 361
        },
        {
            "id": "0ea03193-d2ad-4714-9358-5f8b6664f948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 362
        },
        {
            "id": "6b8fb973-a8ab-4177-ba6e-59abb966799e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 363
        },
        {
            "id": "4a7a2ecf-0110-4f21-8be9-99eb6015f755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 364
        },
        {
            "id": "156ffcc8-5fed-4bc0-a6fc-5b9e9e9c856a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 365
        },
        {
            "id": "9f946eb5-26d1-450c-bb27-844225fb00ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 366
        },
        {
            "id": "934658f6-1851-4660-a938-c09bdb12eec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 367
        },
        {
            "id": "ea07eb53-d184-4921-bb2e-a1a70ea769fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 368
        },
        {
            "id": "aefa7ad6-422d-47ad-99b4-2fe8d331b146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 369
        },
        {
            "id": "2b547cca-378a-4b9a-bd0d-fc2c0d81af48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 370
        },
        {
            "id": "f4317409-c348-47e2-a521-74f384ed0048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 371
        },
        {
            "id": "2260bba3-6655-4ed3-a29b-71afbaf345d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 372
        },
        {
            "id": "dabb5581-4926-4457-a0bb-ada5359b0acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 373
        },
        {
            "id": "5ad8000b-d8bb-40d0-983f-0f5e2ad92f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7808
        },
        {
            "id": "06b83a81-7858-4690-b2f4-6b52aef91346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7809
        },
        {
            "id": "d4d03ed9-567c-4bd1-8317-82daea103139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7810
        },
        {
            "id": "5755f0ba-089b-4f72-baf2-45b07f2f6858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7811
        },
        {
            "id": "395f9063-4d37-4ff5-ba64-c53e31e4959b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7812
        },
        {
            "id": "6f494298-2792-4314-b001-5db9ec6640bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7813
        },
        {
            "id": "a875a398-02e5-44aa-a81a-8de547ad2afe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 8211
        },
        {
            "id": "b0522fc1-bc5b-48ba-b093-7017efed8b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 8212
        },
        {
            "id": "54adaeb1-f482-407f-b7e3-31ad4ed0a32c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 47
        },
        {
            "id": "c4cfa86d-b78a-422e-9bb7-8784d73808fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "3c41a270-4c64-42a7-b4dc-0dd314b74cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 88
        },
        {
            "id": "789790f5-3f1d-45af-8100-008ce5a06120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "fc5fcba7-5cec-44d0-a4e8-cbe0d27397f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 120
        },
        {
            "id": "c532f569-9767-4dec-b413-35fea53cd4a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 45
        },
        {
            "id": "a6e2e58b-b6ab-48bd-bf89-407d410681e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 89
        },
        {
            "id": "6f295804-e647-47cd-aa79-510597b8f8dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "169e16e6-bcfa-412c-97de-e39e15999c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 8211
        },
        {
            "id": "1813f563-0213-4aca-93ab-0303108fa18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 8212
        },
        {
            "id": "91f2fe14-6776-4995-913c-444a2214bae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 86
        },
        {
            "id": "a376b32d-feb4-45a3-9a5e-6d86940073ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 118
        },
        {
            "id": "901a1ad5-d654-4075-8545-02c9b1280344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 47
        },
        {
            "id": "c4ecdcd1-6189-4371-a3a8-424302565fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 86
        },
        {
            "id": "abb3396c-aefb-4b8b-a667-cc1e93058497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 88
        },
        {
            "id": "64188e48-cb98-48cc-9e61-e997e9f3ee0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "ebf4b765-037f-4b8c-8224-b3a91e35c8d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "94a995f0-7450-429c-b4cd-d6c0245b523a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 45
        },
        {
            "id": "5f537d69-3f60-4518-987c-028cb4bf327c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 8211
        },
        {
            "id": "9c174ad5-ef5d-4e74-99e6-ce88c48616d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 8212
        },
        {
            "id": "946dc500-27d3-4604-b665-8dd04ac9b37f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 47
        },
        {
            "id": "327d8b0f-4d0f-4050-829d-0e63f6ece574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 86
        },
        {
            "id": "aa80aea9-4ac8-4535-ad4a-4059af3e9fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 88
        },
        {
            "id": "f6f7defd-e74a-4f12-aa3d-c0a7338ae6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 118
        },
        {
            "id": "ba0eaa5a-b0ec-4dc7-813c-f6169fe002cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 120
        },
        {
            "id": "53cc7bfc-5044-4e84-8481-5d9eb83799c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 45
        },
        {
            "id": "f2b56fdb-de4a-4190-875d-e87921af56bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 8211
        },
        {
            "id": "28b99d70-7c63-4819-a7b6-14d19c1e98d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 8212
        },
        {
            "id": "5cb9ec1a-ad1d-45b8-a14e-bd5d5b4ba372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 44
        },
        {
            "id": "51d965df-25d7-4a44-b106-da974c9fc600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 45
        },
        {
            "id": "87211f3d-6ea3-48be-8f06-37c4244e9708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 46
        },
        {
            "id": "f3cc2818-30a5-4684-9b91-ab62fc7b8d2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 47
        },
        {
            "id": "caea7d7e-c0c7-4bef-8b41-e127b25026c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 74
        },
        {
            "id": "e18104c6-6c8e-42e3-bc0c-80b72cf847b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 106
        },
        {
            "id": "047e39f2-4f6e-44e5-a1df-eb3311c99f4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8211
        },
        {
            "id": "3099e953-3100-4466-af45-d9543ad7ffa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8212
        },
        {
            "id": "939e9a43-48b7-453c-bbc6-f2605a5527c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 8218
        },
        {
            "id": "adb0eae2-5ab7-4e19-bbaf-74f0efde90ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 8222
        },
        {
            "id": "dcab03cc-ae76-41f3-8ad2-74215579ad9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 8230
        },
        {
            "id": "e018b262-7833-4b66-80ad-c3d2b4cbf476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 44
        },
        {
            "id": "f0417c4b-4353-4509-9383-ecc681835dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 46
        },
        {
            "id": "9ad2b1b4-2062-4ff3-8b24-8273ac30039a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 106,
            "second": 47
        },
        {
            "id": "016b160f-3cb7-457c-bfaf-eda37f811712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 74
        },
        {
            "id": "258eaf7b-858e-4417-8b56-2a82ee3c6b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 88
        },
        {
            "id": "9b1dfbbb-506b-4711-a1bf-5aa441fd99b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 106
        },
        {
            "id": "c1f42b3a-e2b5-4edd-846a-858f910542ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 120
        },
        {
            "id": "463a11d9-adbe-43e0-ae5a-292483548068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 8218
        },
        {
            "id": "88fc3e21-b82f-4244-9f2e-021918a0e955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 8222
        },
        {
            "id": "05680238-e41e-4ea8-9207-8a83ed31cf74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 8230
        },
        {
            "id": "2b687734-a591-42af-8312-851b607ab5e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 38
        },
        {
            "id": "6367e866-3eb7-4025-a981-0669cfad6332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 44
        },
        {
            "id": "1fa58ae5-dcc2-49e0-a26d-8fd75099fa17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 45
        },
        {
            "id": "ad2c4b3b-6fb0-40f5-91d2-6566e3a18601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 46
        },
        {
            "id": "2aa4abd9-b4a5-43c6-bfac-7f021538614b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 48
        },
        {
            "id": "d2517a77-7ca2-41f1-863a-d37e5c0e8242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 54
        },
        {
            "id": "f1cbaec0-a07a-42c1-8443-4d4fddecadd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 56
        },
        {
            "id": "93ae20b1-532e-42fa-9586-8aa55480324a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 57
        },
        {
            "id": "7ba90ac0-3837-4182-8bb6-8bc88e7dee26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 67
        },
        {
            "id": "ecf3217e-31d4-4d1d-a804-dc6b8fb6a294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 71
        },
        {
            "id": "0fb83f07-7c52-4e8d-8479-c95f73434f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 79
        },
        {
            "id": "52000009-9ff8-4261-8f63-340ececcffb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 81
        },
        {
            "id": "75a82fd7-2df4-484d-ad95-eac1108798cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 99
        },
        {
            "id": "a2e989b1-26c1-43e8-9ece-582167216817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 103
        },
        {
            "id": "e484ae33-4ed7-4a5d-82e1-ec79c1494b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "5e3af919-3512-4e9a-8220-f616ec26f9db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "8764a720-51d8-43eb-a78f-4ae8b63e0e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 199
        },
        {
            "id": "903d4f85-324b-4c94-b525-955d533dc1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 210
        },
        {
            "id": "3e138eeb-aada-4ac4-bc2d-aae2815bc570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 211
        },
        {
            "id": "64c1f785-45d5-4218-8017-9001c893dca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 212
        },
        {
            "id": "f8a97225-e162-4119-8b60-08f505611d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 213
        },
        {
            "id": "6cfae081-15fb-4107-833e-7897f1cd5f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 214
        },
        {
            "id": "5a9d6ae4-69d3-4b5d-b37d-444949f4cfe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 216
        },
        {
            "id": "b2dbe9ad-23cf-48ae-8d03-46ff4787f529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 231
        },
        {
            "id": "c2d7c131-da00-4a40-8af6-14bd27a64424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 242
        },
        {
            "id": "05eb8beb-ca73-4ea0-ba76-b9bbd1f4320a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 243
        },
        {
            "id": "f1c7ece8-bf94-4deb-afbd-4f2af8bf03a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 244
        },
        {
            "id": "22d89ec0-beec-452b-8a1a-d7dba7899a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 245
        },
        {
            "id": "3b431fce-d3cd-48fe-915e-c77889c93fd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 246
        },
        {
            "id": "27bf83a0-7d13-4f3f-9e00-701620dec2f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 248
        },
        {
            "id": "c9f9865d-9512-4313-b551-8e3f6ede3241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 262
        },
        {
            "id": "7f7cbadd-54a0-486c-b4cb-85f137664d07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 263
        },
        {
            "id": "c23c2fb1-c036-48e8-94bc-641b93caa7df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 266
        },
        {
            "id": "1f597072-dec8-4b3b-b273-1013722d0f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 267
        },
        {
            "id": "9ac3234d-dc31-4c68-8907-5da1347f2ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 268
        },
        {
            "id": "391dd758-e221-453a-ae8b-bfe13e7dc13c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 269
        },
        {
            "id": "a5929bc2-6bec-4e36-ac4a-240e059a749a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 287
        },
        {
            "id": "7bea2278-9402-42e2-aee3-8226d89be452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 288
        },
        {
            "id": "ef956fdf-878f-4d64-9175-549b6c8f9e52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 289
        },
        {
            "id": "1821cc72-3cbc-4d12-96c1-97c6603fdd98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 290
        },
        {
            "id": "414f9bdd-4df0-4233-a07b-b1fda3012c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 291
        },
        {
            "id": "6e38d5dd-96f2-4547-9488-357893ed25ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 332
        },
        {
            "id": "b54e2ab2-6f22-4926-8627-7cf56631a65d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 333
        },
        {
            "id": "1a464e6b-6347-4ea1-8d96-03bb729a2133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 334
        },
        {
            "id": "ab9a18d1-71aa-4882-b54e-c4213f6259c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 335
        },
        {
            "id": "c77bf3e1-5d74-4835-8a69-ff078cdf2e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 336
        },
        {
            "id": "be695bfc-a55e-4dcb-b4e1-c4790b77167f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 337
        },
        {
            "id": "b16fef3c-be68-4b0b-9478-404f076f93cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 338
        },
        {
            "id": "323bc4d4-0df5-41c2-a45c-860d81c8a625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 339
        },
        {
            "id": "1215bf29-eeca-4f62-aa6a-bd0532742cb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 8211
        },
        {
            "id": "b5633eb0-801f-49be-92bb-e43c9dc412c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 107,
            "second": 8212
        },
        {
            "id": "f1f32716-c2c2-41cb-8506-97190837cd92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8218
        },
        {
            "id": "2a5ee158-213c-437d-8650-2bf656ce5ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8222
        },
        {
            "id": "f02647da-1867-4d08-a815-cc18b9031d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8230
        },
        {
            "id": "1752adaf-eba1-4d93-bc71-a2ce148a938a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 34
        },
        {
            "id": "8aad5514-be3e-4379-9529-9b55bc9d948c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 39
        },
        {
            "id": "8a403418-0a2a-410c-a122-c2b1fdbe1336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 45
        },
        {
            "id": "4f2d38e2-0a0a-43f8-bc70-ee3bebfadba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 63
        },
        {
            "id": "b72edecc-c1db-42ce-9580-7333e4eeedcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 84
        },
        {
            "id": "42fa9770-41fd-4fe2-8725-8bb155800138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 86
        },
        {
            "id": "1c914584-f8eb-4a4c-9b51-ff153a36f2b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 89
        },
        {
            "id": "4c11fdf3-4bd1-4a2a-b238-abb0ae757caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 116
        },
        {
            "id": "d7b4f9db-3fbb-4dfa-8ec6-8e667349893f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 118
        },
        {
            "id": "6944c3b8-610f-4d35-a19a-eff2d3882e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 121
        },
        {
            "id": "ec5c6e30-0030-49f0-9eaa-7386119af7a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 354
        },
        {
            "id": "c38d5d1d-6119-4714-aff8-762cdba46283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 355
        },
        {
            "id": "76e7232a-b41e-4f16-b5e9-22d330144465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 356
        },
        {
            "id": "584575ad-8c80-4a1a-b443-cac08415b8e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 357
        },
        {
            "id": "d99c50a8-1a0d-4c24-b72a-84c49ab43778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 358
        },
        {
            "id": "aa0db513-5160-4a2f-b127-fa59e12cbb9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 359
        },
        {
            "id": "d30841b2-7fe1-4d01-9ccd-291b99a9ad94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 538
        },
        {
            "id": "19abe649-f052-461f-bcde-c5f554bd1150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 539
        },
        {
            "id": "b71bf38f-e1cc-4bd0-b506-587aeee3361e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 8211
        },
        {
            "id": "6f365727-90fc-436d-a8e5-f072b318a0dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 108,
            "second": 8212
        },
        {
            "id": "e2318bac-8d04-4760-b1fe-a2efdf116625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 8216
        },
        {
            "id": "3e11e57e-dbcf-4b79-80b6-02a29f83f0a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 8217
        },
        {
            "id": "ed44df2c-a820-46a3-aca4-d4b0cdc0256e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 8220
        },
        {
            "id": "691bb037-41c9-4b5f-871d-00bdc6345f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 8221
        },
        {
            "id": "a4cd9bd7-50dd-4f01-b517-176b6b6401e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 86
        },
        {
            "id": "9eb4222b-c668-4879-9675-929895920578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 118
        },
        {
            "id": "5911bc40-d231-49a2-90a9-4e7f4afc32b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 47
        },
        {
            "id": "dda33b08-0ac9-42c7-a6ac-befa1c2c4bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 47
        },
        {
            "id": "c510d92e-2086-4172-80dc-bbab64140881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 55
        },
        {
            "id": "fde20794-64f1-4267-9d68-79ed27d4deb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 86
        },
        {
            "id": "e2c8613b-7074-4c21-97af-9ac3d49be702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 88
        },
        {
            "id": "99ffe007-90f7-4333-81ed-72246058ddae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 118
        },
        {
            "id": "6d229b95-bb4d-440d-b658-c2fe3a1de044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "8cade83f-9ee8-43cc-8735-86cc847b1d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 44
        },
        {
            "id": "cc531a2c-0ea4-4269-a7fb-e3d3e15d62e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 46
        },
        {
            "id": "d595feb8-28ee-4ca3-b59e-e1487fae34ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 47
        },
        {
            "id": "d17acaed-caf5-4d1a-9b8d-6086a133ef49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 112,
            "second": 74
        },
        {
            "id": "4dc0779d-ab47-4eaa-80a6-046d2c0a2bdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 112,
            "second": 106
        },
        {
            "id": "d891e0f3-5731-435e-a399-e31dd59fe784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 8218
        },
        {
            "id": "4fffe5ca-d413-41ae-997b-fac751440994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 8222
        },
        {
            "id": "e550445e-2fcc-4428-a5e4-282d118119b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 8230
        },
        {
            "id": "db0ae30e-41db-4ff8-9d83-b5b0a739348d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 47
        },
        {
            "id": "a6272571-ab28-43df-b59e-258b20d68603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 55
        },
        {
            "id": "9bcce5d6-62a0-4851-b377-462dae35ee5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 86
        },
        {
            "id": "f0ebc9c8-96ce-4e9f-b6d8-32de25f5716f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 88
        },
        {
            "id": "a8cc7878-f6d2-4728-93c0-4429bfa506bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 118
        },
        {
            "id": "3c8b9f03-04a0-446c-b1c7-311ac3e188b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 120
        },
        {
            "id": "bcd38719-70be-430a-b63b-37f379f2fc7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 86
        },
        {
            "id": "42f43af6-12ea-4a24-9367-03277f1eda0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 118
        },
        {
            "id": "5cf2cf61-418a-48ea-9dc0-ce06b13a1a70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 47
        },
        {
            "id": "d71c52e9-65a5-4694-92b5-2b69b4aec372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 44
        },
        {
            "id": "92dec2d4-b600-41de-b1b8-4bf14a7c6b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 45
        },
        {
            "id": "700c04a6-f07e-45c3-9a5d-e35ef0310c7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 46
        },
        {
            "id": "46d93a68-94bc-41e5-bf2a-6fcb0611eaa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 47
        },
        {
            "id": "b59cd1ef-349c-4106-adef-85234847a695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 74
        },
        {
            "id": "c592bce8-7354-4a4c-9d56-2803534f6d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 86
        },
        {
            "id": "552205d3-68ec-4a63-8bc0-8c68a73e4db7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 116,
            "second": 106
        },
        {
            "id": "cb3e0596-ad65-4101-9e4a-ef6d0d80aedc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 118
        },
        {
            "id": "b0de31c7-7ce7-466e-98ed-7b5cacaa3692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 8211
        },
        {
            "id": "46e476be-0e28-4a1b-b0a4-519c32867715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 8212
        },
        {
            "id": "d41e3391-bf7e-4cc6-bdb2-4ad8b98292aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 8218
        },
        {
            "id": "022b5722-cca6-4db8-9ed1-cce58083ab90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 8222
        },
        {
            "id": "70ec8656-a616-4b74-abf8-e5262f7add8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 8230
        },
        {
            "id": "970bd363-d95b-4d4a-93a2-72d4f8ad30a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 117,
            "second": 47
        },
        {
            "id": "663b13f4-61cf-4a18-a0ce-a8066eeab34e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 74
        },
        {
            "id": "53291e24-928f-42a1-bc87-88ee60390891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 88
        },
        {
            "id": "39a33dff-ffc5-4a92-a9df-c59063a4a2d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 106
        },
        {
            "id": "011db42d-fe26-4363-8cf0-3459e8820329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 120
        },
        {
            "id": "cd82fd02-5883-4de4-bb2a-12ed3d0820c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 38
        },
        {
            "id": "afa07ce6-6e35-4d17-a9cb-2fa619dfc34e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "c0b9da5d-e422-4eb9-8c7f-43e84251c55c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 45
        },
        {
            "id": "719a1c05-2704-46f0-86b2-afb299211b62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "e81266ca-9d75-404f-a292-c651d9f6fa1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 47
        },
        {
            "id": "9f796fd4-324e-406f-8907-bd9f93ef974e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 48
        },
        {
            "id": "17c3c584-c0c7-4620-97a7-b390f81fbc95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 54
        },
        {
            "id": "31f9109c-ee08-4312-a718-d272b75f4cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 56
        },
        {
            "id": "27657977-45bf-46e8-9a17-cf32159cb5d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 57
        },
        {
            "id": "584cadeb-f6c4-4b34-ada5-f8718e2074b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 65
        },
        {
            "id": "d411c3ff-d736-4624-a6d9-55b67a4bf12e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 67
        },
        {
            "id": "71c92371-cc88-45b1-ad8e-46a4942ef5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 71
        },
        {
            "id": "a4744652-500b-49f3-8c3c-89d740766133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 74
        },
        {
            "id": "cb8d446f-b351-4b2e-a520-7b3b96944318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 77
        },
        {
            "id": "697b96b4-18d5-4575-8b14-9d34198e1f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 79
        },
        {
            "id": "06793d3a-f879-4bc5-97ee-ad971838ae6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 81
        },
        {
            "id": "c93aea4d-ecd3-4094-be44-4082d6250837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 84
        },
        {
            "id": "5074188c-120a-42b1-938b-432e665fb1ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 86
        },
        {
            "id": "c1a093f4-aa70-4bbf-8271-bb891cd25c7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 97
        },
        {
            "id": "770b0f28-ba48-4856-8270-788d1fa28d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 99
        },
        {
            "id": "588e328b-b13d-4733-a030-e6cd79f3a8b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 103
        },
        {
            "id": "2418aeca-ec67-41b8-8e96-ad2bf46a5241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 106
        },
        {
            "id": "6a9fafe0-8da9-477e-8872-fb714283bf6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 109
        },
        {
            "id": "702bbcd2-846d-4f15-8113-34b939709da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 111
        },
        {
            "id": "0eccf6f0-5411-4d90-9a3f-a367eb97e07c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 113
        },
        {
            "id": "61f59f3f-e098-499e-ab00-afa1dcc3171d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 116
        },
        {
            "id": "22334631-a736-40ab-8c36-5ef0b928e53d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 118
        },
        {
            "id": "ee5621ee-9e80-4af5-b92b-4f0dafb49ce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 192
        },
        {
            "id": "8e1814e5-325f-4ba3-91ff-b553b601bb42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 193
        },
        {
            "id": "86029e5a-0d27-4b95-a3e4-873ebb687f77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 194
        },
        {
            "id": "e6c2d199-3aa0-4cdb-b49c-5673213b34e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 195
        },
        {
            "id": "7ad2dce1-1391-4a58-9bf7-01e715e5d3b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 196
        },
        {
            "id": "e3a32b88-4028-429d-8de8-59724c47fb1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 197
        },
        {
            "id": "8efc405a-e5ee-4790-9398-a556a9821e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 198
        },
        {
            "id": "f675d6cf-2fc4-4ebc-b47e-9e3ecf31f4d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 199
        },
        {
            "id": "0b726b1d-e966-4e41-95c4-208b1ef7e65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 210
        },
        {
            "id": "1e2def60-ebf6-45bc-93c9-f1c77308a451",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 211
        },
        {
            "id": "cd5571a2-90b9-419f-ad70-9cfc64419ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 212
        },
        {
            "id": "e7a7e427-a49c-4e3f-a0e3-8f8b955b84dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 213
        },
        {
            "id": "8b352dc1-d3bb-4919-a60d-97314d61811c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 214
        },
        {
            "id": "1166b540-8494-44aa-8ad3-d9fe4ab5b411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 216
        },
        {
            "id": "8691b783-89f3-44f8-8ea0-5a6b22673e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 224
        },
        {
            "id": "dd88ba05-d600-420f-8e7f-2d34940c1ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 225
        },
        {
            "id": "8d183c14-1186-4056-9d6d-a76e7580a180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 226
        },
        {
            "id": "e7301b0e-e0a2-45a9-b840-81493a0f1095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 227
        },
        {
            "id": "9ea166a1-4a87-4e46-a207-76cd91f06b1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 228
        },
        {
            "id": "fc6c6cde-cd35-48b1-8f15-ee4708b0dcb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 230
        },
        {
            "id": "8304fc7e-d358-4b7d-98d8-2fbf4ed1ad2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 231
        },
        {
            "id": "5f54a153-ccae-4962-b1da-72ce2809fc87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 242
        },
        {
            "id": "009ffeee-6050-4190-af77-3109144f4e9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 243
        },
        {
            "id": "8bd5e044-7534-474c-84b9-593f8f17a8d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 244
        },
        {
            "id": "bd6ed66a-2616-4d03-9411-60be2906fd0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 245
        },
        {
            "id": "2d2e5996-d85e-4f72-8ce6-19af80947814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 246
        },
        {
            "id": "94e066eb-8d22-4b69-a920-780e18608557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 248
        },
        {
            "id": "00857c47-4c8d-4674-a4aa-4424c597efbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 256
        },
        {
            "id": "d1c9b2c0-defd-48c7-8dea-fd14de621d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 257
        },
        {
            "id": "953024db-1ae5-494c-806e-9a9216cd8658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 258
        },
        {
            "id": "cdf7c77b-926f-4726-b57c-869e951d7d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 259
        },
        {
            "id": "41e27625-2d72-4478-9ef5-d7537377b442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 260
        },
        {
            "id": "0073f4e9-fdcf-4a90-b167-2848dd6ae4bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 261
        },
        {
            "id": "c800a342-120b-4798-9f72-9f98e2d73967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 262
        },
        {
            "id": "8dfc6586-5ba6-4023-babc-2fb463addee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 263
        },
        {
            "id": "d0ce8812-c8f8-42c3-925d-a9fb8a8059f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 266
        },
        {
            "id": "4db67ea3-032a-4d43-af5c-4dd4874f5d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 267
        },
        {
            "id": "8fe5c631-1866-45f5-8d83-706752a19cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 268
        },
        {
            "id": "d82619c6-605c-4121-92d9-f531e0187fdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 269
        },
        {
            "id": "8946d9ef-045d-4ec7-ba31-20a816564f73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 287
        },
        {
            "id": "94aa6dcf-3443-4979-9451-dc773305efd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 288
        },
        {
            "id": "655550b7-17aa-445e-afae-9c7b53e6da0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 289
        },
        {
            "id": "73a2422a-1d5c-472a-a8e3-0c3e41d60f98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 290
        },
        {
            "id": "46d64091-521a-4276-80c3-a71fc99b3cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 291
        },
        {
            "id": "b850a95e-65df-41d6-91d5-01f592a8fdea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 332
        },
        {
            "id": "540d52d5-2d03-4879-80d6-89ecb67708b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 333
        },
        {
            "id": "25ff688f-b97d-4685-95d3-2f262ed54ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 334
        },
        {
            "id": "a851c7b6-3904-4ef7-b363-7e1c64e96213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 335
        },
        {
            "id": "efe22fde-e22d-46ea-bd35-bcdaa7f00665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 336
        },
        {
            "id": "337e6a10-d5c4-4b8e-ba6d-e15fbbd48f2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 337
        },
        {
            "id": "1899af33-9df8-4379-9dec-d19a40ada3f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 338
        },
        {
            "id": "57c022a2-3901-4fc6-9c52-27693f9831ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 339
        },
        {
            "id": "cca2dba8-0bb6-46a0-9c10-492093c03175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 354
        },
        {
            "id": "2c5496c7-deea-4a36-810a-d7d26a063d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 355
        },
        {
            "id": "893af2f7-78d3-445a-9adb-aaa648ea6236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 356
        },
        {
            "id": "99acd2c8-3663-47fc-a592-8185cc36c1ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 357
        },
        {
            "id": "5ba79b1f-61d8-4164-8fa8-9a3c00d2080f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 358
        },
        {
            "id": "df8b334e-b59c-435b-84eb-bcd7b2f8fd13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 359
        },
        {
            "id": "c4327c6b-4ed9-4707-b564-3c23c6119b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 538
        },
        {
            "id": "7792740a-bde4-4b4a-a705-a68b34ba13b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 539
        },
        {
            "id": "01f045ca-cacf-4577-9e85-680a2a1511ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8211
        },
        {
            "id": "a7cacacc-b81b-4a68-9658-63d7b85790cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8212
        },
        {
            "id": "9e2a8735-0d2d-427e-af2b-122172940f53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 8218
        },
        {
            "id": "f9e8b501-b8a9-453c-b94a-1bd1d6c0d1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 8222
        },
        {
            "id": "c7e430af-8308-485f-b540-b60ceae20d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 8230
        },
        {
            "id": "c9a5859e-6980-4b52-b638-3081e762443c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 38
        },
        {
            "id": "e3b444df-79aa-4124-af95-16d85c651cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 45
        },
        {
            "id": "9726d566-8a4c-41fd-8a7c-6c15c913cb41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 48
        },
        {
            "id": "8ff655d9-6397-4468-b7ac-e690e8828897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 54
        },
        {
            "id": "d801e721-8b7a-4e42-b62b-0f30629c558a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 56
        },
        {
            "id": "a3f64182-db05-4476-89df-7478518b42d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 57
        },
        {
            "id": "0fa04060-b119-4323-ac7a-a8e54fb86ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 67
        },
        {
            "id": "0ed8d5a0-61f4-496f-8358-a9da4104ff80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 71
        },
        {
            "id": "89c1aee3-7319-4a5e-80f3-f11dd5d5af1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 79
        },
        {
            "id": "7bb0a8be-da91-4528-8077-fd408b5d8683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 81
        },
        {
            "id": "14439e0c-a9f2-47da-bae4-b7cd0903218a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 85
        },
        {
            "id": "88a9a7eb-9707-448d-8625-f44badbbc9a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 87
        },
        {
            "id": "0b56eb0a-31a3-40f5-bc1c-d2e323d3eea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "fead5950-bb3c-457b-8e54-58ac24446ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 103
        },
        {
            "id": "ec73f0c5-074b-4de8-85d2-f74d68a8d0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "ab43cdd3-550e-47e4-bc18-1bcf88df7799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "a83cdb56-0c28-47ef-8ee8-69b0b3b499ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 117
        },
        {
            "id": "6841fcac-6019-4a73-9d69-d9b6f64860d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 199
        },
        {
            "id": "ed2adc77-bdfa-4476-a735-10bc432839c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 210
        },
        {
            "id": "73182715-36da-4164-84c3-39641687d284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 211
        },
        {
            "id": "4fcb8971-2dd1-414f-89ab-9544c2716810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 212
        },
        {
            "id": "3327ae09-bf43-473f-9834-24a1f0d7d749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 213
        },
        {
            "id": "0fdf1ba3-cc31-4a4f-9fab-4b0617ea3e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 214
        },
        {
            "id": "3eca60f3-6eb1-47e9-9354-6363fe1108ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 216
        },
        {
            "id": "7a56cb02-7838-429b-ab5a-189095d525d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 217
        },
        {
            "id": "c84c414c-e011-4ac1-8aa2-9c023c6d7c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 218
        },
        {
            "id": "f4abb914-30ff-442d-8f7b-db833b3800cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 219
        },
        {
            "id": "1ab11452-4149-439c-8921-a6a32f596146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 220
        },
        {
            "id": "d5a6f0f0-8a97-4a0d-a212-9180b74eccad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 231
        },
        {
            "id": "88132777-cbbf-425b-94a6-b35a0e1f97df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 242
        },
        {
            "id": "c4e0cfb1-564f-4cd8-85ac-db8da89d36ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 243
        },
        {
            "id": "a4d3c981-fda0-4d05-9a6c-42a8f6ff2c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 244
        },
        {
            "id": "c533ced1-83e3-44b0-b4eb-f268a51bd007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 245
        },
        {
            "id": "cc2dedde-0c36-4e6d-97a4-d05fa7e9c8f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "a0d88d14-eabf-4c61-8087-2dab7cfe1bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 248
        },
        {
            "id": "2bd567fa-b00a-462f-932e-48c0435bf0b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 249
        },
        {
            "id": "015b72f3-0414-406e-beb9-a752ba2e961b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 250
        },
        {
            "id": "ff2f71f9-a168-4622-86f4-fffa7327fb78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 251
        },
        {
            "id": "ed042181-ddae-404f-9cc4-5fd5af23e346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 252
        },
        {
            "id": "94ad9e25-a94c-41e1-ab37-508d51ffb62c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 262
        },
        {
            "id": "6c6a453a-ff89-491f-8d0c-387123ffb3a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 263
        },
        {
            "id": "467e1afa-c851-42a6-a33f-1ad730a0846f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 266
        },
        {
            "id": "127609f3-7235-48d3-ad2b-eed3ad8d7978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 267
        },
        {
            "id": "6fbada35-7737-4a42-9264-439fa9205e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 268
        },
        {
            "id": "9d04cbf4-0bdd-44f7-8320-dcf1dc876ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 269
        },
        {
            "id": "c9b26b2f-cd85-427f-a2b1-e282af80abc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 287
        },
        {
            "id": "937bd65e-4455-4b00-85a6-9bcf0a7a9465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 288
        },
        {
            "id": "0a10c956-d249-49db-93f0-010f2bfe4c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 289
        },
        {
            "id": "8e5eb598-1b14-429d-978f-894a341c4974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 290
        },
        {
            "id": "9af2df10-be3e-419c-ada1-4edaacbdecb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 291
        },
        {
            "id": "60d48eee-9ca1-44a5-a5a8-da60288fc03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 332
        },
        {
            "id": "7e89d664-dd0b-47ac-9e57-1d2117d96d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 333
        },
        {
            "id": "a91ed7ef-4416-422b-8e04-2c049de95cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 334
        },
        {
            "id": "ccecf3fd-b425-4eef-96cb-ed7e33a71a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 335
        },
        {
            "id": "410a6034-25dc-4a96-a2cc-c6107e5f88bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 336
        },
        {
            "id": "8022196d-48cf-419d-8cbe-f62bf50624dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 337
        },
        {
            "id": "daf890d0-3866-4012-afa9-d7aad1aca542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 338
        },
        {
            "id": "410836d2-c549-4c42-9a7c-df9ee946e003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 339
        },
        {
            "id": "e629eaa2-edce-4ad3-9281-3ebfb0c17bd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 360
        },
        {
            "id": "fe1bce44-dc1c-45a0-b931-f81bff7e5d79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 361
        },
        {
            "id": "5a107dc0-0154-4602-aa32-75de22873b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 362
        },
        {
            "id": "5f8d3772-228f-4c58-b3a9-1ef59f2e8c9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 363
        },
        {
            "id": "a88cb6bd-0c38-49f6-bf34-9657377e75a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 364
        },
        {
            "id": "da203035-021d-4438-a93f-974509b93ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 365
        },
        {
            "id": "263889f4-bf6d-41e8-901c-8df92c63d51f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 366
        },
        {
            "id": "317a3489-486a-4847-83b3-6d7c8cc9fee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 367
        },
        {
            "id": "5895fb09-89ab-46f8-9122-83b7edec9b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 368
        },
        {
            "id": "58f7b77e-5590-4089-9809-0e9a6dbfd0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 369
        },
        {
            "id": "79d45482-c4c8-447d-9102-be2514107302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 370
        },
        {
            "id": "661e1855-53cb-4bcf-b624-55076137c705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 371
        },
        {
            "id": "e5b25ea2-7ab1-4c0c-a7e2-a270ff7f744d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 372
        },
        {
            "id": "08d5b5e7-f940-49d7-b671-7d0b8e0a8693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 373
        },
        {
            "id": "71f4da33-6729-43a1-88b6-cccc6d611162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7808
        },
        {
            "id": "ddad432a-ae26-4dbb-9396-cea30e12fa90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7809
        },
        {
            "id": "4e54c06b-77e2-4fce-80b2-9b52ef03ff2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7810
        },
        {
            "id": "2ecd3419-09af-47fa-a441-2c9a25591de4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7811
        },
        {
            "id": "4cb9ec4b-8fed-4488-939b-bf302b0b25f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7812
        },
        {
            "id": "73a0e536-9df1-42e3-8dba-e464e8798fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7813
        },
        {
            "id": "b62690c3-e15c-4521-b221-92332d1e93a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 8211
        },
        {
            "id": "f6553499-51b8-4047-b974-bc7135019bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 8212
        },
        {
            "id": "edbebdf1-32e5-463c-a65f-39961c8213e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 47
        },
        {
            "id": "71b722d1-7562-43ee-bfd2-e84d0d9cbc1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 74
        },
        {
            "id": "50aa3e30-c70e-4866-b0d7-f6481794ce99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 88
        },
        {
            "id": "ec3cbf60-616a-47ee-bf9c-2a7e3f38fb57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 106
        },
        {
            "id": "114ae685-8be4-4924-bf03-aee32ac5bf2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 120
        },
        {
            "id": "202060c7-909c-4e00-96c0-c21a93ee67a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 122,
            "second": 45
        },
        {
            "id": "c8ffe32e-4b1c-4013-a46a-59886fffe2d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 89
        },
        {
            "id": "e6c93daa-d1c6-4971-a349-3e555fad0cf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 121
        },
        {
            "id": "f4e5ad0f-0f4c-4ee8-b60d-d483dccee6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 122,
            "second": 8211
        },
        {
            "id": "94ea4066-4c8a-47a9-ba50-89a44b58dbc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 122,
            "second": 8212
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}