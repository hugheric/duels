{
    "id": "6f9e2902-94f9-495a-ac50-40508282c0c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icosahedron",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 8,
    "bbox_right": 113,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93e67dc4-08b2-46aa-a891-232878b11f9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f9e2902-94f9-495a-ac50-40508282c0c8",
            "compositeImage": {
                "id": "9a7ef576-6a4b-4d1f-98d8-74d08a207dbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e67dc4-08b2-46aa-a891-232878b11f9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e4c8b26-be1f-46d8-a9dc-5f0e01383a5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e67dc4-08b2-46aa-a891-232878b11f9c",
                    "LayerId": "4d993969-c750-4c85-b3c8-16c2b920ce11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "4d993969-c750-4c85-b3c8-16c2b920ce11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f9e2902-94f9-495a-ac50-40508282c0c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 58,
    "yorig": 57
}