{
    "id": "fb0d48b0-5292-4928-bf17-e6f9068b8f51",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_endSequence",
    "eventList": [
        {
            "id": "3abd8b04-ada8-478e-b8f8-c08d89d2f34c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fb0d48b0-5292-4928-bf17-e6f9068b8f51"
        },
        {
            "id": "36136db5-7c78-417b-b302-0299a170e7ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb0d48b0-5292-4928-bf17-e6f9068b8f51"
        },
        {
            "id": "41b4eeee-a346-479e-b968-a3a86729d70b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "fb0d48b0-5292-4928-bf17-e6f9068b8f51"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}