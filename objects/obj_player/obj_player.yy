{
    "id": "319a7bb2-2760-47c1-b9cf-9eedb16be72a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "74ec84ac-3907-4b81-a8e4-fbaa0d7d1297",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "319a7bb2-2760-47c1-b9cf-9eedb16be72a"
        },
        {
            "id": "3d4cd2c9-5aef-402e-bac2-f604b6a3ef0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "319a7bb2-2760-47c1-b9cf-9eedb16be72a"
        },
        {
            "id": "88e1f805-21d1-4bcc-9045-6fea083c3ec2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b2d54146-f531-4038-8401-430adcbc9ae2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "319a7bb2-2760-47c1-b9cf-9eedb16be72a"
        },
        {
            "id": "f4f4490e-280a-4f08-ab71-9a0e775063e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "20e91c94-a4c2-4101-ace8-1da776fe2faf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "319a7bb2-2760-47c1-b9cf-9eedb16be72a"
        },
        {
            "id": "2fa81e6b-c55a-4141-9712-6e220b144cd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "bc18ae00-46cc-4455-a7d7-7a00a58e7ca0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "319a7bb2-2760-47c1-b9cf-9eedb16be72a"
        },
        {
            "id": "0c439032-49a2-43e0-99fe-0e3a94968bb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1d22a4f8-97b9-40c9-9afe-c3822ec7a2fc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "319a7bb2-2760-47c1-b9cf-9eedb16be72a"
        },
        {
            "id": "b41edc34-f5a1-40f4-bed1-f570cb1e53ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d666e7d4-0a5c-4940-87e5-10de7955f184",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "319a7bb2-2760-47c1-b9cf-9eedb16be72a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4cde7704-af56-4942-b929-bfbb18f69469",
    "visible": true
}