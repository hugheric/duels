{
    "id": "1d22a4f8-97b9-40c9-9afe-c3822ec7a2fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_icosahedron",
    "eventList": [
        {
            "id": "fcecce82-7204-451e-ae28-957c24287396",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d22a4f8-97b9-40c9-9afe-c3822ec7a2fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6f9e2902-94f9-495a-ac50-40508282c0c8",
    "visible": true
}