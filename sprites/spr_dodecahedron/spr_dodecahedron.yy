{
    "id": "7272164b-8db0-4266-b7f2-4674d1e27f5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dodecahedron",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 5,
    "bbox_right": 106,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5188d08e-9666-4e58-9f02-d9de5ab93634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7272164b-8db0-4266-b7f2-4674d1e27f5e",
            "compositeImage": {
                "id": "081068f4-c2ee-4e39-ad95-44d5ca103674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5188d08e-9666-4e58-9f02-d9de5ab93634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f1defd9-4122-4f37-b693-f4015c49de1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5188d08e-9666-4e58-9f02-d9de5ab93634",
                    "LayerId": "0826de64-30e8-47b8-bb26-ca3e7adce9ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 133,
    "layers": [
        {
            "id": "0826de64-30e8-47b8-bb26-ca3e7adce9ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7272164b-8db0-4266-b7f2-4674d1e27f5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 118,
    "xorig": 52,
    "yorig": 59
}