next = choose(obj_dodecahedron, obj_icosahedron, obj_tetrahedron, obj_octohedron, obj_cube);
global.rand_speed = irandom_range(wave1speedup, wave1speedlo);
global.scale = choose(.2,.3,.4,.5,.6,.7,.8,.9,1,1.1,1.2, 1.3, 1.4)
y_ = irandom_range(-wave1range, wave1range)


instance_create_layer(x,y + y_,"Instances", next)

alarm[0] = room_speed * wave1frequency