{
    "id": "d666e7d4-0a5c-4940-87e5-10de7955f184",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dodecahedron",
    "eventList": [
        {
            "id": "01575d60-6ead-4857-996f-786e5fef8216",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d666e7d4-0a5c-4940-87e5-10de7955f184"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7272164b-8db0-4266-b7f2-4674d1e27f5e",
    "visible": true
}