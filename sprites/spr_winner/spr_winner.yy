{
    "id": "6d8530e7-68f9-4bdf-b132-6d0d2582dd13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_winner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 429,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ddc21e80-81b5-4ae7-aa85-2d752afd833f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d8530e7-68f9-4bdf-b132-6d0d2582dd13",
            "compositeImage": {
                "id": "a5787cff-776a-4e8a-acc5-b0e65a9870ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddc21e80-81b5-4ae7-aa85-2d752afd833f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a23a4e4-0124-4ede-915d-676a3be8dbb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddc21e80-81b5-4ae7-aa85-2d752afd833f",
                    "LayerId": "be57d8c4-79e0-4973-9159-3165c5ccdf72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 430,
    "layers": [
        {
            "id": "be57d8c4-79e0-4973-9159-3165c5ccdf72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d8530e7-68f9-4bdf-b132-6d0d2582dd13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}