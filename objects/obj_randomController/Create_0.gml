alarm[0] = room_speed * 10

wave1range = 100
wave1speedup = -7
wave1speedlo = -5
wave1frequency = 2

wave2range = 200
wave2speedup = -9
wave2speedlo = -7
wave2frequency = 2.5

wave3range = 300
wave3speedup = -11
wave3speedlo = -9
wave3frequency = 2

wave4range = 300
wave4speedup = -14
wave4speedlo = -9
wave4frequency = 1.5

wave5range = 300
wave5speedup = -15
wave5speedlo = -11
wave5frequency = 1

wave6range = 300
wave6speedup = -20
wave6speedlo = -15
wave6frequency = .75