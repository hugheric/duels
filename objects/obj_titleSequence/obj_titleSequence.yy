{
    "id": "c930338e-30bd-4729-80bf-1658096a9850",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_titleSequence",
    "eventList": [
        {
            "id": "f23a3df0-bb2e-40cc-b74c-acd52b2a8a81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c930338e-30bd-4729-80bf-1658096a9850"
        },
        {
            "id": "eab0959b-8dd2-4124-b88b-0541f7c6fed5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c930338e-30bd-4729-80bf-1658096a9850"
        },
        {
            "id": "166546b1-7b1c-4089-892a-5ac878597cef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c930338e-30bd-4729-80bf-1658096a9850"
        },
        {
            "id": "3d52efa4-743a-4bdb-933c-16fde984f308",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "c930338e-30bd-4729-80bf-1658096a9850"
        },
        {
            "id": "6f15cd90-e778-4e57-b457-bbe2d17cb205",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "c930338e-30bd-4729-80bf-1658096a9850"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}