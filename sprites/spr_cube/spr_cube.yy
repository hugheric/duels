{
    "id": "fddc20fd-9b60-4ca2-9cbc-c618b0e3e55f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cube",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 19,
    "bbox_right": 123,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd68fae1-e650-4ace-92b3-f17351c8cf4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddc20fd-9b60-4ca2-9cbc-c618b0e3e55f",
            "compositeImage": {
                "id": "e8ac4cda-64a6-4fec-b0f0-9bc8e3669884",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd68fae1-e650-4ace-92b3-f17351c8cf4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "071e4a1b-0c3b-4c43-9d0e-e288f74322f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd68fae1-e650-4ace-92b3-f17351c8cf4e",
                    "LayerId": "7740cca8-d362-444c-b13b-a37d1a644246"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 132,
    "layers": [
        {
            "id": "7740cca8-d362-444c-b13b-a37d1a644246",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fddc20fd-9b60-4ca2-9cbc-c618b0e3e55f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 140,
    "xorig": 65,
    "yorig": 68
}