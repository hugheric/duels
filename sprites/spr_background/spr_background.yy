{
    "id": "97ed6730-51a6-4ced-9229-6252e94df018",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 703,
    "bbox_left": 0,
    "bbox_right": 1599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb5b6f5e-6f3d-414d-9c7d-0e85d129f3c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ed6730-51a6-4ced-9229-6252e94df018",
            "compositeImage": {
                "id": "a37a7ed4-5a38-44a1-af2d-eed066c1b703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb5b6f5e-6f3d-414d-9c7d-0e85d129f3c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85f0861c-6711-4c2c-a19c-f8eb425fb397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb5b6f5e-6f3d-414d-9c7d-0e85d129f3c8",
                    "LayerId": "d3509546-367a-4688-a9db-7cba0e34596f"
                }
            ]
        },
        {
            "id": "d245a812-e6ee-4f8e-9b06-8dacd9564279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ed6730-51a6-4ced-9229-6252e94df018",
            "compositeImage": {
                "id": "22c18b64-c606-449b-aabc-ea9636b6a00d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d245a812-e6ee-4f8e-9b06-8dacd9564279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a0c1ce-6512-4148-a740-b2cb09f2f2f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d245a812-e6ee-4f8e-9b06-8dacd9564279",
                    "LayerId": "d3509546-367a-4688-a9db-7cba0e34596f"
                }
            ]
        },
        {
            "id": "89c460c1-46cf-45fb-a3e0-27d14abf4408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ed6730-51a6-4ced-9229-6252e94df018",
            "compositeImage": {
                "id": "16b0eb30-0216-4542-b861-fa2f5025de8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89c460c1-46cf-45fb-a3e0-27d14abf4408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d7dbae-0fda-4295-922b-4589b6e3c9e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89c460c1-46cf-45fb-a3e0-27d14abf4408",
                    "LayerId": "d3509546-367a-4688-a9db-7cba0e34596f"
                }
            ]
        },
        {
            "id": "0b86d769-175f-4b29-b7df-dc5653933df2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ed6730-51a6-4ced-9229-6252e94df018",
            "compositeImage": {
                "id": "d0bfdeb3-1304-487c-a6bd-866e317930bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b86d769-175f-4b29-b7df-dc5653933df2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef94007c-3abf-4c74-bb27-a505264a2ce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b86d769-175f-4b29-b7df-dc5653933df2",
                    "LayerId": "d3509546-367a-4688-a9db-7cba0e34596f"
                }
            ]
        },
        {
            "id": "b65b16dc-89a6-40c0-8326-8340f10a7c97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ed6730-51a6-4ced-9229-6252e94df018",
            "compositeImage": {
                "id": "ce32775a-7e32-4b4f-9c99-edbc8afea484",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65b16dc-89a6-40c0-8326-8340f10a7c97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0e0725e-2127-428a-b392-768296befc96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65b16dc-89a6-40c0-8326-8340f10a7c97",
                    "LayerId": "d3509546-367a-4688-a9db-7cba0e34596f"
                }
            ]
        },
        {
            "id": "eef82d7d-c675-462b-9062-fe2ff1992fb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ed6730-51a6-4ced-9229-6252e94df018",
            "compositeImage": {
                "id": "d3811d5d-5bbb-4ddc-affa-2c3b3c200a4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef82d7d-c675-462b-9062-fe2ff1992fb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58ef6131-2b73-49ef-ad80-af7167d8b98b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef82d7d-c675-462b-9062-fe2ff1992fb3",
                    "LayerId": "d3509546-367a-4688-a9db-7cba0e34596f"
                }
            ]
        },
        {
            "id": "68f4a562-c44f-46b5-ba90-f696a74ceae2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ed6730-51a6-4ced-9229-6252e94df018",
            "compositeImage": {
                "id": "6a151528-3efe-4d82-b714-828b3f22fb1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68f4a562-c44f-46b5-ba90-f696a74ceae2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fac11d5-352f-4ac2-bf16-364a799d26f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68f4a562-c44f-46b5-ba90-f696a74ceae2",
                    "LayerId": "d3509546-367a-4688-a9db-7cba0e34596f"
                }
            ]
        },
        {
            "id": "961f5c23-14e1-44dc-aa2c-0b7ea81dddbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ed6730-51a6-4ced-9229-6252e94df018",
            "compositeImage": {
                "id": "f4f2e5f7-9175-44a3-945a-8e3ac97aea4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "961f5c23-14e1-44dc-aa2c-0b7ea81dddbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df0f42ee-f1a4-4655-a686-2acb59e1da1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "961f5c23-14e1-44dc-aa2c-0b7ea81dddbf",
                    "LayerId": "d3509546-367a-4688-a9db-7cba0e34596f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 704,
    "layers": [
        {
            "id": "d3509546-367a-4688-a9db-7cba0e34596f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97ed6730-51a6-4ced-9229-6252e94df018",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1600,
    "xorig": 0,
    "yorig": 0
}