{
    "id": "b2d54146-f531-4038-8401-430adcbc9ae2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tetrahedron",
    "eventList": [
        {
            "id": "d7b15755-00e0-481b-add7-3c231a169972",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2d54146-f531-4038-8401-430adcbc9ae2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c0239c90-be56-4222-8fe8-6f90f61cab7e",
    "visible": true
}