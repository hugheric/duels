{
    "id": "c0239c90-be56-4222-8fe8-6f90f61cab7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tetrahedron",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 11,
    "bbox_right": 97,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3537c36c-6af4-445f-9f39-dc4de03a4746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0239c90-be56-4222-8fe8-6f90f61cab7e",
            "compositeImage": {
                "id": "83083f38-50f4-483c-98f4-cbacc09b09fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3537c36c-6af4-445f-9f39-dc4de03a4746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e5fb04d-7961-458f-b307-fecab7458f8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3537c36c-6af4-445f-9f39-dc4de03a4746",
                    "LayerId": "3e6ca643-0f1c-4f11-a299-71eebbd9264a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "3e6ca643-0f1c-4f11-a299-71eebbd9264a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0239c90-be56-4222-8fe8-6f90f61cab7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 109,
    "xorig": 57,
    "yorig": 75
}