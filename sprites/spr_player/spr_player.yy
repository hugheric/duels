{
    "id": "4cde7704-af56-4942-b929-bfbb18f69469",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 124,
    "bbox_left": 6,
    "bbox_right": 132,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4dfb969c-fd70-4749-97b8-4b3bd3453135",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cde7704-af56-4942-b929-bfbb18f69469",
            "compositeImage": {
                "id": "66abbd47-b628-4af3-9e21-190a343c9ed7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dfb969c-fd70-4749-97b8-4b3bd3453135",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03c3f746-3fd2-4464-afef-ba90509dde56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dfb969c-fd70-4749-97b8-4b3bd3453135",
                    "LayerId": "37303715-9b9e-449b-9db5-73a5d4021fda"
                }
            ]
        },
        {
            "id": "95c7a944-6410-4000-bafe-0c257aa3767f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cde7704-af56-4942-b929-bfbb18f69469",
            "compositeImage": {
                "id": "f8ee5b63-231f-42a0-99c3-de90c0037d4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95c7a944-6410-4000-bafe-0c257aa3767f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83db5a2a-63bc-4263-bf9b-6225ab5fbdbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95c7a944-6410-4000-bafe-0c257aa3767f",
                    "LayerId": "37303715-9b9e-449b-9db5-73a5d4021fda"
                }
            ]
        },
        {
            "id": "8ff907d2-d95e-4bcc-b543-47eb9383d441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cde7704-af56-4942-b929-bfbb18f69469",
            "compositeImage": {
                "id": "ed6fa710-29d3-489c-a20c-e941f769fc43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff907d2-d95e-4bcc-b543-47eb9383d441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd919f6-845b-479b-9663-9ba58d5eb20f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff907d2-d95e-4bcc-b543-47eb9383d441",
                    "LayerId": "37303715-9b9e-449b-9db5-73a5d4021fda"
                }
            ]
        },
        {
            "id": "e8512186-b008-4191-8263-8950f5d66ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cde7704-af56-4942-b929-bfbb18f69469",
            "compositeImage": {
                "id": "9decb797-d00c-4220-a4b1-76f75e00ebfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8512186-b008-4191-8263-8950f5d66ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5c53ade-d830-46ed-9cef-d53075ce5c08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8512186-b008-4191-8263-8950f5d66ffe",
                    "LayerId": "37303715-9b9e-449b-9db5-73a5d4021fda"
                }
            ]
        },
        {
            "id": "e08bf9fe-7266-47f7-b59a-17051a1a76b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cde7704-af56-4942-b929-bfbb18f69469",
            "compositeImage": {
                "id": "553eb918-cb63-4cec-b58f-ef277bd41474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e08bf9fe-7266-47f7-b59a-17051a1a76b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38057953-2a25-4c3d-8bdc-28dd30483e00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e08bf9fe-7266-47f7-b59a-17051a1a76b3",
                    "LayerId": "37303715-9b9e-449b-9db5-73a5d4021fda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "37303715-9b9e-449b-9db5-73a5d4021fda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cde7704-af56-4942-b929-bfbb18f69469",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 140,
    "xorig": 75,
    "yorig": 77
}