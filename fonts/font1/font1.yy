{
    "id": "31b3feb3-c2c5-4acc-aa6f-1f3345159314",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font1",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Johnny Fever",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "962dca47-87f5-4555-aad7-4cd3a0a34674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 80,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 246,
                "y": 412
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2f48d153-d73d-4f12-a3f5-868e2ea35918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 80,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 403,
                "y": 412
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b4eed50b-39e4-4dda-9e9e-7de9c3120255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 80,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 134,
                "y": 412
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b54447e0-7199-492e-9e0f-787816689582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 80,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 170,
                "y": 248
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d91d4a95-e618-45ed-ad3c-4ca658552ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 80,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 104,
                "y": 248
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "73e4693d-e01a-4e35-8d66-90f63e8be57f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 80,
                "offset": 2,
                "shift": 51,
                "w": 47,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d116966f-ea31-43a6-8114-d80e7695598a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 80,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 78,
                "y": 84
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "fbca07e1-82f2-42c4-a0b1-80f166a470f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 80,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 451,
                "y": 412
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c211496b-1617-495e-9d52-28f48830b187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 80,
                "offset": 5,
                "shift": 23,
                "w": 18,
                "x": 226,
                "y": 412
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "28560ae8-e68e-4648-9522-5324d95eb972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 80,
                "offset": 0,
                "shift": 23,
                "w": 17,
                "x": 285,
                "y": 412
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0b7d8f32-27b4-4c05-ad83-1ec28c1b5570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 80,
                "offset": 1,
                "shift": 36,
                "w": 35,
                "x": 41,
                "y": 84
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3d5ac328-d37a-4bea-a97e-3a8fd6e3b6c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 80,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 391,
                "y": 330
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "feace89d-2060-4cdf-8f8c-3197068ae5d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 80,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 375,
                "y": 412
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b8ee2828-f788-456a-ad0b-dc02ebec9c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 80,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 159,
                "y": 412
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c6712d14-0fe4-4069-ae25-d992281c4fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 80,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 390,
                "y": 412
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c391d6ae-ee12-4920-a6bb-5c08cd74c36b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 80,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 274,
                "y": 166
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "87b2fcc0-197f-4ef7-8703-727c252f3c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 240,
                "y": 166
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "9405c8bc-9ffa-4fdb-bf15-87c59876b816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 80,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 266,
                "y": 412
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ec498d64-4d2e-4465-b82f-bd2dd410d3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 376,
                "y": 166
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c7eb603b-26b3-472b-a58d-6dfa241d7fef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 80,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 481,
                "y": 330
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "df8471e2-4e51-4e6f-8fe6-083bd1f1bca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 80,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 299,
                "y": 330
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2436abb4-1def-42ab-bf74-ccd4c22f90b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 80,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 467,
                "y": 248
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "400840d4-2b29-45ab-891e-034e72487a9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 342,
                "y": 166
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "37664b54-02e5-411c-8b11-ff47bddd88b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 80,
                "offset": 1,
                "shift": 34,
                "w": 33,
                "x": 184,
                "y": 84
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "79f5666d-30a6-4155-abf3-43585f22b9fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 444,
                "y": 166
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0fa449d8-7a3e-4ab5-afd9-b55023ddf9e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 167,
                "y": 330
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "404ae80a-da6e-4aba-9729-45f8427ad401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 80,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 416,
                "y": 412
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "eac771fb-bfc5-44c4-838f-9a64b00a4dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 80,
                "offset": 2,
                "shift": 18,
                "w": 13,
                "x": 360,
                "y": 412
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9f6a754b-ad33-4d60-b014-5dcece94caa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 80,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 451,
                "y": 330
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c90566b9-b166-4e09-9d5b-a5e0fa96dc24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 80,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 421,
                "y": 330
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "db94e1fe-24af-4a46-8329-6d0d151e5846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 80,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 361,
                "y": 330
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9d283fdb-102d-4df9-88ff-d536fbde78b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 80,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 138,
                "y": 166
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "64f0e2e4-6502-48d3-a462-d0b2d19b554f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 80,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 200,
                "y": 330
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4f89ede4-523c-47b3-b2f7-b38a73b04b2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 80,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 2,
                "y": 166
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2d6ca3b8-686e-43a7-98e0-4083fdebe6d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 80,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 134,
                "y": 330
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "88a98610-bce7-41da-9362-b91c633ea2ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 80,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 457,
                "y": 84
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "79d0a61a-59d2-4595-b296-ff82bf6f2440",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 80,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 101,
                "y": 330
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "0cb1595a-fcba-4066-beb0-979836263828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 80,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 68,
                "y": 330
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "dcd101dd-78d2-4a1c-a23d-502725b3cc2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 80,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 35,
                "y": 330
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9ec4e3cf-17e9-41b3-9c52-3b74dd90ee1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 423,
                "y": 84
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8353a112-c179-4ef4-a238-03e053ad4326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 80,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 2,
                "y": 330
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7e967756-3366-492b-9dc3-67b514bd6416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 80,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 440,
                "y": 412
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1a0996f6-095f-4b08-9140-6afa5a9e7440",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 80,
                "offset": 1,
                "shift": 28,
                "w": 24,
                "x": 56,
                "y": 412
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b0551baa-b0b5-43aa-a482-1a93ee2f1e5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 80,
                "offset": 3,
                "shift": 40,
                "w": 37,
                "x": 437,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e2513ded-df96-41b0-846e-5a42f708c688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 80,
                "offset": 3,
                "shift": 28,
                "w": 24,
                "x": 30,
                "y": 412
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9211d17b-072b-4670-be45-5a6a447a47f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 80,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "94ed3b5a-b1ad-4aa4-9db3-b635130c8390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 80,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 302,
                "y": 248
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "14c6cff8-da23-42fb-bf32-e8493a968a65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 172,
                "y": 166
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8a5cabf1-037b-4b01-82b4-568afd81c506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 80,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 269,
                "y": 248
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d49ccf33-1b92-407a-bb4d-fd569c7f43fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 321,
                "y": 84
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "56141345-df92-4364-a7bc-f75b6abec158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 80,
                "offset": 3,
                "shift": 36,
                "w": 32,
                "x": 104,
                "y": 166
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "11246680-6161-4ecb-ae45-2c4afa4ecc4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 80,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 236,
                "y": 248
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3ad89014-aa3b-42d3-91ee-3c91629a5a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 80,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 287,
                "y": 84
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c38dfac3-f099-4e4e-8dc4-dfa14b693886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 253,
                "y": 84
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0d5df516-2078-4191-b5ae-4e7d14529243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 80,
                "offset": 0,
                "shift": 37,
                "w": 38,
                "x": 357,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a149d8b5-51ef-4bba-b76f-8a91e39b1800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 80,
                "offset": 2,
                "shift": 49,
                "w": 44,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c1af2a17-6b80-47a3-9742-e02a8cd55c5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 80,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 316,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ef9678a7-a625-4c4b-93aa-2e63ee081419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 80,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 219,
                "y": 84
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b4537504-f6d2-46e1-86a1-5c529ba283a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 80,
                "offset": 1,
                "shift": 36,
                "w": 33,
                "x": 114,
                "y": 84
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "49f39298-7e38-4a74-9748-b928e150ed0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 80,
                "offset": 5,
                "shift": 23,
                "w": 17,
                "x": 304,
                "y": 412
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3ba89f1e-3655-45f7-b52c-0a69796e9977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 80,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 70,
                "y": 166
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "142a9f58-49da-4109-8bc8-8f74152ed164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 80,
                "offset": 1,
                "shift": 23,
                "w": 17,
                "x": 323,
                "y": 412
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "037ea86a-895d-4bab-965a-043154b3775c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 80,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 2,
                "y": 412
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f6bd8356-55d9-4a9a-a8d2-d359e4d43fe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 80,
                "offset": -1,
                "shift": 38,
                "w": 40,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e4398431-2237-4d54-8cf7-4785e668088a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 80,
                "offset": 3,
                "shift": 22,
                "w": 16,
                "x": 342,
                "y": 412
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "cbadce07-8626-40b7-8231-e9f1301ac2bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 80,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 36,
                "y": 166
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b81e9fa9-a86f-4290-a348-0a1d704f9dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 80,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 203,
                "y": 248
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0e0745dc-0a89-4d2a-b4f9-0b70f0182ac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 80,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 355,
                "y": 84
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6d626643-6608-4b71-aa9d-fd1806dc98c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 80,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 233,
                "y": 330
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7d85eaea-27bc-4890-8fc7-7742ce5f7d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 80,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 401,
                "y": 248
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "fddf27ee-2fbc-4a54-a827-766cecd3f258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 80,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 434,
                "y": 248
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6512b6a6-7237-49dc-8951-cafbb1ca10d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 389,
                "y": 84
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "fd20c72b-37b4-4e35-8abe-f4a86364ed44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 80,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 266,
                "y": 330
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e2ff08e0-bad5-4996-bc7a-921fbbe9eba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 80,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 429,
                "y": 412
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "548986f7-a2eb-4c9c-a9f6-03b5e4ef3090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 80,
                "offset": 1,
                "shift": 28,
                "w": 24,
                "x": 82,
                "y": 412
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d41c9bbb-eb29-4de2-8304-971e6a577e94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 80,
                "offset": 3,
                "shift": 40,
                "w": 37,
                "x": 2,
                "y": 84
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "195115d0-029d-4ac7-9b5b-09df79baba38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 80,
                "offset": 3,
                "shift": 28,
                "w": 24,
                "x": 108,
                "y": 412
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "1b0f7536-12c7-418b-bbe0-aab4336a3249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 80,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "04cb8441-f52b-42aa-9907-01ae14d8af9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 80,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 335,
                "y": 248
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "da1b77b5-d405-4016-b979-ba5850d504eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 36,
                "y": 248
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "69339dc1-d192-4a93-a50b-50f5bc065a69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 80,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 368,
                "y": 248
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "07e15ff5-0ee2-4bef-af3b-840bb2a55e7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 206,
                "y": 166
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4bbdcce8-f350-4a87-ad06-9c4982c6bff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 80,
                "offset": 3,
                "shift": 36,
                "w": 32,
                "x": 308,
                "y": 166
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "407dad64-8470-498c-a898-e42b84fe31db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 80,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 137,
                "y": 248
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3570091b-11a2-44de-a3f2-f0868d923c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 80,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 410,
                "y": 166
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "12aaabb0-75eb-41c8-86ce-8e91eb9ac8ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 80,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 70,
                "y": 248
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6e57478c-866c-4665-b5e9-578cc2260b2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 80,
                "offset": 0,
                "shift": 37,
                "w": 38,
                "x": 397,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5d19df61-35cf-4b4a-b592-5a4bc6d3ac20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 80,
                "offset": 2,
                "shift": 49,
                "w": 44,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "27ea8d89-8cdf-4cc0-8370-d2c17f068fa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 80,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 275,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0c5c0550-e82a-461d-86f7-2d461ef8a6f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 80,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 2,
                "y": 248
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2b1fa980-4b2a-4ab5-a9a4-d0f074e2edc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 80,
                "offset": 1,
                "shift": 36,
                "w": 33,
                "x": 149,
                "y": 84
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "769856b7-32b9-42e9-8042-4efdcb7b071f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 80,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 184,
                "y": 412
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "5204c1db-5936-471f-b990-2ef93622d6f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 80,
                "offset": 9,
                "shift": 25,
                "w": 8,
                "x": 462,
                "y": 412
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f0f2450f-6acc-4a52-933c-f961655f5a02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 80,
                "offset": 1,
                "shift": 23,
                "w": 19,
                "x": 205,
                "y": 412
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b3bc8f4f-e12c-4c6d-80f9-2cee47d57023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 80,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 331,
                "y": 330
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "55abf915-29ac-4492-ba18-aadfcb37dee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 74
        },
        {
            "id": "a8ee54c2-8c50-489c-aaf5-8dc046de4ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 106
        },
        {
            "id": "cf51712d-664c-4ffa-a4de-470521f8b102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 34,
            "second": 34
        },
        {
            "id": "ae505887-e80c-4c50-a977-8081c5980a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 34,
            "second": 39
        },
        {
            "id": "8d5a88e6-c144-488f-9cab-07b78844dee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 44
        },
        {
            "id": "eed543c9-3e6c-4035-9278-903d5723b639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 46
        },
        {
            "id": "d84e2ba4-e66c-4811-a961-6ec5391fa32d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 34,
            "second": 74
        },
        {
            "id": "049f6809-a21a-4962-b560-f235e67cfca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 34,
            "second": 106
        },
        {
            "id": "0a00355e-775b-456c-a4dc-059ae695a5f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 224
        },
        {
            "id": "59c666ab-375a-452c-93d3-5511238c9756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 225
        },
        {
            "id": "7560ec6b-9396-419a-b890-da2b512ca4b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 226
        },
        {
            "id": "8e92a806-1f18-4131-a79d-a14f467e9676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 227
        },
        {
            "id": "5cf81b22-76c7-4fb0-8696-8c33168c46cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 228
        },
        {
            "id": "0885e4f1-8bed-4841-8b3c-19a3fd515cae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 230
        },
        {
            "id": "8b5fedcb-5a85-4208-b17c-671385671d43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 256
        },
        {
            "id": "0b8bb8b1-d59c-42e7-b044-7c1a8ed1f0db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 257
        },
        {
            "id": "118958b1-1057-4b5d-a8d0-ae8e412aa6a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 258
        },
        {
            "id": "fac2ff67-a460-40fc-814d-cbdc6e11181d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 259
        },
        {
            "id": "c5d0320b-24b2-4214-a735-d8d0e259313a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 260
        },
        {
            "id": "54c5843b-198b-4427-b396-e79c22df3f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 261
        },
        {
            "id": "b7e87bfa-f64d-4559-9634-ab5e11cd144c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 354
        },
        {
            "id": "d68c49a9-815f-4ed4-8d7c-b8d7b0451b26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 355
        },
        {
            "id": "7966ead3-99f1-475d-98d1-af43f3639e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 356
        },
        {
            "id": "83ddfc9a-2f9a-409a-a45a-48ba7e83514f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 357
        },
        {
            "id": "c10de3bc-5e57-4371-aa34-d00a546dfd5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 358
        },
        {
            "id": "1c7eb2e6-98a7-4aea-8c96-1a0817e224f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 359
        },
        {
            "id": "aa961dba-a100-4e8b-a630-8e068a1aca35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 538
        },
        {
            "id": "eea7c55f-c94d-4e64-9440-c1a77387252b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 539
        },
        {
            "id": "8d8ce6cb-c9e5-4423-b794-d42e0291a846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 34,
            "second": 8216
        },
        {
            "id": "3bbe3653-a030-4367-aaaa-1bf32c6eb6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 34,
            "second": 8217
        },
        {
            "id": "ce7ca9ee-68b7-4a6e-a951-45fe67f2a601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 8218
        },
        {
            "id": "6b44698a-33e2-4995-8877-b14ea7787f04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 34,
            "second": 8220
        },
        {
            "id": "4447b538-3f8e-45a4-9df3-ccb7f7f021f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 34,
            "second": 8221
        },
        {
            "id": "70183e00-d87a-4667-a406-10e9bfa15cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 8222
        },
        {
            "id": "b700130c-1a3b-405e-bd2d-d2901a7e4599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 8230
        },
        {
            "id": "fbe4c210-17c5-42f8-ba85-60b066a209b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 38,
            "second": 84
        },
        {
            "id": "982369ca-06b8-4071-9d54-12e41e777a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 86
        },
        {
            "id": "d1db628b-e8ce-4d53-a52a-fc09ede14acd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 89
        },
        {
            "id": "1147411c-428c-438a-bcdc-65564ed19175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 38,
            "second": 116
        },
        {
            "id": "9532c65d-d7f0-46c8-a17c-d70edd1d7e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 118
        },
        {
            "id": "0fde8820-cde0-4457-9690-8d191b220d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 121
        },
        {
            "id": "a8cc491a-0496-47ad-b47e-999af4b4fbbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 38,
            "second": 354
        },
        {
            "id": "e4e5eeb8-a7f5-4a57-9ca1-52a72a253972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 38,
            "second": 355
        },
        {
            "id": "956fd9e9-b8c1-4b33-abf8-6059af213267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 38,
            "second": 356
        },
        {
            "id": "2b2026fe-cac5-4ecf-ab3a-719c37d5c665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 38,
            "second": 357
        },
        {
            "id": "7107f5a7-f3d5-4dec-b615-81631724a87b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 38,
            "second": 358
        },
        {
            "id": "2241c004-b52b-429b-93b8-84c0b73ee0b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 38,
            "second": 359
        },
        {
            "id": "e7a733d1-be39-4cc6-8282-3bc025ea9fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 38,
            "second": 538
        },
        {
            "id": "6022b327-54cb-40d9-b6b4-d63deddad55c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 38,
            "second": 539
        },
        {
            "id": "307f49d9-bb83-4078-94f5-33f671c1a73d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 39,
            "second": 34
        },
        {
            "id": "95670c02-da21-46fe-96bc-9ffc36a71708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 39,
            "second": 39
        },
        {
            "id": "d54c1254-e284-41e3-929c-e4f47a745e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 44
        },
        {
            "id": "f407cbe9-9221-4013-89d7-e9858314db61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 46
        },
        {
            "id": "b6b77c14-97a1-4053-8826-e24c4a212f57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 65
        },
        {
            "id": "e17c4447-c0ec-41db-973e-b0bcef262236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 74
        },
        {
            "id": "0191bbbe-2ae6-4686-bfaf-0ec028065dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 84
        },
        {
            "id": "332bc1fa-69fd-427a-b7d4-195365cbc301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 97
        },
        {
            "id": "9470b0e2-d343-45cf-b00a-1d01978c35b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 106
        },
        {
            "id": "9932eaf6-0f3c-44b0-a921-0e79b438070c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 116
        },
        {
            "id": "46705479-75de-40cd-a4ad-93c49c3b33bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 192
        },
        {
            "id": "0ed274b9-ad05-4525-8552-145a33f37063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 193
        },
        {
            "id": "74212060-11ab-49b1-b7eb-2de4116cd9ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 194
        },
        {
            "id": "f6f6ba44-714f-483d-a96f-592f1249d5ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 195
        },
        {
            "id": "9c4ad792-dd1c-40d2-95f8-5d8132ff30f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 196
        },
        {
            "id": "133c274e-12d6-4b01-88a7-c137872b1781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 197
        },
        {
            "id": "2c264fa8-6c11-4f31-94e4-72fe8ea942d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 198
        },
        {
            "id": "31ac2e49-bde6-4200-aeb2-e42b535a474c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 224
        },
        {
            "id": "469940f7-10ad-44cf-8121-a550b8ed2ede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 225
        },
        {
            "id": "2b264aca-46ea-4118-98b4-e79199c9a11e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 226
        },
        {
            "id": "77b98a36-93f6-4074-ae0c-268a33c17528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 227
        },
        {
            "id": "df67a9af-0367-425b-8a7f-6ce276066ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 228
        },
        {
            "id": "33502447-6a61-401c-ae33-27c5e040cf30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 230
        },
        {
            "id": "fea25593-3c02-46c9-8879-1c8bf66744c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 256
        },
        {
            "id": "584aeba5-d17e-4c6f-8a62-c153a06bc68c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 257
        },
        {
            "id": "e29935ba-5882-4ee7-9c2d-b5db11f805ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 39,
            "second": 8216
        },
        {
            "id": "8bbf3104-9634-4801-9645-9e1898b8e998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 39,
            "second": 8217
        },
        {
            "id": "4c833980-1b08-4fe6-825d-2d0c5cb211b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 8218
        },
        {
            "id": "20d9a454-85b5-4d3a-96b0-618542de1426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 39,
            "second": 8220
        },
        {
            "id": "3f733064-f2c6-43ae-8d84-97bccac306f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 39,
            "second": 8221
        },
        {
            "id": "0df08261-4836-4e63-9b66-860a18c765f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 8222
        },
        {
            "id": "36e7ef25-8cf7-4d5e-872c-105e054cdd94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 8230
        },
        {
            "id": "bfe7d1c6-28bc-4c59-bb0f-2b0848c3458b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 34
        },
        {
            "id": "60ece354-e2cc-45a4-a1ce-706f563f4e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 39
        },
        {
            "id": "74608c19-058e-4217-b316-b8d1ab7e51dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 44,
            "second": 49
        },
        {
            "id": "245435ad-1116-49e6-b3f5-35e7d0bce3f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 52
        },
        {
            "id": "4ea8c67f-9354-477c-a503-67c11dce1201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 84
        },
        {
            "id": "541ec76b-33c8-4f6b-97bb-63f1ad3fcac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 86
        },
        {
            "id": "ed708977-e0e6-4572-a266-b2c49339e3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 89
        },
        {
            "id": "57215e3d-d7a0-40a1-aa56-981a217da6d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 116
        },
        {
            "id": "5ddda402-bf12-44a1-88e2-4b5eedf4497e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 118
        },
        {
            "id": "b63d9ff7-4929-4560-8529-fe61985da8f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 121
        },
        {
            "id": "69188054-95b7-480f-bd05-83e59328f45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 354
        },
        {
            "id": "5c61c808-efaf-49b5-a8a9-e57e7b95aa03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 355
        },
        {
            "id": "43c70a38-8456-4132-8d93-43a6fe7cef00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 356
        },
        {
            "id": "358a389f-c650-4938-8b8e-9307d55c9ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 357
        },
        {
            "id": "e1c25c96-e432-47f0-9707-359a7e80e8b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 358
        },
        {
            "id": "278560e3-0061-4acd-8a4e-190b8dc7b8ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 359
        },
        {
            "id": "f70817e3-6360-423a-8c40-d190244a3eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 538
        },
        {
            "id": "5c102572-3159-45fc-90e8-b1459d680554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 539
        },
        {
            "id": "f7ef4c4d-6896-4194-b62b-dc6c476abf8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8216
        },
        {
            "id": "6138f4e5-3f62-4ef3-9700-1974e07eee73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8217
        },
        {
            "id": "f20a20f1-9ece-4102-9225-55bf1ca5463e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8220
        },
        {
            "id": "61acd111-e323-4571-b2f4-9688e9a25df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8221
        },
        {
            "id": "d99575d3-073c-4a3e-85d8-089f69abce4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 45,
            "second": 74
        },
        {
            "id": "ce53e72f-85af-4ea4-ab1e-b64fc826d740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 84
        },
        {
            "id": "adee818d-b2ea-4c6b-95a1-76195992efde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 86
        },
        {
            "id": "cc313ce1-2cec-4f04-8d5e-4b3503bbed0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 88
        },
        {
            "id": "875e39b7-3d32-4447-82d6-f0344723bb20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 90
        },
        {
            "id": "284d9f91-05cc-403e-93bb-9e73f29bd579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 45,
            "second": 106
        },
        {
            "id": "42dbccb8-d9de-4a51-99ef-207d905a0064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 116
        },
        {
            "id": "f1e0806f-fbd6-41db-8f7c-69f1a649a119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 118
        },
        {
            "id": "5cdd1df7-6c96-4a30-920b-8a7572f3c2ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 120
        },
        {
            "id": "14e44b8f-4d2e-4911-b977-19ce03288874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 122
        },
        {
            "id": "113d3215-e41b-4b71-b429-32a9eca268a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 354
        },
        {
            "id": "0657be90-f4d7-4696-9504-2f2ca2bae443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 355
        },
        {
            "id": "1d229fa7-8bfb-41f4-980a-2fdc21793c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 356
        },
        {
            "id": "81c92921-26b5-4399-bc3f-b327d8135541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 357
        },
        {
            "id": "ccf5785f-454b-4d36-a343-3a58f020a94d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 358
        },
        {
            "id": "ddc80722-32a4-4acf-9fb1-f22b39642a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 359
        },
        {
            "id": "1e31f927-ad74-4648-9a82-399fdc48353c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 377
        },
        {
            "id": "69e843a2-82a9-4110-b431-39180b34105f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 378
        },
        {
            "id": "d379c619-75be-4f1f-b531-73d2b04110d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 379
        },
        {
            "id": "72f8afc1-26ba-4911-b9e7-aab4a1f2fa75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 380
        },
        {
            "id": "48d46bf5-c8e3-48b7-b3de-c933fdcac571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 381
        },
        {
            "id": "27bfea2c-ffb7-408f-976e-474f443fb7ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 382
        },
        {
            "id": "557279cb-0126-4511-acfe-090944ce10cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 538
        },
        {
            "id": "264ff2bc-9638-4973-b29c-edf54c45e067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 539
        },
        {
            "id": "2c79d7c8-c2dc-41c5-be92-26af773606ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 34
        },
        {
            "id": "8037a618-da4a-4a93-a58f-22b3efc2035b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 39
        },
        {
            "id": "46f18999-d70d-4cb4-98d4-fddf3308e0a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 46,
            "second": 49
        },
        {
            "id": "4dd3ee4e-6f0d-437e-9eab-ce7269536a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 52
        },
        {
            "id": "e7347189-84c6-46d7-acab-10e5bcfe5956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 84
        },
        {
            "id": "ef933324-87f6-43a7-9e81-2b37ec802ebc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 86
        },
        {
            "id": "6f03fde4-e6bf-4d59-bd00-dd2807969e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 89
        },
        {
            "id": "3c90b278-af24-42b9-9ce8-fdd41b8ba36c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 116
        },
        {
            "id": "629e0a69-6e67-4a3d-9c11-11cb1a413a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 118
        },
        {
            "id": "e89d2359-7d06-4d33-8747-3070d3e0026a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 121
        },
        {
            "id": "2e8a89a0-e854-4d55-8ab0-efd40405cc2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 354
        },
        {
            "id": "297a2f52-dee8-4386-ad3e-ded03ca5decb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 355
        },
        {
            "id": "122481c9-9170-4239-b91e-cc732e6a97e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 356
        },
        {
            "id": "8e51c060-aa89-487d-9ee2-f661b6b74c72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 357
        },
        {
            "id": "93e19c12-c7e0-4a2e-bdf9-3842b31db49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 358
        },
        {
            "id": "6cf1346c-cd58-4883-a1df-65a63ff657f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 359
        },
        {
            "id": "0c9d137e-7bc3-4af3-ab8b-3db5d4912aab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 538
        },
        {
            "id": "6301cabf-f7a1-48dd-8623-c204d2957797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 539
        },
        {
            "id": "352bfda2-8427-48d3-b47a-ab53d6c46faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8216
        },
        {
            "id": "9f177f7f-a469-4256-b20f-f9c70c115392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8217
        },
        {
            "id": "4b6b870d-2ca6-4321-8f62-480744f527b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8220
        },
        {
            "id": "8a218ff2-c558-4e80-a5df-b82fd2d93ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8221
        },
        {
            "id": "e25c8d00-b600-445c-b12b-68eff0af2857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 48
        },
        {
            "id": "a216de64-851b-4ec4-8e20-fe205fdc5423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 54
        },
        {
            "id": "77fc702a-1628-4f52-91a8-6e0ed37f2f8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 56
        },
        {
            "id": "da45f0d8-3110-41c0-a15b-c399b15453dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 57
        },
        {
            "id": "6b6c740c-7971-499d-bc0f-38a2e91bc0ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 65
        },
        {
            "id": "f2a60e52-55ab-467b-9ade-ed431c237f2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 67
        },
        {
            "id": "496d7c49-af4d-487c-8790-aa8dae032078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 71
        },
        {
            "id": "6020be05-85a5-41c9-8663-cd09360023ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 47,
            "second": 74
        },
        {
            "id": "0138a112-4da7-47e7-a604-fc8caa427b55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 77
        },
        {
            "id": "e648247d-2912-452c-92f4-8280b90a1d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 78
        },
        {
            "id": "b3f0deb4-a0fa-47b6-993e-3f888096ff16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 79
        },
        {
            "id": "e93ce9e0-7190-4e72-8c57-02c4fac634f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 81
        },
        {
            "id": "b0e22ef7-c063-4517-9a92-bdd31ce8ad56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 83
        },
        {
            "id": "08073393-ba99-4dc9-919f-1aca8dd94e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 97
        },
        {
            "id": "9c0527f3-d135-4d63-871f-60732f21eefb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 99
        },
        {
            "id": "1becb34e-2196-4339-9969-1c3f5a649102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 103
        },
        {
            "id": "b618f9e2-708a-46ae-8145-62c57325678c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 47,
            "second": 106
        },
        {
            "id": "8b23c52e-04df-4495-b48c-cb56dec509cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 109
        },
        {
            "id": "87fd8af4-17e6-4962-8ac1-e14dff7de2c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 110
        },
        {
            "id": "feae6073-d621-43f4-910e-e70207999188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 111
        },
        {
            "id": "9f358237-0b52-4ed2-a95c-69b0822d10a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 113
        },
        {
            "id": "6664a04d-016e-4f1e-95d1-9e9b7a7aa9e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 115
        },
        {
            "id": "2c37a1a6-dab6-4050-838b-b60402c2e11d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 192
        },
        {
            "id": "31d5bbcc-1215-4083-9e3a-61d0202372fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 193
        },
        {
            "id": "176c8ea5-7c61-4387-babe-f26ba7ba2d9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 194
        },
        {
            "id": "92580400-34ee-40c9-9652-6396fe76294a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 195
        },
        {
            "id": "869ec076-4734-47d8-a9ff-2f0466e20b1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 196
        },
        {
            "id": "8adb40b2-20f3-4d78-bdcc-509f4958afc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 197
        },
        {
            "id": "ab62442b-5b80-402b-8464-e1bb6f21220e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 198
        },
        {
            "id": "8acea07f-f1a8-4903-a495-e9133c887569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 199
        },
        {
            "id": "f031a6d9-5b2f-4bad-adf8-2e4a2a920696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 209
        },
        {
            "id": "9ae0d4f4-79c9-47db-8974-64a410fef3c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 210
        },
        {
            "id": "1b8bdebd-3edb-4b5e-ac5c-e8e9a8fb7049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 211
        },
        {
            "id": "85ab50b4-b7a0-4a8b-a442-2bbd08c711fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 212
        },
        {
            "id": "5c66a42a-5e47-46b8-9de4-956b0f85cfeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 213
        },
        {
            "id": "8e079e8c-90a3-4af3-ab29-aadb867e6a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 214
        },
        {
            "id": "e8389588-b191-4956-b444-658602f4e907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 216
        },
        {
            "id": "4d76ffc6-2604-4ad5-999a-cf79d0034ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 224
        },
        {
            "id": "be0dd255-daa3-48f0-b5b2-23e9f2b8cc5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 225
        },
        {
            "id": "195cd09b-cf3f-40c4-958d-3a342efcae95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 226
        },
        {
            "id": "b741fb72-eaa3-42eb-87d9-f65225ccc98f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 227
        },
        {
            "id": "321932ed-8a2e-4692-865c-287da7650253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 228
        },
        {
            "id": "dab36c08-5bba-44ad-ad33-87dd65c5fa8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 230
        },
        {
            "id": "9fc0b304-5c21-4680-aa39-d5621502252a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 231
        },
        {
            "id": "9cc2ffbe-5299-49ff-bfc3-6fa80b84ddda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 241
        },
        {
            "id": "8c2564c4-352a-467f-b143-f90f71ca4da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 242
        },
        {
            "id": "2cf988b8-70f8-4b01-a7da-66b200eceb5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 243
        },
        {
            "id": "ddd3281c-2223-4f6f-8d8b-03aaf3872b3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 244
        },
        {
            "id": "535ce3f8-8a72-4b6e-ac1c-5a4fe6154c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 245
        },
        {
            "id": "136459b1-321d-4af0-a50d-5683360e9c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 246
        },
        {
            "id": "7bedb2d1-2918-4c15-af3a-708ffc5ee2a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 248
        },
        {
            "id": "c19afa4e-6889-4ec1-b830-db85a688d1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 256
        },
        {
            "id": "77190564-bfe8-4fc6-a225-63948e5245fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 257
        },
        {
            "id": "2e6bf572-f4c7-43a1-8c54-c39c645b83bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 258
        },
        {
            "id": "d5343a96-bd5f-4925-ba27-986222eb31fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 259
        },
        {
            "id": "f1c879de-dfba-4839-9b57-2adf904f747c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 260
        },
        {
            "id": "892fc474-28fe-4ea4-87c0-8464dd767526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 261
        },
        {
            "id": "d4206b01-f611-486b-b84a-3e3d84b9978c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 262
        },
        {
            "id": "11cb8875-01df-42c4-b59c-d7a5210122c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 263
        },
        {
            "id": "2a1bdda6-a474-41ba-8e2a-df1fb630b27f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 266
        },
        {
            "id": "ff0dc234-ac9a-4e9c-8c03-582b93fc7a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 267
        },
        {
            "id": "e81ec21a-beca-4ea7-96fd-ba577ae4fb81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 268
        },
        {
            "id": "6981e6a5-b919-4894-a0d8-d7d329d2ca73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 269
        },
        {
            "id": "ad80671c-a99e-41b9-8a08-ab3093f553c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 287
        },
        {
            "id": "803f7735-945e-4821-a270-82b38b439c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 288
        },
        {
            "id": "35e467bc-0c74-4f3d-af3f-a5ac6647061a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 289
        },
        {
            "id": "7fd0f021-b684-4d40-a3f0-73e948948ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 290
        },
        {
            "id": "c488b694-d820-4ed3-a5aa-f070877e5179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 291
        },
        {
            "id": "ecdd3e65-f2f4-4e9c-b87f-fe7c34d9d0ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 323
        },
        {
            "id": "d76b1e0f-f1ff-4064-bdb3-42a5d3bc3be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 324
        },
        {
            "id": "0135ca14-3d03-486e-b99c-81f164b64913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 325
        },
        {
            "id": "e5b5a491-00cf-4d18-9fc2-9dc311890ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 326
        },
        {
            "id": "ee35e98c-0d36-497e-8e86-88a6acef9be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 327
        },
        {
            "id": "8203a8f7-8de9-4c5c-8a04-47a58c894847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 328
        },
        {
            "id": "e99481a0-22ff-47cc-a9e8-d8ae964695a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 332
        },
        {
            "id": "a0874fa3-e200-47f7-8085-806de9cffa3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 333
        },
        {
            "id": "87a85f68-420d-4cc5-8c9f-126f22222d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 334
        },
        {
            "id": "2f17ae41-66a0-43b1-b37b-5e9e5953b436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 335
        },
        {
            "id": "08ff29e4-4028-4d35-a640-5de641fe0ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 336
        },
        {
            "id": "16518450-946d-4792-837b-20d142bc31ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 337
        },
        {
            "id": "5bec6a3e-c18c-40de-865e-4a17571e349b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 338
        },
        {
            "id": "0d428015-fdbc-418a-8ceb-7dad410cdce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 339
        },
        {
            "id": "10adcb10-644b-444b-97db-c4d10927bf9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 346
        },
        {
            "id": "bf14d6eb-9255-4dec-8cf6-0f5abbff65b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 347
        },
        {
            "id": "720e117b-baf3-4007-adab-998aa859cef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 350
        },
        {
            "id": "abe6287b-70f9-4cbd-8a07-2751daf8503a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 351
        },
        {
            "id": "bca2ee95-2081-4368-8840-ca3d93d5ef79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 352
        },
        {
            "id": "5300e60a-0caa-47de-8b80-5833d531447e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 353
        },
        {
            "id": "723e505f-2a48-4023-af39-402ffc6acd01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 536
        },
        {
            "id": "b4b50628-0522-4477-a288-7141835680ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 537
        },
        {
            "id": "ac573b19-80b2-4cec-9eaf-f78433454739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 47
        },
        {
            "id": "654a6f8b-0d5e-4d30-8564-c175a8265658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 55
        },
        {
            "id": "508d1057-3efd-49cd-8851-0d12ca0ff059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 86
        },
        {
            "id": "cc70afa8-f14d-4329-b29c-3bc3f96927fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 88
        },
        {
            "id": "c216a09d-ccbf-4370-a9b9-38ecf9e76b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 118
        },
        {
            "id": "c981ab64-c1e1-456c-b3c3-9b7800aed4d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 120
        },
        {
            "id": "53002701-96ad-427c-85cf-bce82a2a9596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 55
        },
        {
            "id": "fb0b58df-714b-4f4a-b62f-7eacc9edc3d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 49
        },
        {
            "id": "8b1d746b-4cca-4b5c-8d65-3e65d8bdd453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 55
        },
        {
            "id": "0c528a7a-a1ee-4e08-8b0b-96049dc2d184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 47
        },
        {
            "id": "04c19130-f4d7-45e7-be84-33317f9c65fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 55
        },
        {
            "id": "bd8bedfd-71cf-4e79-a498-50147c34dae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 86
        },
        {
            "id": "774eb3c4-8459-478d-8d5d-4e4d4365a529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 88
        },
        {
            "id": "dc2398a2-b026-49e4-a8ab-8f61d46dd3f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 118
        },
        {
            "id": "4cf55917-81bd-4b68-ab1d-318689999d2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 120
        },
        {
            "id": "b50db0f6-00e9-4c13-945d-5245dc499827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 44
        },
        {
            "id": "9f2d4a11-38c6-4410-8ff7-ed4daaffa255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 45
        },
        {
            "id": "7b4e8fca-9d58-4ba9-8454-f15090c00dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 46
        },
        {
            "id": "5362fbb8-e4f2-4ad8-8863-2f0dda21c84e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 50
        },
        {
            "id": "e9ba64e6-c55e-49f4-b03f-f16d6dd4b8ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 8211
        },
        {
            "id": "4d1d850a-1787-401c-bd1e-1622992d9048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 8212
        },
        {
            "id": "818c68db-a6f7-4df6-a02d-4792ce7db19c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 8218
        },
        {
            "id": "abfec562-9daf-49b4-a383-6a922b7b30d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 8222
        },
        {
            "id": "102ecf87-98e1-4c70-bcec-f252758b026a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 8230
        },
        {
            "id": "4ac1c219-8c0f-4058-b3cb-3a2fa152635f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 47
        },
        {
            "id": "ada2b9bf-057d-4509-ab0c-c89c4759b745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 55
        },
        {
            "id": "58822d5f-4ff2-49d5-9f22-2ae59ca7fb72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 86
        },
        {
            "id": "4ea28131-a3f3-4783-88c0-b3723ba9de8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 88
        },
        {
            "id": "2f8a8fa0-b862-4e5f-be5d-5d9a109facf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 118
        },
        {
            "id": "9570851e-0812-49a3-88a3-7f226ebaf2aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 120
        },
        {
            "id": "fb94f293-aa25-4fd0-8292-c08b9e6b8c71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 47
        },
        {
            "id": "282492e7-79b1-42e9-aa60-80829d7cacc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 55
        },
        {
            "id": "1c949b21-accf-4476-b3ed-ad1240d58d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 86
        },
        {
            "id": "6530fad2-3d07-4b50-9fd6-1c08e2e85e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 88
        },
        {
            "id": "a12e5654-a996-4049-8de5-3b6ca312bd0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 118
        },
        {
            "id": "8ba57317-6fe0-463c-8a15-ef53014a4b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 120
        },
        {
            "id": "c7db1f52-6c2f-4194-a0cc-92d794966e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "94a73007-fe66-439c-9264-a535cfe2d2f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 118
        },
        {
            "id": "03f3a1fd-66ed-4f91-b19a-7eb1e6dc716a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 47
        },
        {
            "id": "480445f6-f10b-464d-bba9-9561b7b904c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 86
        },
        {
            "id": "29b33d78-b616-4c6d-9079-a28d9e8a8bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 88
        },
        {
            "id": "208ec1db-6b33-49e5-94c5-e654b4cca452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 118
        },
        {
            "id": "73cae94f-aec6-4874-8353-de9093884ceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 120
        },
        {
            "id": "3e496199-a8c8-45f8-a08b-e2fb8f6f5b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 45
        },
        {
            "id": "d52127bd-c86f-4c0b-afa1-09571124b0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 8211
        },
        {
            "id": "6edf76cb-03ff-42f1-a6ff-f39756c556d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 8212
        },
        {
            "id": "5dfd3fbb-86d9-4044-b1cd-9db55e82b2a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 68,
            "second": 47
        },
        {
            "id": "73bef71e-a5f6-4b56-a171-287c4d6b091e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 86
        },
        {
            "id": "c943c5da-3c9d-4d10-bfbb-ccad0aed3fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 88
        },
        {
            "id": "0ffab447-8ea4-4819-8f5d-a84e2633b290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 118
        },
        {
            "id": "f6df8d27-7e9a-4abe-960a-4c8db86ab789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 120
        },
        {
            "id": "75dd10e5-8654-4316-b0dd-d21bf6fa9527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 45
        },
        {
            "id": "0c1d33a4-ce29-4438-bca3-a50d9b7a3a5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 8211
        },
        {
            "id": "1a81f4d1-dac7-45ad-a672-f74ed59ab47c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 69,
            "second": 8212
        },
        {
            "id": "89fa317b-f727-4e51-af72-2b1cc2ae639a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 44
        },
        {
            "id": "e7c0e1bb-a0df-4f15-bf95-fb88ed3d2ec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 45
        },
        {
            "id": "e5910e47-d8d9-4b71-aed7-2a5d09cdd401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 46
        },
        {
            "id": "1a898197-0062-405a-b7e0-1251e60f9229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 47
        },
        {
            "id": "abd47d6f-659a-4fc1-a56a-1be24cad81f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "01943d01-0aae-489f-b4e2-f7bd9b56823a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 74
        },
        {
            "id": "954d9b1a-08b2-4548-8551-97454efdc631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "11f25568-5692-4114-abab-eae69985d04e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 106
        },
        {
            "id": "899ccca7-8475-4e81-9a0d-44104d5536df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "17c6b266-b5bd-418a-95a6-77f4a01c971b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "6fe2d989-72f8-429d-a982-904d664a8134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "5448421a-1534-4e92-b1a1-822ef0470f01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "17c1bc74-106b-4a9b-88b9-8b2112202338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "5e67c0c8-0186-43a3-9f3c-7399d9538951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "94d30e16-c610-43eb-87ce-641cedb03363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "63b91e71-4868-42c5-93b2-781dfad162fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 224
        },
        {
            "id": "da42581e-a834-411c-acf5-2b2242577290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 225
        },
        {
            "id": "2cc10e2a-21c5-41b9-a7d4-3d03a76825c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 226
        },
        {
            "id": "9a23a385-a7d3-4fb9-8f52-5970d196041d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 227
        },
        {
            "id": "a68ecc10-8bcd-486f-9008-6ed409a5077f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 228
        },
        {
            "id": "28963a6b-a73f-4047-8fe6-6002934d6c13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 230
        },
        {
            "id": "a790f6b7-b44a-4a88-92ee-909444a72e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "1352892c-12fe-42e0-938d-b24491c86875",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 257
        },
        {
            "id": "b833c0ed-dfa8-4ede-ba9b-a52c85e4fe31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "abd2d6a6-e28f-47e1-a36f-2057f2dd0942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 259
        },
        {
            "id": "74a18647-4519-4794-8e9c-f9cefbde3529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "11bb9051-099c-4a40-b9ff-cd6875e2101f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 261
        },
        {
            "id": "4ac57e28-ce4d-47e5-8891-872c8f618ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 8211
        },
        {
            "id": "5ece00c0-443e-4c85-8e73-542a234ee9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 8212
        },
        {
            "id": "91a93ff7-450b-4830-a37f-5159aacd7782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 8218
        },
        {
            "id": "4f1b8287-f934-47bb-89ed-3063f989ad86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 8222
        },
        {
            "id": "07fa4846-8735-44f1-bcbc-52752f1c10a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 8230
        },
        {
            "id": "ef70c7b4-ed70-4c7d-9e0b-170f6158bb52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "e444215f-1b8e-4347-906a-c989be61c2df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "ed6411f3-ebe4-492f-aaec-214cf5a32f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 74,
            "second": 47
        },
        {
            "id": "83c0bd4c-ce82-4c69-8a70-f5cabe80b0de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "841726f5-fcec-44cc-93fe-e0c7d005f3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 88
        },
        {
            "id": "380631c9-4a97-4bd9-9a8b-66302ab24a5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 106
        },
        {
            "id": "cc57fec8-5096-4024-98e3-1605117b6ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 120
        },
        {
            "id": "7cc90db6-681d-45ce-9fb1-35896edf7c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8218
        },
        {
            "id": "f644dc34-47a4-4bcd-a336-01349d57f82f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8222
        },
        {
            "id": "3a2207fe-68fc-4d9b-8f49-26e291927301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8230
        },
        {
            "id": "7c6cdb9d-1527-447c-b5cc-b45b079a7f5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 38
        },
        {
            "id": "6e54c484-c024-48f7-bee2-dd2a9b6e443a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "6e52ef5b-a419-49d6-a7cb-3aec776f8904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 45
        },
        {
            "id": "5b77e0fe-809b-4599-93eb-da0c26ceae0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 46
        },
        {
            "id": "0dfbf0e1-17f2-49ee-8c3d-4543e9d70c4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 48
        },
        {
            "id": "d776f155-01dd-4cec-9f7d-22b99a10507e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 54
        },
        {
            "id": "751286ac-bf83-43de-a9ee-d7c003ec15a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 56
        },
        {
            "id": "d9b35b1d-443a-4982-8d90-92f296dce789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 57
        },
        {
            "id": "d98fd281-2e8e-4bf1-b85a-e6630f7a367f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 67
        },
        {
            "id": "0e1991df-cc4b-4d95-aaf8-abb28c7684b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 71
        },
        {
            "id": "2774ee77-e415-4788-ada3-e382294f15f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 79
        },
        {
            "id": "00585200-f270-40e1-9230-a3a4be1a968e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 81
        },
        {
            "id": "369706f8-4bbd-47ae-9821-0016c49149da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 99
        },
        {
            "id": "060baab5-a701-4daa-a13c-78e72f713178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 103
        },
        {
            "id": "84426fb3-4b43-4fe5-86b6-3ed4e61c42d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 111
        },
        {
            "id": "ff63cf0b-b33b-409a-9497-08b60dd8c5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 113
        },
        {
            "id": "1cb8e818-8fd4-4956-9880-23fd17ae85cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 199
        },
        {
            "id": "e41b2234-61b0-492f-b806-0c2e674ba44f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 210
        },
        {
            "id": "60a5a88f-7a95-4404-a234-682be60532ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 211
        },
        {
            "id": "09ac6d14-5a64-4c51-82f1-2d0bfeda77d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 212
        },
        {
            "id": "5afa6685-e914-408a-82e3-02b628bf1f4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 213
        },
        {
            "id": "bbd362d4-6196-4a3c-b408-49e01a6caf98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 214
        },
        {
            "id": "faa3a681-f0b8-42a0-bb8d-139d568062ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 216
        },
        {
            "id": "5929bbea-0db1-4a12-93d2-5917ece22737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 231
        },
        {
            "id": "d0febd37-55c2-4d6f-873d-67038d61ec07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 242
        },
        {
            "id": "11e18e05-7a91-4a49-bfc0-6c1d0f091764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 243
        },
        {
            "id": "91decd73-f391-4e06-9bee-e585cb914b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 244
        },
        {
            "id": "440dd075-8901-4eb3-8452-10730f3faef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 245
        },
        {
            "id": "77365b4a-0e76-431d-a408-320af615b9bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 246
        },
        {
            "id": "82b3c710-73b1-4aeb-a14c-8882f29bdbe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 248
        },
        {
            "id": "b411e3d9-d83e-4407-bc86-14dff1107e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 262
        },
        {
            "id": "ccf3f37c-42e9-4733-bd14-772ed637aeee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 263
        },
        {
            "id": "d0c87510-e483-4a78-bae6-80a70b328ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 266
        },
        {
            "id": "a68d7dd1-8a3e-4f17-bc96-d6edb0220a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 267
        },
        {
            "id": "dd8c3459-7c9a-4d8e-831e-d8ecd9db46a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 268
        },
        {
            "id": "bb6cece0-b3d1-428e-95e1-96e3025799aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 269
        },
        {
            "id": "1e3cdec5-ddbc-449d-8172-e5fbd0e02757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 287
        },
        {
            "id": "2cd90805-2ffd-4d2c-8e31-0bae6cf9f98d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 288
        },
        {
            "id": "37c09ca4-98e5-49e7-a352-62dbb720a223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 289
        },
        {
            "id": "2e0ae327-63f3-4fc4-bc61-393c739f115e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 290
        },
        {
            "id": "35abe7fa-f098-4a6c-aa63-21e95abf08f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 291
        },
        {
            "id": "9f4d5547-8d17-4592-baad-6a1499749e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 332
        },
        {
            "id": "4dbc3230-52ac-4219-8eab-cc84b65efd9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 333
        },
        {
            "id": "74ff4866-abbf-427a-bd16-aa2a42e46da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 334
        },
        {
            "id": "9115b324-9329-4968-8861-2aed2e201392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 335
        },
        {
            "id": "d1a0d569-d8bf-48df-ba11-b9c7a8837cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 336
        },
        {
            "id": "267c6da2-f316-411a-97af-658cc4244280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 337
        },
        {
            "id": "e32a6b4a-ecca-4462-9412-836895ca253f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 338
        },
        {
            "id": "7cb8e310-7bfc-40f2-b278-8a3658ad1f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 339
        },
        {
            "id": "d93a3bba-f196-441f-b304-dbd556b49933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 8211
        },
        {
            "id": "b975fb08-8602-4ec5-9cf7-aab99f67a77f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 75,
            "second": 8212
        },
        {
            "id": "e99ff61f-a218-4d19-a04c-2968a60f5756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8218
        },
        {
            "id": "095b4f6a-8905-4ffe-9006-afdf5d6e8ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8222
        },
        {
            "id": "aac4c890-481a-466d-a0cb-575b21a49c8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8230
        },
        {
            "id": "eed3a6a8-b579-458c-b4a6-1e7baf373c96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 34
        },
        {
            "id": "80fe0739-acb7-4cb5-ab62-fb2f5d485acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 39
        },
        {
            "id": "d7d13ef9-c1af-4090-9a0c-98c411e50cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 45
        },
        {
            "id": "3c0edc15-f33c-4655-983c-590081082f00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 63
        },
        {
            "id": "ab623dbd-c415-4838-b0e7-7f2dc832ddbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 84
        },
        {
            "id": "516eb91b-4b4b-49b9-9021-b9b02f30c964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 86
        },
        {
            "id": "01f1a871-3848-46a2-8ec0-f6702ef5834e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 89
        },
        {
            "id": "05d58244-00d6-43cf-ae36-d4bdfdad2f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 116
        },
        {
            "id": "422fac95-9eac-41cd-945c-85fa619e4712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 118
        },
        {
            "id": "674be228-c338-45be-b052-f487b2d880dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 121
        },
        {
            "id": "081ce13f-7ee4-4912-b74b-4c6960a315f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 354
        },
        {
            "id": "5c6a827a-9e5a-4f8f-8976-ef544cc2fb3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 355
        },
        {
            "id": "9918541d-0777-4665-b3b2-5388f705deff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 356
        },
        {
            "id": "ddb74019-d789-464a-ad5d-e889954fc147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 357
        },
        {
            "id": "b2acded0-9ab6-4e59-a7b2-57c42f9ca3f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 358
        },
        {
            "id": "74143fca-fa36-49ce-9e10-a939515e8935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 359
        },
        {
            "id": "03bd952e-ea50-4a7c-bb67-ddad09a6320c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 538
        },
        {
            "id": "2deabd47-db74-47c5-82fa-56f331f9cb1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 539
        },
        {
            "id": "39050c99-2665-4dc7-9416-9b88f5604dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 8211
        },
        {
            "id": "f94fb612-dc8f-499a-b512-ccf76029f1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 8212
        },
        {
            "id": "1f6b20c8-eed4-4149-a1b2-525b352bbc26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 8216
        },
        {
            "id": "34163ff4-f4aa-41b4-905e-01bd2338ce63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 8217
        },
        {
            "id": "15f11768-c33f-47c1-97c5-09423a47d937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 8220
        },
        {
            "id": "1da2de5b-5e2b-426e-9f13-106399e92bed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 8221
        },
        {
            "id": "d0acd2bb-e281-483e-9fb1-a28db4b8e17c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 86
        },
        {
            "id": "fda505ec-9a18-45de-badb-cbbdb95394e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 118
        },
        {
            "id": "74a52e68-a0a0-444c-b6c9-28a799e92ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 47
        },
        {
            "id": "ef623afe-f692-4b37-8ca9-a6bd91029640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 79,
            "second": 47
        },
        {
            "id": "8b2c77b8-8700-46cb-8ff0-f9b64674a5d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 55
        },
        {
            "id": "0d13a6b1-f6e2-452d-b8eb-8d6f6914aebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 86
        },
        {
            "id": "5a4d2822-44c0-403e-9607-cf38fd9a40fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 88
        },
        {
            "id": "f2ba95f5-92c5-4114-9743-3de347a8b232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 118
        },
        {
            "id": "11e530c4-a4be-44bb-8735-836d4c0b7520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 120
        },
        {
            "id": "b8c8a9a7-e7d2-4e46-80db-83965bc82c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 44
        },
        {
            "id": "f2c01f85-01be-4e8f-a3cf-13c62fcde486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 46
        },
        {
            "id": "a585814f-f211-44d7-a907-49623cb2dc78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 47
        },
        {
            "id": "11dec81c-2756-4135-b5a6-9acfdab31352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 74
        },
        {
            "id": "aca92acd-fdc5-43da-9828-c8fb3f6ded60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 106
        },
        {
            "id": "da947f1d-7fa7-426d-803f-9093114664cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 8218
        },
        {
            "id": "feda7dc8-b708-413f-9d9e-3a5ba469abf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 8222
        },
        {
            "id": "130c4e1a-ceba-43ae-8749-47fcd065ab39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 8230
        },
        {
            "id": "68550ed7-e6a7-4625-848b-8a961d9cd407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 47
        },
        {
            "id": "ebf2bf5e-1979-43c9-b607-0cbd4284cf78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 55
        },
        {
            "id": "53cc9d25-3e20-4dcb-bae3-4b7e88e0c9e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 86
        },
        {
            "id": "6618f2d3-c999-4a79-b7e3-7c6e142b2f2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 88
        },
        {
            "id": "f008964f-74d7-4782-b3fe-fbf1ac48b8a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 81,
            "second": 118
        },
        {
            "id": "4e26657c-1be8-4c1d-894b-9bd55649acd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 120
        },
        {
            "id": "1345655e-c4b8-457b-9f6d-b4359144b3bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 48
        },
        {
            "id": "7f24701a-ba1c-4bb3-bf4e-aa77881e6c84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 54
        },
        {
            "id": "a7cdcda2-103e-4c65-9035-7c92cef289d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 56
        },
        {
            "id": "68c60823-8ae1-4507-a027-dc4b14678314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 57
        },
        {
            "id": "c750a661-dd5e-4b09-9179-7988d29822b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 67
        },
        {
            "id": "03d6ae29-3061-474a-867c-8435f646c9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 71
        },
        {
            "id": "ea0e4614-b613-400a-a22c-f2e7e91f5370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 79
        },
        {
            "id": "d54de634-7631-4b96-a6b6-0e4d769e5abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 81
        },
        {
            "id": "5e8fac8e-f031-4a3a-bbfb-e446137fdd50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "32a2e34e-cc18-4cfc-bcec-62eb15387f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "544bca4b-a1b2-41aa-bc5a-9208d48cf639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 99
        },
        {
            "id": "18206290-70f6-47a4-856b-89f2b7a00d76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "f1881f2c-387f-4b53-a5ba-246788c4d852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "77310720-b08c-404b-99e9-0bbb441c0c85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "2d6eb77b-ceb1-4bdf-9c70-8dde7a862d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 116
        },
        {
            "id": "4139e867-2857-463b-922f-dbc6333f3ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 118
        },
        {
            "id": "e2d38db8-2059-42c7-be5a-b9b5baecd5ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 199
        },
        {
            "id": "3f49488a-ce53-4d83-bcd0-a2b9520d8b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 210
        },
        {
            "id": "9500a934-0a90-4cf6-9164-1f1702130424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 211
        },
        {
            "id": "c2943b2a-5a00-45d3-9995-3a098ed5446c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 212
        },
        {
            "id": "2478fe94-56b6-4c4f-9c15-573da2a32c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 213
        },
        {
            "id": "f6abf66e-0213-4c82-abb0-80559bcb323c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 214
        },
        {
            "id": "a4d93408-e585-4dbf-9ee8-da75c924d76c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 216
        },
        {
            "id": "073bd812-ce38-4c90-8b87-c01660f80068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 231
        },
        {
            "id": "8b23f34f-2bb7-4f69-a4a7-cbb0f8c9ea1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 242
        },
        {
            "id": "f90c5469-4634-497f-9f08-08b5f740309b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 243
        },
        {
            "id": "34c298b1-0f9e-49eb-a0a8-9089b0ac5077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 244
        },
        {
            "id": "0ef00a41-2347-4d6d-8687-622bc592f192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 245
        },
        {
            "id": "993b35cb-f93c-435c-a94e-5a46824fb3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 246
        },
        {
            "id": "a90b169f-ebcf-4abd-86d8-522b252d5136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 248
        },
        {
            "id": "360e9f01-3969-4d57-a99c-95c4d9dd8cfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 262
        },
        {
            "id": "acccce4e-de91-4b0c-ac67-568d10c5bb13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 263
        },
        {
            "id": "23e194d8-912c-4606-afb6-691852a7115c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 266
        },
        {
            "id": "ba29061f-6e49-4287-a641-a4cf83ed8710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 267
        },
        {
            "id": "03ca128c-991a-426b-8dc9-a90852109461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 268
        },
        {
            "id": "fe8995fe-a34b-4c9c-9111-170853758a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 269
        },
        {
            "id": "f03e057e-3a52-4747-b7b1-011310334560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 287
        },
        {
            "id": "515fedcb-af32-4f30-945d-47b9ee837397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 288
        },
        {
            "id": "7a8cb62f-9a39-4f05-aca6-c80b1222c75f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 289
        },
        {
            "id": "bd0de336-d41a-4c61-9c86-616eb16dfceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 290
        },
        {
            "id": "78668ec6-8a7f-45b7-870b-92cdd881928d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 291
        },
        {
            "id": "daabe041-24cf-4b91-bbcf-62ebe1258201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 332
        },
        {
            "id": "45683e52-14c9-4c85-aa50-10dc1896b647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 333
        },
        {
            "id": "7105c743-6468-4cfa-9697-0dd452d788ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 334
        },
        {
            "id": "1351d0a6-0f5a-4193-aa87-bf7ad69ee1f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 335
        },
        {
            "id": "1111d1b5-b602-4231-be61-32efc042e57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 336
        },
        {
            "id": "ad09297a-8519-4ab0-8474-c292b9d6ae62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 337
        },
        {
            "id": "5f929534-6271-4ff0-919f-dd793172f340",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 338
        },
        {
            "id": "27cfcd0d-0cc0-4913-9c43-d83c23ad9547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 339
        },
        {
            "id": "1b774667-abe7-4110-84f4-3f1e5effdbfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 354
        },
        {
            "id": "dcf629ed-3732-4d85-b57f-f6ed52d3ed4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 355
        },
        {
            "id": "a6b543d7-cb29-4e95-8e31-16442021743e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 356
        },
        {
            "id": "aefdae31-5e8b-46d3-82c8-a57b53444b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 357
        },
        {
            "id": "87946e0b-973f-4b9a-ab00-ea503c2ba5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 358
        },
        {
            "id": "0cf9f06c-bbbf-4b22-983f-4b80a9d5ac5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 359
        },
        {
            "id": "da0d9ea8-f8fe-4d07-9aec-f899059c53c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 538
        },
        {
            "id": "ac64cc9c-9321-44bb-9508-baee91716ea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 539
        },
        {
            "id": "5592ecb8-898e-453d-92ab-9caf51a57632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 47
        },
        {
            "id": "03f69edd-5c62-4b0d-8d01-bcf1accf5d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "de1095c2-fa1d-4bad-b91a-cafe8c13cfc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 45
        },
        {
            "id": "d4d488f0-015b-4586-84e7-3e93f81b6abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "e18146e1-b0ec-44f2-b827-279ced9705d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 47
        },
        {
            "id": "9a3f8d16-76ce-4998-be29-b7f6b9dc5807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 74
        },
        {
            "id": "a4dbf686-4bc3-4015-9b2b-2c783883b613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 86
        },
        {
            "id": "31496c69-8fe7-48e7-8113-c52f92c708b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 106
        },
        {
            "id": "6439cc88-2293-42a4-a657-0ab41084a110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 118
        },
        {
            "id": "c6980fb7-377b-4ec9-8e9e-249de1d2d0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 8211
        },
        {
            "id": "e7dc46ca-a85a-4901-9cb7-0a7a7c9bb252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 8212
        },
        {
            "id": "58c42084-2dd8-43bc-ad61-9d1670350e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8218
        },
        {
            "id": "a4573889-f025-42cb-ae91-783027041f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8222
        },
        {
            "id": "329dfa19-1d8e-42bc-b933-b3a167425788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8230
        },
        {
            "id": "407a7118-5372-463d-90c2-1f4054e74e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 47
        },
        {
            "id": "27ad33bb-53a6-4d75-b486-0aca2948cc30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "8bec7e6e-8c00-49b0-b887-08b47c77ad31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "e5111840-6990-41c3-9b8d-3495289ffebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 106
        },
        {
            "id": "3a8f91ca-6c2a-40f8-b170-fb65b4757ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 120
        },
        {
            "id": "49351cfa-6997-481f-b9fd-1f6997db643e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 38
        },
        {
            "id": "15edcb21-90c6-465c-b586-7158a6f4c037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 44
        },
        {
            "id": "1f59701b-b47d-4365-aba0-aaf255306660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 45
        },
        {
            "id": "3fe85f42-d961-49bc-b9eb-e89cd1767097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 46
        },
        {
            "id": "c7610d7c-e99a-4034-b32b-455843ea13d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 47
        },
        {
            "id": "358041d8-74da-4520-baf2-c9dadc5d965d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 48
        },
        {
            "id": "6142b4cb-5b0b-4081-8a7a-e38232fa7589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 54
        },
        {
            "id": "7f3dea9b-8486-4216-8744-43d7727886da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 56
        },
        {
            "id": "49cdaf21-fb58-45c8-b444-24040379db1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 57
        },
        {
            "id": "3c587d73-ae06-463e-b4a3-b80ca8acf7c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "f455692b-404a-4cc3-845f-76fad1014fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 67
        },
        {
            "id": "f8534a67-6ae6-45c0-8070-ee8d9f8d725d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 71
        },
        {
            "id": "50061549-fc72-483c-b6ca-064e4dfbcb5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 74
        },
        {
            "id": "e0f0ad5f-a99e-4854-9937-02a6f6ec5bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 77
        },
        {
            "id": "13adc39d-d872-4101-9f44-809a505f82c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 78
        },
        {
            "id": "2c45f199-70e1-491f-804e-e4952c7f07dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 79
        },
        {
            "id": "6c3b610c-cfa9-4091-92cf-f37c4a0b32bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 81
        },
        {
            "id": "faa410e4-b1eb-421f-82f4-6782df17971a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 84
        },
        {
            "id": "054707e3-d503-4937-991a-10b0dc07d08b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 86
        },
        {
            "id": "92113824-51ce-45bf-846e-5fc8b26aed6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "1e45a95a-6a88-45da-b147-9f1c7eb88f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 99
        },
        {
            "id": "d0969c99-75d5-42ce-9da4-1d1767b1aa6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 103
        },
        {
            "id": "340d80df-fe1c-442e-930c-de7bff8b1010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 106
        },
        {
            "id": "37063852-0a7b-4aa5-8b66-aeb70ed7c802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "768b300a-a274-4bc8-a07f-7d8ad20e73c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "9335955d-d67f-4434-8364-56f4a409085a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "badc3196-6fc3-4e9d-badf-a60e8cf32bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 113
        },
        {
            "id": "df1fbfe1-de39-4207-9194-2845ff73efc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 116
        },
        {
            "id": "91ccae57-3296-447f-ad7a-b1e11b1beb1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 118
        },
        {
            "id": "e5171f20-f5eb-45ca-af15-c86023783274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 192
        },
        {
            "id": "45cf4825-81d4-4f0b-9d86-9b27a7be959c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 193
        },
        {
            "id": "88d4ada0-4af9-4d3d-8361-00394f773b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 194
        },
        {
            "id": "f86ddeb3-a414-4766-8077-20e785c83000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 195
        },
        {
            "id": "b8615835-451f-44e7-849a-c168939e4273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 196
        },
        {
            "id": "0f9dd5e5-854d-4e87-b36b-3478b091960a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 197
        },
        {
            "id": "561903dd-1ed5-4bd0-8a0b-d29920c4a973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 198
        },
        {
            "id": "5651b030-6d7c-4683-ba7e-12979f3137eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 199
        },
        {
            "id": "f4a9de37-86af-41a8-8155-f1706658e722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 209
        },
        {
            "id": "a941c5d9-6194-4d8f-acbb-c461ccbe04f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 210
        },
        {
            "id": "422f7b99-a73e-4546-a170-de2d4e5f3cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 211
        },
        {
            "id": "b691ce6c-0d96-4d58-a4ec-664f4c5fc69c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 212
        },
        {
            "id": "149fa944-d5b3-4f41-a9ea-7dc4ad29a9e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 213
        },
        {
            "id": "0ece11cc-4d6a-41f8-9176-2ffd69c1656c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 214
        },
        {
            "id": "2817a79f-27a1-4a97-88a5-5d108789e83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 216
        },
        {
            "id": "01bc414d-210f-4fcd-82c2-81dbf273b772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 224
        },
        {
            "id": "d358301a-affd-462d-8ef5-a9eb40040d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 225
        },
        {
            "id": "923e044e-69a2-4873-9b43-e5cbdd597975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 226
        },
        {
            "id": "4b28413b-8a72-443f-bda7-3a3e5270dfc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 227
        },
        {
            "id": "450ce6ac-ac6a-4446-b719-010dc2580e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 228
        },
        {
            "id": "3dd153ce-ab5d-415f-8737-cb3d67ba070b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 230
        },
        {
            "id": "df60bfa2-a5b4-4d69-a817-f05ecb66c2db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 231
        },
        {
            "id": "90f0a09c-a557-416e-97e6-de6c73d56035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 241
        },
        {
            "id": "9f6673f5-8a98-4e4a-b05f-a36e627f4bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 242
        },
        {
            "id": "b9b28226-1042-42a0-82ff-57fa97bf242a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 243
        },
        {
            "id": "de6a3522-62d0-4ea6-989e-4118cbcfda26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 244
        },
        {
            "id": "624623f7-97bc-4335-9d9d-8a3f0a6caab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 245
        },
        {
            "id": "e669a3bf-a16c-47df-8ac6-793ecd7431cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 246
        },
        {
            "id": "613ef173-02b1-4a1a-a58a-09731798cbc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 248
        },
        {
            "id": "1775b5fd-b680-4f1c-b483-51ffcfaaa2f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 256
        },
        {
            "id": "0a55375d-dde9-4553-8c22-0b40641a2a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 257
        },
        {
            "id": "3b6721b2-5a09-4c7f-89e0-2bf77f3003ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 258
        },
        {
            "id": "ec17dad2-111d-4832-940a-565de3a1c924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 259
        },
        {
            "id": "1c80cef0-9cd9-4acc-a7f6-b2ff5b02d68f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 260
        },
        {
            "id": "9232e227-dff4-4341-9214-92dccdb88335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 261
        },
        {
            "id": "dfd33aad-6641-4c54-ad6f-cd86f05c4607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 262
        },
        {
            "id": "359716a8-c80e-4e21-96d4-164d6779cd3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 263
        },
        {
            "id": "2ae58e75-1695-438e-956f-12d6a6efe048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 266
        },
        {
            "id": "1aa6bd25-eae8-471c-8f3a-e2bd98a5b443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 267
        },
        {
            "id": "b8cec153-6bf0-4faa-809e-2f11d47063d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 268
        },
        {
            "id": "b22463df-1d2f-44ff-9350-44f327d33425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 269
        },
        {
            "id": "65f71659-8e49-44bb-be77-2599be8bffb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 287
        },
        {
            "id": "ef41e3da-073c-4e78-91d4-45a2bb7ce9fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 288
        },
        {
            "id": "0854bf5c-25cb-4d15-8ecf-1222fbdbd214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 289
        },
        {
            "id": "a8bdc3a4-fd29-4dc9-8a64-7ef1f0ef9c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 290
        },
        {
            "id": "adb62fc3-6cca-4825-b5e7-b1ec50c633a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 291
        },
        {
            "id": "f71aafea-534a-4ac5-a80a-0b7be7990842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 323
        },
        {
            "id": "292d630a-1b8a-4ce5-850a-095d1ea50e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 324
        },
        {
            "id": "0a141246-b2cd-409a-9e29-e956416cfee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 325
        },
        {
            "id": "507563a2-c73e-4baf-ad2e-ff70282d7ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 326
        },
        {
            "id": "8f4fc623-1b5b-463b-bb0e-d70b41bfd402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 327
        },
        {
            "id": "f201c0d0-90a6-4ec7-bfc3-ca3dbb94a422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 328
        },
        {
            "id": "0877fbb1-09fc-4510-a838-f5a1c8579b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 332
        },
        {
            "id": "0be599f0-154e-42cb-8f12-ee02dc357281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 333
        },
        {
            "id": "6324f27e-5d66-46f7-ab41-7a84911b80a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 334
        },
        {
            "id": "d63008a2-83b1-4436-8bd3-d30af8d13486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 335
        },
        {
            "id": "a7bee471-5c2f-40c2-97f7-10daa1a90c1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 336
        },
        {
            "id": "5d81df8b-bdb5-44c6-8f9e-96cc8a41dffa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 337
        },
        {
            "id": "c38e5d01-3a4b-47e0-b28f-cde6d38530ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 338
        },
        {
            "id": "6f84d313-3269-4a11-bb5d-66aa8cb89b1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 339
        },
        {
            "id": "3848cce0-84cb-4296-af40-384650b2256b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 354
        },
        {
            "id": "ca0bf947-19c3-467d-aca9-8acab3d2921c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 355
        },
        {
            "id": "2fee1db9-7f49-4575-a1ef-a9c71478d168",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 356
        },
        {
            "id": "1c8f807e-086a-412d-ae46-1cabed184580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 357
        },
        {
            "id": "be641930-d01b-45b8-8083-e23816e61b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 358
        },
        {
            "id": "90fcfa4f-8eca-40e5-af63-37072d4e3744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 359
        },
        {
            "id": "7fd34bd3-7aa7-4a7f-8f99-88a2cc13261c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 538
        },
        {
            "id": "28d660c7-2710-439f-bdb1-24d35fb73951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 539
        },
        {
            "id": "99be028b-d82f-4b5e-9e27-d91c31ee5a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8211
        },
        {
            "id": "7f87fa0c-1505-4dae-8e63-cc648955ee9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8212
        },
        {
            "id": "dd62e746-0b4c-4592-8bd0-31556b11bcfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 8218
        },
        {
            "id": "eed98277-8fb5-411c-8280-51318a30b924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 8222
        },
        {
            "id": "f2f19161-6a33-4db7-a7f4-cb26bd1c55d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 8230
        },
        {
            "id": "be412432-4198-4388-b2cd-31ba1afd1a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 47
        },
        {
            "id": "edf78805-677a-44e0-999c-1fd03ca1a356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "8ea28a12-7080-4625-a19e-abb2a7337339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 88
        },
        {
            "id": "b88631a9-46b7-463a-9b73-7cfd4a025953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 106
        },
        {
            "id": "a644b85b-837f-4d84-9c18-171a73acec5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 120
        },
        {
            "id": "36c5b704-ece5-4e1c-9791-019c727a9078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 38
        },
        {
            "id": "b5f81c56-5e94-4abb-a172-233bb54abd0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 45
        },
        {
            "id": "c93474b9-708c-4f89-9840-fb678c8f501b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 48
        },
        {
            "id": "50d11036-1336-4853-805a-b5f41c3956cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 54
        },
        {
            "id": "de6d5dbc-5ba7-402e-a967-7969a65fd584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 56
        },
        {
            "id": "00a9aa82-8511-4000-ba4f-2e14b5c64f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 57
        },
        {
            "id": "d578a400-2897-4e1c-8524-6df36f9d44b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 67
        },
        {
            "id": "46a587d9-80f6-49b7-b12f-c3340bc0e626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 71
        },
        {
            "id": "a95b9d2f-f61e-4423-9546-0c6b9209dda2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 79
        },
        {
            "id": "2f93e26b-c255-45b7-ae0a-b867ea38dd2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 81
        },
        {
            "id": "700046ef-d517-430d-9691-0666e696b648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 85
        },
        {
            "id": "d912da10-0a93-43cb-a7ca-138a52dc2cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 87
        },
        {
            "id": "349ee5c7-a418-412d-8cea-fb80516e0db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 99
        },
        {
            "id": "ce94fd0d-4699-4877-9038-bee88ca7d5d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 103
        },
        {
            "id": "a34936e5-4edb-4bef-89d3-8aa253aeff8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 111
        },
        {
            "id": "4204207f-a750-4ab7-8f7d-c9432c3c5d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 113
        },
        {
            "id": "cd7dfed5-5936-4e02-93b0-14cea1d1cf14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 117
        },
        {
            "id": "096f6271-1c0d-41ab-a072-2f23b506c171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 199
        },
        {
            "id": "972aac06-6aea-4892-8a14-239d89b74d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 210
        },
        {
            "id": "3617369b-4215-419b-8314-8766ea82668e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 211
        },
        {
            "id": "eab009f8-27f7-4ea3-ab82-b0cc80be7fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 212
        },
        {
            "id": "8c430d3a-478a-42cd-8668-d0f3656936c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 213
        },
        {
            "id": "6a4386a3-3944-41a9-8147-d81e71f65840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 214
        },
        {
            "id": "ceb02bf6-0aa6-4648-b385-5fb4299f2a19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 216
        },
        {
            "id": "7670f232-57c7-4636-92d4-e5cbc314319c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 217
        },
        {
            "id": "8323d819-b628-44dc-b342-e25f65be3c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 218
        },
        {
            "id": "4db01b4a-b4a7-4061-b1db-0d657a7ffee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 219
        },
        {
            "id": "3afb1df5-f01c-4cc3-bf50-1f2cf79dcb95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 220
        },
        {
            "id": "fe755620-a198-4d6f-a0f8-fb33cde2668f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 231
        },
        {
            "id": "a2297604-b53e-419a-a81f-d6d85157f2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 242
        },
        {
            "id": "0402c590-74c6-4257-bf4a-231f9b1f606d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 243
        },
        {
            "id": "f0f04712-ab9c-4fdb-b353-9b546edcc815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 244
        },
        {
            "id": "420d87dd-b382-416d-93d1-ff5c0e0fd17c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 245
        },
        {
            "id": "cec10a6d-32f1-4066-a2b9-61084d0165f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 246
        },
        {
            "id": "2427761a-2605-4283-a1f8-6b69029ecff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 248
        },
        {
            "id": "17c99020-5205-41d3-a2ab-5da024a0af2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 249
        },
        {
            "id": "350a9f92-b47f-4a51-9bfb-55f57014185d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 250
        },
        {
            "id": "714aa85d-8ad5-4297-9528-78f6aac6c562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 251
        },
        {
            "id": "7778972b-9c45-4a7d-b4d1-59aa77fb2100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 252
        },
        {
            "id": "ca70108c-3c35-4ff5-96c3-d96eaaa0f303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 262
        },
        {
            "id": "483f3cea-5005-47a6-b75e-4dd025a4a33e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 263
        },
        {
            "id": "ef43b710-cb35-4764-b15d-67caf78bb0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 266
        },
        {
            "id": "38d7d63f-4f71-4e54-81d1-ed3bb7c209da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 267
        },
        {
            "id": "50534ac6-4838-4d97-b48a-2e25b2c6692b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 268
        },
        {
            "id": "390e141e-0364-46eb-8029-4f7cfe3a0bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 269
        },
        {
            "id": "3f624c0e-8287-40a4-aeff-a60353953d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 287
        },
        {
            "id": "dbb91a78-1786-4212-908a-178924c9b202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 288
        },
        {
            "id": "91945784-8256-4a93-95ff-19440300ea84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 289
        },
        {
            "id": "d7cc0f64-3bd8-4263-97b9-21b088b3abff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 290
        },
        {
            "id": "27347224-e00b-459f-b392-3c12eef415a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 291
        },
        {
            "id": "fb347f6f-9543-4902-970b-01369df05036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 332
        },
        {
            "id": "a5d5111b-a4dd-446a-b3a7-0d6aed5999c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 333
        },
        {
            "id": "5e3250c0-1f47-4140-9118-383ba8e16446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 334
        },
        {
            "id": "7a4faa3e-0cdc-44f2-b06b-d2510beb6887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 335
        },
        {
            "id": "d4e01895-8b89-4dff-b86a-e1d9e6fd75ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 336
        },
        {
            "id": "03108cbb-2a6e-4345-b711-05981cedf15b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 337
        },
        {
            "id": "bec1a4d8-eea2-4789-abe6-8828e2445169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 338
        },
        {
            "id": "3287a6a9-9b9f-40d1-b792-2047ca975c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 339
        },
        {
            "id": "d19ec0bd-e6dc-4f07-9d7d-e8e6c7b35310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 360
        },
        {
            "id": "2c5d3911-bf73-451b-901b-bc006ce616ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 361
        },
        {
            "id": "187ec577-866f-4382-a503-4edef201569e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 362
        },
        {
            "id": "b48377f6-da01-432e-8951-4fbc0308b43a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 363
        },
        {
            "id": "f028382e-93b2-481b-b565-0b0cbe023fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 364
        },
        {
            "id": "cb608fcb-2146-4143-9324-83c8a3bc1a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 365
        },
        {
            "id": "908683eb-e40d-4050-ac27-1a01b160a1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 366
        },
        {
            "id": "5bcc06ba-289c-423e-ab53-5d29b10d87b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 367
        },
        {
            "id": "af321b33-785a-44af-8475-0e7dbc1e8c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 368
        },
        {
            "id": "f62feca2-38d2-4def-8bc6-d1570554e3c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 369
        },
        {
            "id": "76c4fa25-b373-46b4-b8ab-2f8c3f6ca157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 370
        },
        {
            "id": "fa016e1c-2dde-4e2b-bf8c-71bf7b649e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 371
        },
        {
            "id": "e259d19b-5098-49be-b4be-e24331e0c59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 372
        },
        {
            "id": "0342a344-3fda-48a9-8f46-cbb6b431d3d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 373
        },
        {
            "id": "1c090dcb-9b48-4bc7-843c-39aaea31cf93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7808
        },
        {
            "id": "fcb8605a-0191-4a20-89c8-fca3a493a5aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7809
        },
        {
            "id": "1ba09696-e828-47e0-a058-156bf510cdf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7810
        },
        {
            "id": "e265f264-0962-47f1-a9cd-7cec9219c33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7811
        },
        {
            "id": "c9c056fd-c2ef-4557-aa5c-3ef744dab7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7812
        },
        {
            "id": "3fea4089-eba2-41bf-8157-be1f207e98dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7813
        },
        {
            "id": "b2dbfa20-93c9-4cf9-9694-0b10ad3ab9e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 8211
        },
        {
            "id": "f6d4ad38-3acb-4fe8-bbfa-c2cb5590618e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 8212
        },
        {
            "id": "ecaeba5d-0af7-4965-a8fb-dc340f51e76f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 47
        },
        {
            "id": "c9e1b1f6-8d44-4b65-941a-75cc6acbdbb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "1678ffce-ab45-4c3d-ab22-f698499e50e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 88
        },
        {
            "id": "b44b77f2-a8a0-4fd7-b1f9-58fda50d03d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "3a500d26-df8a-43c9-96e4-bd13c444f8a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 120
        },
        {
            "id": "0ccb013a-a177-4426-ae71-9862ba2b8c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 45
        },
        {
            "id": "680f807f-1b95-4c41-8829-b72e989674ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 89
        },
        {
            "id": "d670c9d8-824c-48fa-9071-de6e51e006f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 121
        },
        {
            "id": "cc7d12ed-cb03-4dfc-8ddc-a5596f3091b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 8211
        },
        {
            "id": "4ae5043d-448f-41ed-bb6d-d72705dc5937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 90,
            "second": 8212
        },
        {
            "id": "dc1dafb4-f41e-46cd-9aca-1f710679e013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 34
        },
        {
            "id": "283e1981-ff05-4f5c-bb20-be1c462e92bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 39
        },
        {
            "id": "a72f2363-507f-4f7f-b7ab-ec9b3e2657a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 84
        },
        {
            "id": "ddb072da-51f9-4f1c-8c66-376ce4bb0581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 86
        },
        {
            "id": "6fa7a33e-cf85-4e9c-aa38-38307c89c73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 116
        },
        {
            "id": "d97f4364-d4a4-440f-be64-d79eb053e088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 118
        },
        {
            "id": "35eae5bf-15e9-4cbe-836c-eb8cdd2b3edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 354
        },
        {
            "id": "dfd5ca4d-8e25-49f7-a078-5cdc83ca9c99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 355
        },
        {
            "id": "53a5fe94-b838-44af-b16a-8a05fbb3682a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 356
        },
        {
            "id": "f9445274-c20c-474e-9cc3-5ca57bd111f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 357
        },
        {
            "id": "b53dcbdf-2422-41cb-9fea-e4a64d58adbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 358
        },
        {
            "id": "f9bd3a33-f0ca-405e-a1f1-2fad30b9d790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 359
        },
        {
            "id": "c74a9a52-0b98-4081-88e3-ea79a02faba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 538
        },
        {
            "id": "0a76209f-155c-44cc-8bee-2669f9dc0e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 539
        },
        {
            "id": "59a27d13-f9b9-4422-93a0-981385637dec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8216
        },
        {
            "id": "3c5fd1a5-4383-4d47-9d02-3530dd35a5a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8217
        },
        {
            "id": "55366905-e8a6-437c-b94e-9bb4cabc7259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8220
        },
        {
            "id": "95d77448-50a0-41bc-9b94-309ae19bd11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8221
        },
        {
            "id": "c3fb37a5-b01f-4ada-ba74-b3177befc422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 44
        },
        {
            "id": "87bc4ff0-de41-422a-bc3a-c27e2d593b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 46
        },
        {
            "id": "112bc20b-2262-4bc5-aba2-ace84a4296f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 47
        },
        {
            "id": "78ea47f9-1157-4401-bceb-1d605afb763d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 86
        },
        {
            "id": "e9a82879-62d5-4c10-8ec6-52d33b43fbd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 88
        },
        {
            "id": "da000d24-88bf-4069-8fec-8312bd6507e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 118
        },
        {
            "id": "c1077f1f-5fde-4f6f-8eea-270df9dfe9cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 120
        },
        {
            "id": "af142109-9b4b-4c20-bee3-9b5afd3f0d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8218
        },
        {
            "id": "fa343f25-195a-49a9-b179-b5355d13fbe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8222
        },
        {
            "id": "079f6d6f-e835-4926-b477-c8b54d8a8da7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8230
        },
        {
            "id": "2b32d4c3-22c4-4fd9-bb16-7795c84bf8ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 45
        },
        {
            "id": "aa17a4d0-e84e-4f44-a626-31b2ad1c6069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 8211
        },
        {
            "id": "3ad88d45-78c4-4a76-a229-dd48f606c6e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 99,
            "second": 8212
        },
        {
            "id": "b593e342-7e0e-47fe-aa7e-3b8f2e1d5cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 44
        },
        {
            "id": "b685d674-1192-4a54-8e6c-afcbb5e1afd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 46
        },
        {
            "id": "a58e0d53-2ef4-4012-baac-efd57b38fe43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 100,
            "second": 47
        },
        {
            "id": "75cf17bb-d2af-465a-8a26-971d02e5b213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 86
        },
        {
            "id": "eb97918b-77fe-42d4-8ba5-3a7aec81adb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 88
        },
        {
            "id": "3875b14c-c4cf-4ac6-a67a-af766a330d08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 118
        },
        {
            "id": "ca8e0d3a-9d46-496f-a77b-f594998b5a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 120
        },
        {
            "id": "8e2bea5e-a5ea-49e9-ac8e-251df67240d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 8218
        },
        {
            "id": "0f111feb-31bb-4b95-a44e-fa916bff382c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 8222
        },
        {
            "id": "4b7c2d34-9e82-45ca-a86d-ae0cc37e9c8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 8230
        },
        {
            "id": "14fd37ac-d7ee-4029-b893-3df809551399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 45
        },
        {
            "id": "49e30c18-e8bc-497e-83ec-c472a081818b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 8211
        },
        {
            "id": "358d7630-063c-40bc-b76e-d1d0c011d448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 8212
        },
        {
            "id": "a8d9ba42-7659-4228-9e76-46c32b55f361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 102,
            "second": 44
        },
        {
            "id": "4f11594b-1537-4022-a6d5-b2cd6c1c84a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 45
        },
        {
            "id": "bf743194-22f3-47f7-82ac-d3dd81c1ae06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 102,
            "second": 46
        },
        {
            "id": "e9b91479-ca50-4e3e-a37f-b0b7d2b6a7f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 102,
            "second": 47
        },
        {
            "id": "1badcfc1-cfe7-4ae2-8b66-863d941458b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 65
        },
        {
            "id": "7221bfe6-ee0d-4d4d-806f-1a4ce47acc16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 102,
            "second": 74
        },
        {
            "id": "47aa9db9-5327-4375-8138-1712eb4e0bc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 86
        },
        {
            "id": "cfd97e1e-f54b-4dbf-927f-26485e313770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 97
        },
        {
            "id": "637d0a3e-6175-4d70-a695-21ce9fda5189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 102,
            "second": 106
        },
        {
            "id": "478ed6a4-8949-4ba0-8ed0-3e9b49dfdf80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 118
        },
        {
            "id": "22bb8d71-32eb-4587-94d9-a31ed77b9f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 192
        },
        {
            "id": "c899ebef-37ad-4a05-9349-8d47505528b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 193
        },
        {
            "id": "70401e52-5e3b-4c69-a43d-ae3b7ac72201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 194
        },
        {
            "id": "758d5dd3-b6d0-4232-a768-526c99380f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 195
        },
        {
            "id": "eb15bc58-21da-426a-9270-76bad08c818f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 196
        },
        {
            "id": "4a95fc69-02b2-4fcd-a881-d22c7e000eff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 197
        },
        {
            "id": "e8932dba-3511-43dd-b2c7-76d3d3169565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 198
        },
        {
            "id": "1f1db8c0-1e36-4822-801b-957fa447a667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 224
        },
        {
            "id": "f86f226e-ec83-4bcf-9a71-5d9e2a170d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 225
        },
        {
            "id": "27fbb2de-3aa0-49b3-94f5-a99a62957762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 226
        },
        {
            "id": "0a9b63e8-2ee0-45b5-9671-2549ff2f7ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 227
        },
        {
            "id": "c097f98d-539f-4647-8155-3b7a7697535d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 228
        },
        {
            "id": "04859357-c42b-4b09-8fd3-879dfbe16a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 230
        },
        {
            "id": "a803ed91-0bb4-445f-9525-b6ffcedb3a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 256
        },
        {
            "id": "e2a19289-a288-4073-9a74-5cf2a4359538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 257
        },
        {
            "id": "3220a72c-ff34-4d89-b9ea-b9dac0196775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 258
        },
        {
            "id": "6c7b27ea-2b80-497c-b685-147fa642ba10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 259
        },
        {
            "id": "fe5db775-d47f-4f42-a1df-00fd310bf1b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 260
        },
        {
            "id": "191fbb11-2593-4d71-94c1-2e27b06f2a18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 261
        },
        {
            "id": "63110202-db3e-48ae-b967-db38d9fb6ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 8211
        },
        {
            "id": "4db6c81e-3df8-43bc-bbd1-7095823a2473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 102,
            "second": 8212
        },
        {
            "id": "009b79b5-469c-4238-bacf-b794838f0356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 102,
            "second": 8218
        },
        {
            "id": "045f0937-7da8-43bf-9abb-5bd208036245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 102,
            "second": 8222
        },
        {
            "id": "cd19a367-0cae-41c4-8945-eba2312e690e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 102,
            "second": 8230
        },
        {
            "id": "72259491-bbaf-4dcc-940b-fb205ff69c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 86
        },
        {
            "id": "baf9814a-5acf-4657-a4fa-aa5bdd954dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 118
        },
        {
            "id": "7fa97aa1-e20c-4f91-a2f1-e29c5e015097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 44
        },
        {
            "id": "62f0b21a-9c03-4d7c-9b54-80c007ca32bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 46
        },
        {
            "id": "cc1a9bb7-4bcb-4df4-80d3-ae8761187cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 106,
            "second": 47
        },
        {
            "id": "b32dbeef-08bf-4235-9966-a9f737e5fb1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 74
        },
        {
            "id": "01035d3e-b00c-4cdf-8720-5001456e3720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 88
        },
        {
            "id": "fbd3d6a2-dbc9-4ac6-a28e-f0d04d2f2814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 106
        },
        {
            "id": "53b47406-e896-468c-8dac-7dcdb9274a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 120
        },
        {
            "id": "e510c6cd-e2ba-4c5e-b68e-ba6cddb631a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 8218
        },
        {
            "id": "acf32ccb-af56-4c1e-b052-e3ed4d427882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 8222
        },
        {
            "id": "66cbc0a2-0397-4c60-98a8-46b1fdf6f9eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 8230
        },
        {
            "id": "ffd9d99f-07e5-4694-aed3-90921d940e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 38
        },
        {
            "id": "a2c29c6d-d5aa-40cc-9837-c92240b82c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 44
        },
        {
            "id": "326ac071-23a1-4df3-8183-b9470ac5b83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 107,
            "second": 45
        },
        {
            "id": "e53a2e8f-a5ce-4929-98e5-1d8a852d9603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 46
        },
        {
            "id": "ede7e5a6-6d61-427d-8704-17f1c367e5db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 48
        },
        {
            "id": "6c5c745f-0b0a-4493-8cc6-a491b679ceee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 54
        },
        {
            "id": "a72c8e7e-7554-47fd-aff5-5422f62d9c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 56
        },
        {
            "id": "15d221d1-0fce-4661-bab1-89c55c434372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 57
        },
        {
            "id": "48dac501-b7e1-42f8-8716-3e06de81a09d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 67
        },
        {
            "id": "56cbc646-51be-44f5-8419-abbb2df499ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 71
        },
        {
            "id": "c978a7f5-05ba-44d0-b565-40b1e74e477e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 79
        },
        {
            "id": "c00628ef-9cd7-43f8-a3b2-7f7dff5681b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 81
        },
        {
            "id": "304118fd-b883-4196-a8a9-3cc664a10299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 99
        },
        {
            "id": "6f11bb5c-771f-4dca-962e-02306b9ca092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 103
        },
        {
            "id": "775ed3cc-4495-46e4-9005-fa49a03001f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 111
        },
        {
            "id": "9c901430-3d6c-4e9b-b01a-8a8e893355ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 113
        },
        {
            "id": "55a5e51b-35dd-46ed-a026-98705c051b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 199
        },
        {
            "id": "835b7cd2-d829-455a-99f5-22a2ca4d31a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 210
        },
        {
            "id": "817bb209-c144-4647-9d29-8e94f21ecba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 211
        },
        {
            "id": "df62cf7e-306a-49d4-9f5e-c3f67828ba08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 212
        },
        {
            "id": "29c4eaff-5f41-4f66-a8f1-ce263a154ae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 213
        },
        {
            "id": "ccfa089a-b84a-435f-9594-471fdf555ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 214
        },
        {
            "id": "d11a2908-18fe-4e48-b849-fb8f6114677a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 216
        },
        {
            "id": "4ce469c3-cd71-493a-9a5f-9db312ed6cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 231
        },
        {
            "id": "6ecf8b8a-f8b8-41cb-9f54-9d6bbc9bcfc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 242
        },
        {
            "id": "40ded2db-6649-45fa-94b0-9d2b9e042ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 243
        },
        {
            "id": "a0679788-1de2-4634-889f-71077498f889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 244
        },
        {
            "id": "a8c24e47-2c11-471f-a625-0f54cb790106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 245
        },
        {
            "id": "b51ff899-3679-424d-a579-37c9609bd2c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 246
        },
        {
            "id": "0dcd0864-c3f5-4167-ad3c-f705baba6361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 248
        },
        {
            "id": "05e47789-dbed-42aa-af5b-61e69db658db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 262
        },
        {
            "id": "9613cb04-8665-4d39-9d5f-e8cbf9b63ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 263
        },
        {
            "id": "fe6c8494-c721-4c11-b6d2-18e0878ee191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 266
        },
        {
            "id": "6b3a12bc-cf13-407f-b9d6-d5d5849b3b5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 267
        },
        {
            "id": "59b1bad9-e1fd-486c-8279-52dfcb4c5830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 268
        },
        {
            "id": "ed9d26f8-f754-4cfa-a2f9-cc3e5b1f1a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 269
        },
        {
            "id": "d733de87-b3cb-4938-8179-fa121d82a810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 287
        },
        {
            "id": "c4927fb0-f1e4-4d61-9690-786629b7c4ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 288
        },
        {
            "id": "d233f239-2151-49d5-931d-1b65fbe2d597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 289
        },
        {
            "id": "fb508291-9bf8-4d8e-8ae8-fa1df25402e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 290
        },
        {
            "id": "ee5fc930-8e52-413e-9080-02608b56d18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 291
        },
        {
            "id": "ddd8b886-5b33-4131-9aae-b04b10a7ca70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 332
        },
        {
            "id": "580f184e-0eb0-44c7-b60e-a664da330574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 333
        },
        {
            "id": "4ec199ed-7ec0-4386-9b2b-adf30fa82c53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 334
        },
        {
            "id": "a40046a7-33bd-47c8-9aaf-4d1cdbcd5410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 335
        },
        {
            "id": "d0229ba3-651f-4bf9-a59b-8555fb4f4166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 336
        },
        {
            "id": "618dd258-4138-4db2-9cc7-410f6c2386f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 337
        },
        {
            "id": "01161f29-24ea-49c9-9cec-d57fc1a132b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 338
        },
        {
            "id": "13828b15-b833-4ec2-a5d3-193295f2a00e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 339
        },
        {
            "id": "e09546bc-884c-4eaf-9f33-34cc8acd3a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 107,
            "second": 8211
        },
        {
            "id": "555432bd-84a3-446a-b8ce-d5902ad12300",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 107,
            "second": 8212
        },
        {
            "id": "e0642b9a-54a6-49cb-b040-d00602a82e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8218
        },
        {
            "id": "3aff0d45-b61a-4cb4-8b76-11ac4564d33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8222
        },
        {
            "id": "eb0cfc02-4f86-4db9-aad3-77022edc152c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8230
        },
        {
            "id": "43e1effa-3c6c-43ec-a8a8-9c315ab261f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 34
        },
        {
            "id": "d6b23fba-fad4-4a06-a07f-c5ec22de23c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 38
        },
        {
            "id": "f4bd9e08-f76d-42db-880d-0ab63e946072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 39
        },
        {
            "id": "ea93143e-5509-4cb6-9657-9f67875e5622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 108,
            "second": 45
        },
        {
            "id": "2a4c5e46-914f-43d8-b20b-36e8110edb2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 108,
            "second": 63
        },
        {
            "id": "35633057-49d1-44a4-bdbb-02e6c5b84e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 84
        },
        {
            "id": "06aaec52-2e66-457a-b8f9-9ba65b1d29c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 86
        },
        {
            "id": "0eba8c8c-8674-4190-9ebd-2e5137f82ab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 89
        },
        {
            "id": "f187bbad-beb6-448c-a175-bdb692df94d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 116
        },
        {
            "id": "8547ba1c-7e76-4ed9-a2c2-506e3705a05d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 118
        },
        {
            "id": "9222f7c8-a1f7-4f40-86ab-8492570a5219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 121
        },
        {
            "id": "1a367652-475c-47e5-8363-53cdfb9893c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 354
        },
        {
            "id": "3ca8b826-3f95-4b74-aa9d-fb2eff971f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 355
        },
        {
            "id": "e8dd7bd9-d6c3-42c1-bf01-a23889976ceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 356
        },
        {
            "id": "393dec8f-5ae9-475e-a4f5-28982e04c9cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 357
        },
        {
            "id": "5093252b-a7e7-4f7f-a9ff-6f3063bd27bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 358
        },
        {
            "id": "c7966b60-ee56-4def-8bbe-59377f3d2c79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 359
        },
        {
            "id": "b6bf15bf-2b6c-4c59-9ab0-7fd5aa73ca25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 538
        },
        {
            "id": "d60918c2-b548-4e37-812c-361e30c521dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 539
        },
        {
            "id": "40a05e59-1ef0-4b63-af5f-cb0331577c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 108,
            "second": 8211
        },
        {
            "id": "5c04dcbb-0138-4677-a891-426fcdc63825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 108,
            "second": 8212
        },
        {
            "id": "f4eeaf71-8551-44ee-bc0d-f79225f04c4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 8216
        },
        {
            "id": "90c9edd4-7bd8-4005-9212-73978102d65a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 8217
        },
        {
            "id": "081cac42-a483-485c-a7f4-ce51a4f7bb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 8220
        },
        {
            "id": "36649d34-544e-471b-8513-ce646a734750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 8221
        },
        {
            "id": "08422471-86fb-48a2-9fc0-1895793dd314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 86
        },
        {
            "id": "f3317938-3e2b-4dbf-a822-1919a8c3ac44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 118
        },
        {
            "id": "fb05b827-4df6-4d78-94e0-3564e4e1bea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 47
        },
        {
            "id": "4564636d-09f5-49ab-ab1f-756e3e1ccb79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 44
        },
        {
            "id": "f067ef32-72bc-4a90-bf8c-8bcb336fb0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 46
        },
        {
            "id": "bec71d6f-8a99-4580-b892-2a610464c9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 111,
            "second": 47
        },
        {
            "id": "281d7b58-d8f9-4534-8d55-963dd457538c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 55
        },
        {
            "id": "037340be-0e4e-4d9f-9400-3ed632bf6fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 86
        },
        {
            "id": "ecd0733a-2bc9-46c1-b62e-8e7493daed81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 88
        },
        {
            "id": "51d11147-cee0-4292-8555-160a76254c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 118
        },
        {
            "id": "bfe7ceab-d0bb-43db-8445-788587cfb3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 120
        },
        {
            "id": "f0f663e2-c00f-47cb-8577-9ec2a7d9a873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8218
        },
        {
            "id": "91612d72-a0bf-4ad4-a566-f0888793ad37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8222
        },
        {
            "id": "d04bdf2b-ed42-40bb-bc26-b7415d8c25c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8230
        },
        {
            "id": "c345e55c-8e66-49fb-8d8d-0228463a9ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 112,
            "second": 44
        },
        {
            "id": "15c9164c-e52e-4ab7-a216-c7d1ba9afe0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 112,
            "second": 46
        },
        {
            "id": "11d032a8-fefa-4d59-8025-bb31b96564d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 112,
            "second": 47
        },
        {
            "id": "631257db-73b5-4fa9-aa36-253f7e38c0d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 112,
            "second": 74
        },
        {
            "id": "36338fc0-3cef-4b9b-8f56-c5ea3dd4073f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 112,
            "second": 106
        },
        {
            "id": "1e86e09f-1d4d-45fe-854a-d7ed9e8bb4fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 112,
            "second": 8218
        },
        {
            "id": "e42c94dc-f46d-4451-970a-35451e2f0faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 112,
            "second": 8222
        },
        {
            "id": "3ba7b459-a493-495e-b3ee-993780201dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 112,
            "second": 8230
        },
        {
            "id": "259f37e5-4471-4922-9fee-277aa89607aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 44
        },
        {
            "id": "d92432c7-ca2b-4c5f-8eb6-6596319d2c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 46
        },
        {
            "id": "9d064cca-41db-4519-95b1-4de1b7d10bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 47
        },
        {
            "id": "0802f9dc-42ad-4702-b8fd-0ea6d90f6c49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 55
        },
        {
            "id": "52248076-300f-4bab-ab1f-dc49f050f501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 113,
            "second": 86
        },
        {
            "id": "35548d0c-7aa4-403b-b944-90aceda8d52a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 88
        },
        {
            "id": "dc7f09d5-3065-4c6f-9023-607fc55b142b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 113,
            "second": 118
        },
        {
            "id": "8ec400ab-e2a3-4218-8324-08805a1924dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 120
        },
        {
            "id": "07c8cdb2-732d-4828-8503-424e031a2054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 8218
        },
        {
            "id": "a2ce0d9d-79f0-47b0-94af-015308dba4ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 8222
        },
        {
            "id": "d7e96c26-6aff-4486-b4e7-84a5e6163b83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 8230
        },
        {
            "id": "da6c2113-3d5e-42bf-9be7-676f3356e534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 48
        },
        {
            "id": "96692ecb-cfe4-45f4-a166-c7b8fd22ed50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 54
        },
        {
            "id": "d0d515f0-ea8f-4e63-be92-0b9a7fd0a367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 56
        },
        {
            "id": "04811fba-4b8e-488a-a6aa-9231b16e8d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 57
        },
        {
            "id": "67872f0b-eedb-4cb4-9f26-bfc3a2ac8345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 67
        },
        {
            "id": "df3ff363-f4bc-454d-a35d-92a165d64624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 71
        },
        {
            "id": "e470aa77-8253-4ea8-b126-d96e2f5d7ea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 79
        },
        {
            "id": "4bab591b-e356-41ae-9ba9-c124d942cc29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 81
        },
        {
            "id": "4b744b27-1ce2-424b-978e-612ebc2599c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 84
        },
        {
            "id": "39162d98-e2a2-4177-bcef-dd9dbca35374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 86
        },
        {
            "id": "2a73058c-0975-4b0d-9955-6ef92ac7c63f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "e7d0a693-c96e-4eb6-91a6-e8ec9f697de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "a71ec2d2-599c-4c27-856f-6c7bcf7f3c85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 111
        },
        {
            "id": "34cb13b5-d505-451e-aeb5-95b76a85691e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 113
        },
        {
            "id": "2a1e567c-d09a-4b58-8bd5-611009f89844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 116
        },
        {
            "id": "e7375f9a-9150-407e-98b8-1c0d18f02b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 118
        },
        {
            "id": "16f723d4-fb83-4b85-a7ef-3426c4c28db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 199
        },
        {
            "id": "5f0dbde6-9a37-438c-9c4b-347bbfff15d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 210
        },
        {
            "id": "e38a2e50-642c-4e6b-b3b8-eddf836b86ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 211
        },
        {
            "id": "56b99757-24d3-4555-a28e-06fe995d4352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 212
        },
        {
            "id": "1255e13b-85af-4217-a200-f72a3afc3bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 213
        },
        {
            "id": "eec13a97-bb95-4d43-b154-d603454ea8ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 214
        },
        {
            "id": "297e3e9e-7029-450d-aadf-ebe89c76d6b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 216
        },
        {
            "id": "070b177e-d12e-4b17-a157-512e7b4b2064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 231
        },
        {
            "id": "ca03e304-e54d-4960-9984-dc6bf097a48d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 242
        },
        {
            "id": "e440fefb-bef4-4475-8d56-b08859aaf5d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 243
        },
        {
            "id": "b205a03c-8d0e-4d13-869d-d835ce97e40e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 244
        },
        {
            "id": "b9aad217-1d2b-4c1f-b0aa-5137d9359085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 245
        },
        {
            "id": "0b369cbe-5b7b-4b45-97c5-e868b6d8a4b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 246
        },
        {
            "id": "5e0f9c3e-0dcb-4c89-82b2-dbd0f7075420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 248
        },
        {
            "id": "57f92b81-db91-44ef-bb2b-3fc2c9d64dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 262
        },
        {
            "id": "30684c2e-1705-4894-a920-3e5288baf932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 263
        },
        {
            "id": "11df537d-78c1-48dd-9328-3e1f663b9297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 266
        },
        {
            "id": "59af5427-1cd3-4acb-be78-84dde2466409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 267
        },
        {
            "id": "f5dac228-4260-4c59-993a-d34fabf0ca2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 268
        },
        {
            "id": "e0235e6c-db2c-4eb8-b7de-a7e85dff3f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 269
        },
        {
            "id": "47e6c24a-2aea-4290-aa3c-cdf455db592b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 287
        },
        {
            "id": "183eb376-d310-4092-b378-c084ece0abce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 288
        },
        {
            "id": "2513b9e7-c71c-4c04-89c2-fe321c894e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 289
        },
        {
            "id": "1aff75c8-682c-4dfa-acb2-1d4bd8f8b376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 290
        },
        {
            "id": "222cc4c2-e2a1-4564-b3e8-8f539ec24fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 291
        },
        {
            "id": "6a37395a-c453-4f7f-9673-eb9a4ffea6f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 332
        },
        {
            "id": "a2983081-1de5-424b-99cd-27c60d97cabe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 333
        },
        {
            "id": "d70eed6a-ce4f-4af6-b2d4-d193e85b9e04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 334
        },
        {
            "id": "1b5fe5a1-ed5c-458d-a7dc-20286b0b840f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 335
        },
        {
            "id": "e6d3d1de-783d-46b3-9785-3dbbd2e16049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 336
        },
        {
            "id": "ae585498-d4a6-4be5-9b3f-09a04a4d40dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 337
        },
        {
            "id": "af481522-ea70-48ea-a186-537da4de24f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 338
        },
        {
            "id": "0fbf04bf-e19d-42c5-b00e-b0dac6d4e97f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 339
        },
        {
            "id": "1151b20e-1bf2-4960-8d56-5e03c9d9353b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 354
        },
        {
            "id": "b651ec72-8f10-4d42-aceb-860ca2cd6fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 355
        },
        {
            "id": "674855db-abbf-4bc9-acd3-eae80f073302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 356
        },
        {
            "id": "85ddb3e9-0028-4e6d-9635-6aa482ef072b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 357
        },
        {
            "id": "65c5dffd-fdea-4950-ae31-4cb2f146e7c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 358
        },
        {
            "id": "f6651439-0ca9-485f-9795-5ed8a5b24118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 359
        },
        {
            "id": "b0c5cc3f-4044-4628-b276-588f915958ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 538
        },
        {
            "id": "3bf09f6b-5a42-45c0-ad61-6a3788ce9b62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 539
        },
        {
            "id": "94b1baa4-8dc9-4523-bc80-1f44226b2212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 47
        },
        {
            "id": "0c10ed26-4aa6-4275-ad10-f85630f2e1b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 34
        },
        {
            "id": "a558dfe4-d091-460d-ba96-44e7eab6ebd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 39
        },
        {
            "id": "22daa7c6-5762-4a27-8910-88ff59f7f7e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 44
        },
        {
            "id": "249077d0-e415-46f7-a421-fa15d91b882b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 45
        },
        {
            "id": "1e318063-8599-42f4-bc37-ab07222cf55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 46
        },
        {
            "id": "5bc4c172-f505-4564-ab22-b1ec51088609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 116,
            "second": 47
        },
        {
            "id": "050f9b89-8804-45c6-8cce-d3531c817c8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 65
        },
        {
            "id": "14b6404f-d1ae-4735-9c64-e9fd19d9ebce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 116,
            "second": 74
        },
        {
            "id": "4ea5515c-b564-45bb-9e7e-ed9ffe86f7a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 84
        },
        {
            "id": "101bd2d3-aee4-44be-90cc-e69441671458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 86
        },
        {
            "id": "f47d08af-fdb2-46dd-b708-a08748cb2bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 97
        },
        {
            "id": "95793b0a-d8e8-4b58-ac09-b6d05b4f0a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 116,
            "second": 106
        },
        {
            "id": "3cc99c06-6c95-43e7-8b92-256a053c3b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 116
        },
        {
            "id": "463fcf56-57ab-4492-8433-ed66820d5de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 118
        },
        {
            "id": "accb05b8-cf87-4b44-9119-402a261ed1e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 192
        },
        {
            "id": "efb32c86-9208-457d-a448-7c9390fc6f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 193
        },
        {
            "id": "aae46e5b-fec1-4773-9332-2b5b1e2ba51f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 8211
        },
        {
            "id": "9629cb8c-f94a-4866-9a10-06d63e85bbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 116,
            "second": 8212
        },
        {
            "id": "41ce1846-dcab-4d4f-bc44-ca18d3e8604f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 8218
        },
        {
            "id": "bf2031c6-d126-4bb8-ab0e-c7bd6ef04f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 8222
        },
        {
            "id": "b32ef35e-66cf-4a8b-9b9e-953a16e3538a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 116,
            "second": 8230
        },
        {
            "id": "932200b1-0d4e-42e8-83f1-92badebb8519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 117,
            "second": 47
        },
        {
            "id": "ca870ee7-389c-436a-b109-0aca1b66c3f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 74
        },
        {
            "id": "eab998da-708a-4861-862e-fc641f67b27c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 88
        },
        {
            "id": "63e6c000-81da-4f0d-964f-506464d10528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 106
        },
        {
            "id": "215768a7-070e-46db-aec7-6c00c2d498e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 120
        },
        {
            "id": "75a0d6e7-ca47-4cab-9999-5d2e859e8831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 38
        },
        {
            "id": "21aa8908-f2a7-4fb0-a8e4-9575a96eecdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 44
        },
        {
            "id": "4371df93-6efe-4d2c-8057-3cc655650317",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 45
        },
        {
            "id": "7b5cdea2-1607-45cd-ac58-25b87d621d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 46
        },
        {
            "id": "c70793e2-6f6a-4336-bb50-3cf640cea6e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 118,
            "second": 47
        },
        {
            "id": "5bad5f28-dfa0-4290-8d12-f5484f8f1a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 48
        },
        {
            "id": "37e34bb1-5edf-4687-8c5d-842901bbf5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 54
        },
        {
            "id": "032e3e6e-d3d7-48e2-8cd0-b508c4317c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 56
        },
        {
            "id": "ec20f5c9-9c11-4c01-96ca-71f47d8d4027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 57
        },
        {
            "id": "a0cb86aa-317c-4fd5-9716-95a4e2ddec86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 65
        },
        {
            "id": "b04847ce-4e2a-4bdc-8742-65c6c095a22a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 67
        },
        {
            "id": "692decfb-3027-45bb-a0f2-567ca8b64fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 71
        },
        {
            "id": "d1aef6e9-788a-4abe-b5f0-c647275af819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 118,
            "second": 74
        },
        {
            "id": "b7673884-12fd-4856-94b0-7fe21ee2a9ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 77
        },
        {
            "id": "54d23455-bc78-4eef-afbf-c3ed4e7cdbc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 78
        },
        {
            "id": "8266db63-f7a9-47ac-9d40-ca44031a7f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 79
        },
        {
            "id": "8c99e05c-1040-4ded-b9bf-3a9d486d22a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 81
        },
        {
            "id": "2e418f80-f0dc-423e-ad62-b7979c3b7bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 84
        },
        {
            "id": "2cc91d96-e70d-4010-8185-a1a766d98b0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 86
        },
        {
            "id": "b7fc6de7-8ac5-41cf-8f78-740a50511c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 97
        },
        {
            "id": "e9ae66d3-27b0-4fd8-af69-a771841bf461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 99
        },
        {
            "id": "e9627f5d-09c7-4b88-947a-53c0cf97f672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 103
        },
        {
            "id": "6861b1c5-99cb-4b19-ab4b-cc8828f70860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 118,
            "second": 106
        },
        {
            "id": "74a4276b-0b65-457a-ba86-b4e197c92e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 109
        },
        {
            "id": "302411a6-1666-4e11-b4e3-e02171aab9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 110
        },
        {
            "id": "b3dfdd26-c67e-40b9-8900-4bee082e140b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 111
        },
        {
            "id": "74626ea2-68bc-47bf-bbe5-7cd5bbff01c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 113
        },
        {
            "id": "23f6fed7-7890-42c4-a210-7a24b29b2734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 116
        },
        {
            "id": "8868b9d2-490f-45e9-abc1-d721c8e8a9aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 118
        },
        {
            "id": "fc6e93d1-e7ea-4a65-8b3f-9a1bf29f9491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 192
        },
        {
            "id": "73fd9f3d-9841-435b-bd3e-8c8c9e6e39ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 193
        },
        {
            "id": "749783a1-1db2-40ef-ac89-7f5a76e73625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 194
        },
        {
            "id": "f3c22e79-806b-4644-8506-97610ca5fb8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 195
        },
        {
            "id": "e69da7f3-874d-492e-9947-8bdb0c2d812a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 196
        },
        {
            "id": "7ae42596-d050-40c1-9481-fb7a466ddb53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 197
        },
        {
            "id": "1dad0070-1110-4ea7-b1b5-288aa9feada4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 198
        },
        {
            "id": "7c658e4d-5d33-4fd5-9f76-963ec9fa0d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 199
        },
        {
            "id": "6fc0811f-3611-478d-a6c0-f07e2c7bafd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 209
        },
        {
            "id": "bdef27af-c62f-406c-95e6-40c7877d1ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 210
        },
        {
            "id": "f57019c5-a315-4800-a053-d9a410102eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 211
        },
        {
            "id": "ab5fb723-fa41-483c-a5ca-28a74c202cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 212
        },
        {
            "id": "bc74c59a-c124-4cf5-aebc-203e8b7c3ce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 213
        },
        {
            "id": "aa4939a4-5a0b-47b7-92ba-82ae132f5664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 214
        },
        {
            "id": "fad7ff4e-98d6-4ee1-9f0d-120e6de92a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 216
        },
        {
            "id": "470d041f-120e-4a62-be69-60d93348a079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 224
        },
        {
            "id": "6629ade6-b33d-423a-a609-1b1606ce4302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 225
        },
        {
            "id": "a867e4fd-be80-40ec-b23c-3b0b994b87fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 226
        },
        {
            "id": "95fce4a5-dc83-4299-90e0-177b222f6dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 227
        },
        {
            "id": "eac17302-92bd-44df-be19-ae44e8bb1f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 228
        },
        {
            "id": "d2acddf1-a2c0-43be-b809-fd1e780a7cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 230
        },
        {
            "id": "a5e3878a-7912-4886-aa39-53cf516c9813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 231
        },
        {
            "id": "7c8b2804-32e0-4268-a2a1-52cf917b33f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 241
        },
        {
            "id": "de3b9d71-c691-469a-b5c2-9717788d0cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 242
        },
        {
            "id": "724e2300-b359-40ac-8f2b-3a06b869c9d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 243
        },
        {
            "id": "e038d567-9347-4c2d-8a8c-8252fc5a2197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 244
        },
        {
            "id": "5801ee4e-7bbd-4f30-bb8e-f5a4bef50b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 245
        },
        {
            "id": "95292b68-a1ce-40db-8113-545e4c89d47f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 246
        },
        {
            "id": "203c2222-7585-4856-be09-9fd80f04e345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 248
        },
        {
            "id": "cee41277-118a-4869-9905-50fba9130131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 256
        },
        {
            "id": "b193e213-e705-4f76-bbcc-6d96561f7e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 257
        },
        {
            "id": "faa7bf29-15c9-4501-9c90-f848bd6e4a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 258
        },
        {
            "id": "d0e7270b-f4bc-40c0-9725-4fbec58436b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 259
        },
        {
            "id": "8f2e4a8f-2bf7-4601-983f-092d927280e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 260
        },
        {
            "id": "5c931bb0-94d7-44c5-928f-6d00dd24c3d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 261
        },
        {
            "id": "fdeb2732-1d8d-4fcf-ad1d-00c00a5ef4a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 262
        },
        {
            "id": "94145f41-6439-4dfd-bf41-54a3e83572df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 263
        },
        {
            "id": "d1477f01-4f01-4716-9777-d2bf47cbdee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 266
        },
        {
            "id": "8b8228b6-d090-4f81-a728-a3d3c1011762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 267
        },
        {
            "id": "d9cec05a-c4f8-4136-83b8-5b20bcb0b9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 268
        },
        {
            "id": "82463ce7-f6a9-462d-91c0-f213ff35c9ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 269
        },
        {
            "id": "fc1b247a-0c97-440f-8df8-cc09a1704d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 287
        },
        {
            "id": "c8cbc1ec-bd9a-489b-b223-bd13a57725eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 288
        },
        {
            "id": "74226b52-5e14-4c7c-82a8-6bfeb5f5785c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 289
        },
        {
            "id": "fe0b9805-f634-424f-946d-620696e72f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 290
        },
        {
            "id": "097737c3-2a12-4a3f-93f0-1cf48bdab64c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 291
        },
        {
            "id": "0e02f3ac-24e4-4d72-b444-17cbe79a916b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 323
        },
        {
            "id": "45f6923a-2429-41d6-80b8-28e8f22184b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 324
        },
        {
            "id": "9fef219f-f554-4d4c-9c9d-61a747f13f81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 325
        },
        {
            "id": "ddb69555-0700-406c-b4ab-e35dd86cb5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 326
        },
        {
            "id": "0e22d9d4-5275-4457-a618-4833eed0ab68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 327
        },
        {
            "id": "c100f3fd-d632-42c6-9995-ddb9dea3a8c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 328
        },
        {
            "id": "e84165d2-8065-48fc-9174-8829186d6056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 332
        },
        {
            "id": "acf9947d-5f72-452c-bd44-c627851004ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 333
        },
        {
            "id": "6a03f69b-cd3c-4992-a10b-5a6944e9d2fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 334
        },
        {
            "id": "b87a75cf-bb42-4b38-9794-622f0eae4842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 335
        },
        {
            "id": "68a139a8-aadc-43c2-b1f5-9f1fd762e982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 336
        },
        {
            "id": "3f68ab14-f11d-4bd0-91ad-4c3752f9195d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 337
        },
        {
            "id": "f753d2b5-13f1-4365-8e38-26adea24bb91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 338
        },
        {
            "id": "5e34b875-cda4-4c79-88db-42e5a74281ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 339
        },
        {
            "id": "7e89a8a1-1b65-4827-a45f-392015f6be6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 354
        },
        {
            "id": "3067a8e0-dcdc-4756-904d-74c555071bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 355
        },
        {
            "id": "dc4d7df5-65a4-4100-9ecb-9b90274c17a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 356
        },
        {
            "id": "afec248b-8b60-4e8f-be94-dfc408b6e95f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 357
        },
        {
            "id": "2d1fa60d-81ec-4cba-b07d-b731c877e065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 358
        },
        {
            "id": "1288d1b7-b097-4105-9384-39c60c734062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 359
        },
        {
            "id": "c3d48ee2-cbb1-47ff-ae60-1252d5718ae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 538
        },
        {
            "id": "1f581440-9086-439d-9bda-95182e4b371d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 539
        },
        {
            "id": "2173e6e8-914f-4323-9a0a-9ad30ef6cff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 8211
        },
        {
            "id": "94598809-dcf8-45d6-8711-82b7b6543664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 8212
        },
        {
            "id": "6206ba31-8c04-45d0-9f78-db6ecf827d63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 8218
        },
        {
            "id": "d5db0445-8489-494d-8ccb-f9b9b01b83ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 8222
        },
        {
            "id": "9ad13180-82d6-4e84-8f75-b8f5c7425842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 8230
        },
        {
            "id": "c8b111c1-b51c-4ff8-9e48-695af583167b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 38
        },
        {
            "id": "5b30d88a-45bb-4e66-9fab-7cd6d22d6351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 120,
            "second": 45
        },
        {
            "id": "e2f8e4a4-b841-481c-a811-99788b20ce50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 48
        },
        {
            "id": "8de1be8a-86e4-4e16-a158-4a437ee2aef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 54
        },
        {
            "id": "5121059d-a4b5-43bf-938b-69714590dbaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 56
        },
        {
            "id": "ca0e97eb-025e-475d-af5f-a56c49c753b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 57
        },
        {
            "id": "ada00641-67f7-4925-b2c2-9c3b92ce98d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 67
        },
        {
            "id": "9ad76843-a5a9-4831-83b0-2fd142b42590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 71
        },
        {
            "id": "9f5c74a5-b632-405a-ba48-aafa49c28400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 79
        },
        {
            "id": "68701f60-951f-44cd-b84f-bbaf1c65ec05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 81
        },
        {
            "id": "c36743fe-b3ae-4ecd-91af-e15d2ff3f0aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 85
        },
        {
            "id": "d3355056-b0ad-43b2-b067-15957ed9c327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 87
        },
        {
            "id": "961911a1-f6b7-44db-ac06-385121240d26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 99
        },
        {
            "id": "0ef78b17-77ed-49e9-ac86-80db35b845be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 103
        },
        {
            "id": "7afadea7-9495-42d9-8341-f055907367aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 111
        },
        {
            "id": "775e0275-2d97-47cf-9108-cd5bd4e18c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 113
        },
        {
            "id": "1ef23176-4b63-4ac3-80a7-67c935add68f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 117
        },
        {
            "id": "983a8f45-d2ab-4064-b6df-3a2f9396963a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 199
        },
        {
            "id": "e78c3e6e-a459-4a2e-a7c1-eeaf3c1dfc0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 210
        },
        {
            "id": "1e800efc-fd24-45ba-89d4-83536b440075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 211
        },
        {
            "id": "f9967b0b-bf04-4ec2-94f7-d37e303f9ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 212
        },
        {
            "id": "0459f4e5-17b8-4729-a9aa-58645e55a629",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 213
        },
        {
            "id": "2d7be5d9-9086-4c92-88b9-27c0ba88741a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 214
        },
        {
            "id": "64563237-dac2-4f94-834e-4744fcd36871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 216
        },
        {
            "id": "7271515c-94b8-41ea-8f50-d8886a6f800d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 217
        },
        {
            "id": "61da743a-3ebe-4de8-973b-0fdd05e8315d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 218
        },
        {
            "id": "0f51406a-033f-4c3e-85f3-d0de4c82c4b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 219
        },
        {
            "id": "92e4add9-d74e-4b52-86a8-850d511ade8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 220
        },
        {
            "id": "7fc17ad6-510c-4815-9ab8-95b1fd305d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 231
        },
        {
            "id": "30b21c0a-e211-480c-8829-ddb5daf64ef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 242
        },
        {
            "id": "36fb6a9a-6667-49e2-af79-2f7fba1fd9c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 243
        },
        {
            "id": "49079791-2477-45d7-8e20-1909de0b6a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 244
        },
        {
            "id": "b35fc740-9c56-44af-b2f0-b55a86322832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 245
        },
        {
            "id": "0ec37691-4fb6-4393-9161-677f96d11c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 246
        },
        {
            "id": "e16c16a3-a16e-47a3-b96b-69e83c019646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 248
        },
        {
            "id": "91e5fe6a-ad43-493b-a7f7-e159636748ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 249
        },
        {
            "id": "43a765fa-017d-43d2-8f8f-8be9f93e4367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 250
        },
        {
            "id": "6d9dd9f9-3b1d-4183-973d-a01f09e29e3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 251
        },
        {
            "id": "beb8de07-2f9e-494d-a697-385e0e677b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 252
        },
        {
            "id": "9c61aca3-7bdd-44fb-82e5-42ceceea184f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 262
        },
        {
            "id": "9fe2074b-f475-4bbb-8193-25aa22b03e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 263
        },
        {
            "id": "b563b29d-178d-4250-938c-04c30ecb0a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 266
        },
        {
            "id": "0b679d7f-645b-462f-9321-f758bceb7027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 267
        },
        {
            "id": "17f5253e-44fa-44a8-8a25-be593daa9484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 268
        },
        {
            "id": "5266aa70-f4c6-402d-bd76-929ac1e26342",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 269
        },
        {
            "id": "14d30762-8188-4ce2-b2f4-395503db2e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 287
        },
        {
            "id": "6ba185d6-b8e0-4cbb-bd52-9e6f5a297ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 288
        },
        {
            "id": "4019c8d0-60b6-47cb-936e-ac3c1e64fc27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 289
        },
        {
            "id": "e9d124de-aba6-4ea1-9380-d091e845112a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 290
        },
        {
            "id": "5f1a6103-308a-40a5-963b-e118e6cfb8e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 291
        },
        {
            "id": "ea271e09-d491-47c6-a365-91925a8314ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 332
        },
        {
            "id": "c0cc321f-f5c9-4e30-96a2-21b9a67fc233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 333
        },
        {
            "id": "5507dfeb-1a7e-49ba-85b9-30a8a9fb1652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 334
        },
        {
            "id": "b9b3099d-8b1f-4061-86dc-1a0a8a61f144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 335
        },
        {
            "id": "9b29d530-37ef-4ece-be14-84061c0ba352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 336
        },
        {
            "id": "ecba26a5-3c73-46e2-bdbf-6df69342bdd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 337
        },
        {
            "id": "57838e95-4d86-43d8-bd87-9fbd16dee1ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 338
        },
        {
            "id": "61197592-f730-4211-81c8-298c31c7e5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 339
        },
        {
            "id": "c5f93398-6f4a-4474-ba3e-47ca10d4933c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 360
        },
        {
            "id": "af7748fc-0271-4761-8d62-1540105c4ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 361
        },
        {
            "id": "2ae2d809-644e-4bd4-a827-21ac83d2ed7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 362
        },
        {
            "id": "7dca328a-3f1c-4ace-a89b-09baaf1ab1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 363
        },
        {
            "id": "47bbb671-7775-4435-94c9-b27815a331f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 364
        },
        {
            "id": "55aa453c-a72a-44dd-9e51-9bf44b2197b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 365
        },
        {
            "id": "4f383c1e-7504-4b47-a936-77546c0dbb44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 366
        },
        {
            "id": "c3f9553e-5d55-4d64-8721-ef385c8e5ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 367
        },
        {
            "id": "649372fc-6361-4fe8-8a7e-1e50dfa28e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 368
        },
        {
            "id": "7eb479a0-9f16-46b8-8e4d-949dc0d2f7c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 369
        },
        {
            "id": "ce7cbe92-9b26-49f2-8405-5519445165d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 370
        },
        {
            "id": "015dbcf8-8f2e-4d57-a41f-164cb2cdbb7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 371
        },
        {
            "id": "e8cd4f53-c773-4c2b-a4a9-0c835c8e5ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 372
        },
        {
            "id": "69ca3002-23af-46e1-bee1-fedc1ca3b250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 373
        },
        {
            "id": "a8eddd2a-e228-4b18-80e0-c6c7362cb2ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7808
        },
        {
            "id": "6b46be1a-6ef8-49c1-9c5c-ab1f31b4d3e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7809
        },
        {
            "id": "6f963b2b-8c06-4a26-a1f0-f7837f9760cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7810
        },
        {
            "id": "d9c04fde-d8c2-416e-ae51-067ee68d2a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7811
        },
        {
            "id": "706d9aca-d16b-4578-991f-90569715f9d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7812
        },
        {
            "id": "df19793f-3949-4a74-8c2c-ae849282decd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7813
        },
        {
            "id": "65bb1cf4-f7b0-46fc-b259-cf5992b0f988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 120,
            "second": 8211
        },
        {
            "id": "da40777f-cfcb-46f8-9333-1b904bf1e73e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 120,
            "second": 8212
        },
        {
            "id": "e48f11b7-7da4-4323-a562-6c7f7c20190b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 47
        },
        {
            "id": "6aeccd3c-78bc-44a0-ad1c-ed525f86a989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 74
        },
        {
            "id": "f402770b-1104-482a-b686-604816ab5a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 88
        },
        {
            "id": "35d4e065-171b-4883-88b6-cb52a05f9b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 106
        },
        {
            "id": "9a4ec2d0-caf6-4475-8de0-6dd642fba628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 120
        },
        {
            "id": "480a78f4-efac-433b-bb1a-8ac55d0ec65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 122,
            "second": 45
        },
        {
            "id": "3734a899-9aec-47f1-b658-c4d2781d7ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 122,
            "second": 89
        },
        {
            "id": "dd3d1864-2f2d-4f4e-ba43-7ffdd8c7e96f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 122,
            "second": 121
        },
        {
            "id": "109e156e-3fc8-47cb-9381-b07a8d9dda31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 122,
            "second": 8211
        },
        {
            "id": "af7b2818-ea50-4d90-877e-c2134c038d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 122,
            "second": 8212
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 50,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}