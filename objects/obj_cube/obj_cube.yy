{
    "id": "20e91c94-a4c2-4101-ace8-1da776fe2faf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cube",
    "eventList": [
        {
            "id": "d6815476-6c13-453f-8b88-8658158eff69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20e91c94-a4c2-4101-ace8-1da776fe2faf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fddc20fd-9b60-4ca2-9cbc-c618b0e3e55f",
    "visible": true
}