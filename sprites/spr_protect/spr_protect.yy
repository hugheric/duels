{
    "id": "b8b01d74-696a-4839-81e6-b3648571cb3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_protect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 1,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d5304fd-420e-4b03-a798-f969dcef3b59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8b01d74-696a-4839-81e6-b3648571cb3d",
            "compositeImage": {
                "id": "c676a9c0-3ffd-4875-8683-72a98c3dc7a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d5304fd-420e-4b03-a798-f969dcef3b59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0b56750-5be2-4e72-ae53-5ff807ff6c98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d5304fd-420e-4b03-a798-f969dcef3b59",
                    "LayerId": "91f8b7ef-7a31-4afb-b548-ae0653970606"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "91f8b7ef-7a31-4afb-b548-ae0653970606",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8b01d74-696a-4839-81e6-b3648571cb3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}