{
    "id": "2fa4a103-7b3e-440a-b80c-fc626398b61c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fail",
    "eventList": [
        {
            "id": "102c5b8f-ef62-4c0d-8378-db63e7bbaea1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2fa4a103-7b3e-440a-b80c-fc626398b61c"
        },
        {
            "id": "2e8fd50e-2505-48bf-9167-246c08d4e79a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2fa4a103-7b3e-440a-b80c-fc626398b61c"
        },
        {
            "id": "256cb3b6-0e4b-4b2c-9b46-97fb2088a358",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2fa4a103-7b3e-440a-b80c-fc626398b61c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}